﻿#include once "IUP/iup.bi"
#include once "IUP/iup_scintilla.bi"
#include once "windows.bi"
#include once "dir.bi"

#DEFINE NL !"\n"


'compile with -dll option
extern "C"
	declare sub poseidon_Dll_Go alias "poseidon_Dll_Go" ( _dllFullPath as zstring ptr )
	declare sub poseidon_Dll_Release alias "poseidon_Dll_Release" ()

	TYPE HH_AKLINK
		cbStruct     AS LONG         ' int       cbStruct;     // sizeof this structure
		fReserved    AS BOOLEAN      ' BOOL      fReserved;    // must be FALSE (really!)
		pszKeywords  AS WSTRING PTR  ' LPCTSTR   pszKeywords;  // semi-colon separated keywords
		pszUrl       AS WSTRING PTR  ' LPCTSTR   pszUrl;       // URL to jump to if no keywords found (may be NULL)
		pszMsgText   AS WSTRING PTR  ' LPCTSTR   pszMsgText;   // Message text to display in MessageBox if pszUrl is NULL and no keyword match
		pszMsgTitle  AS WSTRING PTR  ' LPCTSTR   pszMsgTitle;  // Message text to display in MessageBox if pszUrl is NULL and no keyword match
		pszWindow    AS WSTRING PTR  ' LPCTSTR   pszWindow;    // Window to display URL in
		fIndexOnFail AS BOOLEAN      ' BOOL      fIndexOnFail; // Displays index if keyword lookup fails.
	END TYPE
	
	dim shared as any ptr		ocxHandle
	dim shared HtmlHelpW as function( hwndCaller AS HWND, pswzFile AS LPCWSTR, uCommand AS UINT, dwData AS DWORD_PTR ) as HWND
	
	dim shared as any ptr		sevenZipHandle
	dim shared SevenZip as function( _hwnd as HWND, _szCmdLine as LPCSTR, _szOutput as LPSTR, _dwSize as DWORD ) as integer
	
	dim shared SevenZip2 as sub( _hwnd as HWND, _szCmdLine as LPCSTR, _szOutput as LPSTR, _dwSize as DWORD )


	dim shared as Ihandle ptr	POSEIDON_HANDLE, THIS_MAINDIALOG_HANDLE
	dim shared as string		DLLFULLPATH, DLLPATH, LANGUAGE

	
	' Shared Variables
	dim shared as Ihandle ptr	listManuals, textManualDir
	dim shared as string		manuals(), tempManuals(), hhDocument()

	
	' Main dialog OPACITY CALLBACK
	private function OPACITYVal_VALUECHANGED_CB cdecl( ih as Ihandle ptr ) as integer
	
		dim as integer v = IupGetInt( ih, "VALUE" )
			
		IupSetInt( THIS_MAINDIALOG_HANDLE, "OPACITY", v )
	
		return IUP_DEFAULT
		
	end function
	
	
	' Select List Item
	private function listManuals_ACTION( ih as Ihandle ptr, text as zstring ptr, item as integer, state as integer ) as integer
	
		if( textManualDir <> 0 ) then
		
			if( item <= len( tempManuals ) ) then
			
				dim as integer _pos = instr( tempManuals(item-1), "," )
				if( _pos > 0 ) then

					dim as string _name = left( tempManuals(item-1), _pos - 1 )
					dim as string path = right( tempManuals(item-1), len( tempManuals(item-1) ) - _pos )
				
					' Double Check
					if( _name = text[0] ) then IupSetAttribute( textManualDir, "VALUE", path )
				end if
			end if
		end if
		
		return IUP_DEFAULT
	end function
	
	
	' Edit Text
	private function textManualDir_ACTION( ih as Ihandle ptr, c as integer, new_value as zstring ptr ) as integer
	
		if( listManuals <> 0 ) then
		
			dim as integer id = IupGetInt( listManuals, "VALUE" )
			if( id > 0 ) then
			
				dim as string dirText = trim( new_value[0] )
				dim as integer _pos = instr( tempManuals(id-1), "," )
				if( _pos > 0 ) then

					dim as string _name = left( tempManuals(id-1), _pos - 1 )
					tempManuals(id-1) = _name + "," + dirText
				end if
			end if
		end if
		
		return IUP_DEFAULT
	end function
	
	
	private function iuputText_K_ANY cdecl( ih as Ihandle ptr, c as integer ) as integer
	
		if( c = 13 ) then			'ENTER

			dim as string	iuputResult = trim( IupGetAttribute( ih, "VALUE" )[0] )

			if( len( iuputResult ) > 0 ) then
			
				IupSetAttribute( listManuals, "APPENDITEM", iuputResult )
				
				redim preserve tempManuals(ubound(tempManuals) + 1 )
				tempManuals(ubound(tempManuals)) = iuputResult + ","
				
				IupSetInt( listManuals, "VALUE", ubound(tempManuals)+1 ) ' Set Focus
				IupSetAttribute( textManualDir, "VALUE", "" )
				
				'IupSetFocus( textManualDir )
			
			end if
			
			IupDestroy( IupGetHandle( "CHMhelp_iuputDlg" ) )
		
		elseif( c = 65307 ) then		' ESC

			IupDestroy( IupGetHandle( "CHMhelp_iuputDlg" ) )
		end if
		
		return IUP_DEFAULT
	
	end function	
	
	
	private function btnToolsAdd_ACTION( ih as Ihandle ptr ) as integer
	
		dim as Ihandle ptr iuputText = IupText( 0 )
		IupSetAttributes( iuputText, "SIZE=100x" )
		IupSetCallback( iuputText, "K_ANY", cast( Icallback, @iuputText_K_ANY ) )
		
		IupMessage( "", IupGetAttribute( listManuals, "SIZE" ) )
		IupMessage( "", IupGetAttribute( listManuals, "VISIBLELINES" ) )
		
		dim as string xy = IupGetAttribute( textManualDir, "SCREENPOSITION" )[0]
		dim as integer _x, _y, _pos = instr( xy, "," )
		
		if( _pos = 0 ) then return IUP_DEFAULT
		
		_x = val( left( xy, _pos -1 ) ) + 20
		_y = val( right( xy, len( xy ) - _pos ) ) - 30
		
		dim as Ihandle ptr iuputDlg = IupDialog( iuputText )
		IupSetAttributes( iuputDlg, "BORDER=NO,RESIZE=NO,MAXBOX=NO,MINBOX=NO,MENUBOX=NO,OPACITY=198" )
		IupSetHandle( "CHMhelp_iuputDlg", iuputDlg )
		
		IupSetFocus( iuputText )
		
		IupPopup( iuputDlg, _x, _y )
		
		return IUP_DEFAULT
	end function
	
	
	private function btnToolsErase_ACTION( ih as Ihandle ptr ) as integer

		if( listManuals <> 0 ) then

			dim as integer id = IupGetInt( listManuals, "VALUE" )
			dim as integer arrayBound = ubound(tempManuals)
			
			
			if( id < 1 OR id > arrayBound + 1 ) then return IUP_DEFAULT
		
			dim as string _name = IupGetAttributeId( listManuals, "", id )[0]
			
			' Double Check
			dim as integer _pos = instr( tempManuals(id-1), "," )
			if( _pos > 0 ) then
				
				if( _name = left( tempManuals(id-1), _pos - 1 ) ) then

					IupSetInt( listManuals, "REMOVEITEM", id )
					id -= 1
					
					if( arrayBound > 0 ) then

						if( id <> arrayBound ) then
							dim as string _temp = tempManuals(id)
							tempManuals(id) = tempManuals(arrayBound)
							tempManuals(arrayBound) = _temp
						end if

						redim preserve tempManuals(arrayBound-1)
						IupSetAttribute( textManualDir, "VALUE", "" )
					else
						
						erase tempManuals
						IupSetAttribute( textManualDir, "VALUE", "" )
					end if
				end if
			end if
		end if

		return IUP_DEFAULT
	end function
	
	
	private function btnToolsUp_ACTION( ih as Ihandle ptr ) as integer
	
		if( listManuals <> 0 ) then 
		
			dim as integer itemNumber = IupGetInt( listManuals, "VALUE" )

			if( itemNumber > 1 ) then
			
				dim as zstring ptr prevItemText = IupGetAttributeId( listManuals, "", itemNumber -1 )
				dim as zstring ptr nowItemText = IupGetAttributeId( listManuals, "", itemNumber )

				IupSetAttributeId( listManuals, "", itemNumber - 1, nowItemText )
				IupSetAttributeId( listManuals, "", itemNumber, prevItemText )

				IupSetInt( listManuals, "VALUE", itemNumber - 1 ) ' Set Foucs
				
				dim as string temp = tempManuals(itemNumber-2)
				tempManuals(itemNumber-2) = tempManuals(itemNumber-1)
				tempManuals(itemNumber-1) = temp
			end if
		end if

		return IUP_DEFAULT
	end function	

	
	private function btnToolsDown_ACTION( ih as Ihandle ptr ) as integer

		if( listManuals <> 0 ) then
		
			dim as integer itemNumber = IupGetInt( listManuals, "VALUE" )
			dim as integer itemCount = IupGetInt( listManuals, "COUNT" )

			if( itemNumber < itemCount ) then
			
				dim as zstring ptr nextItemText = IupGetAttributeId( listManuals, "", itemNumber + 1 )
				dim as zstring ptr nowItemText = IupGetAttributeId( listManuals, "", itemNumber )

				IupSetAttributeId( listManuals, "", itemNumber + 1, nowItemText )
				IupSetAttributeId( listManuals, "", itemNumber, nextItemText )

				IupSetInt( listManuals, "VALUE", itemNumber + 1 ) ' Set Foucs
				
				dim as string temp = tempManuals(itemNumber)
				tempManuals(itemNumber) = tempManuals(itemNumber-1)
				tempManuals(itemNumber-1) = temp
			end if
		end if

		return IUP_DEFAULT
	end function
	
	
	private function btnManualDir_ACTION( ih as Ihandle ptr ) as integer
	
		if( listManuals <> 0 ) then
		
			if( IupGetInt( listManuals, "COUNT" ) > 0 ) then 
			
				dim as Ihandle ptr fileDlg = IupFileDlg()

				IupSetAttribute( fileDlg, "PARENTDIALOG", "THIS_MAINDIALOG_HANDLE" )
				IupSetAttribute( fileDlg, "DIALOGTYPE", "OPEN" )
				IupSetAttribute( fileDlg, "TITLE", "Add CHM Dir" )
				IupSetAttribute( fileDlg, "EXTFILTER", "CHM Files|*.chm|All Files|*.*" )
				
				IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
				
				if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
				
					dim as string fullPath = trim( IupGetAttribute( fileDlg, "VALUE" )[0] )
					if( len( dir( fullPath ) ) > 0 ) then 
						
						IupSetAttribute( textManualDir, "VALUE", fullPath )
						
						dim as integer item = IupGetInt( listManuals, "VALUE" )
						if( item > 0 ) then	tempManuals(item-1) = IupGetAttributeId( listManuals, "", item )[0] + "," + fullPath
					end if
				end if
			end if
		end if
		
		return IUP_DEFAULT
	end function
	
	
	private function normalize( _s as string ) as string
		
		dim result as string
		
		for i as integer = 0 to len( _s ) - 1
			if( _s[i] = 92 ) then result = result + "/" else result = result + chr( _s[i] )
		next
		
		return result

	end function


	' by TJF, https://www.freebasic.net/forum/viewtopic.php?t=22091#p193892
	private SUB delete_all()
		VAR n = DIR("*", fbDirectory)
		dim as string t = ""
		WHILE LEN(n)
			IF n <> "." ANDALSO n <> ".." THEN t &= n & NL
			n = DIR()
		WEND

		VAR a = 1, e = a, l = LEN(t)
		WHILE a < l
			e = INSTR(a, t, NL)
			n = MID(t, a, e - a)
			IF 0 = CHDIR(n) THEN
				delete_all()
				CHDIR ("..")
				RMDIR(n)
			END IF
			a = e + 1
		WEND

		n = DIR("*")
		WHILE LEN(n)
			KILL (n)
			n = DIR()
		WEND
	END SUB
	
	
	
	' From FreeBASIC Manual
	private function getHH( _dir as string ) as string
		
		_dir = normalize( _dir )
		
		if( len(_dir) > 0 ) then

			if( right(_dir, 1 ) <> "/" ) then _dir += ( "/" )
		
			Dim As String filename = Dir( _dir + "*", &h20 ) ' Start a file search with the specified filespec/attrib *AND* get the first filename.
			Do While Len(filename) > 0 ' If len(filename) is 0, exit the loop: no more filenames are left to be read.

				if( right( filename, 3 ) = "hhk" ) then
					return _dir + filename
				elseif( right( filename, 3 ) = "hhc" ) then
					return _dir + filename
				end if
				
				filename = Dir() ' Search for (and get) the next item matching the initially specified filespec/attrib.
			Loop
		end if
		
		return ""
		
	End function
	
	
	private sub deCompile()
	
		for i as integer = 0 to ubound( manuals )
			
			dim as integer _pos = instr( manuals(i), "," )
			if( _pos > 0 ) then
			
				dim as string			_name = left( manuals(i), _pos - 1 )
				dim as string			_path = right( manuals(i), len( manuals(i) ) - _pos )
				
				dim as boolean			bExtractOK
				dim as zstring ptr		buf
				dim as DWORD 			dwSize = 0
				
				buf = allocate( 1024 )
			
				' check chm is existed
				if( len( dir( _path ) ) > 0 ) then 
				
					' check the folder is existed ( under DLLPATH )
					if( len( dir( DLLPATH + _name, fbDirectory ) ) = 0 ) then ' No existed
						
						mkdir( DLLPATH + _name )
						
						if( SevenZip <> 0 ) then
						
							bExtractOK = true
							if( SevenZip( 0, "e " + _path + " *.hhk " + DLLPATH + _name + "\", buf, dwSize ) <> 0 ) then
								if( SevenZip( 0, "e " + _path + " *.hhc " + DLLPATH + _name + "\", buf, dwSize ) <> 0 ) then
									IupMessage( "Error", "7z.dll Error!" )
									bExtractOK = false
								end if
							end if
							
							'if( bExtractOK ) then IupMessage( "SUCCESS", _name + chr( 10 ) + "extract OK" )
						else
						
							if( shell( "hh " + "-decompile " + DLLPATH + _name + " " + _path ) = -1 ) then
								IupMessage( "ERROR", _name + chr( 10 ) + "decompile ERROR" )
								continue for
							else
								IupMessage( "SUCCESS", _name + chr( 10 ) + "decompile OK" )
							end if
						end if						
					
					else
						if( getHH( DLLPATH + _name ) = "" ) then
							
							if( SevenZip <> 0 ) then
								bExtractOK = true
								if( SevenZip( 0, "e " + _path + " *.hhk " + DLLPATH + _name + "\", buf, dwSize ) <> 0 ) then
									if( SevenZip( 0, "e " + _path + " *.hhc " + DLLPATH + _name + "\", buf, dwSize ) <> 0 ) then
										IupMessage( "Error", "7z.dll Error!" )
										bExtractOK = false
									end if
								end if
								
								'if( bExtractOK ) then IupMessage( "SUCCESS", _name + chr( 10 ) + "extract OK" )
							else
							
								if( shell( "hh " + "-decompile " + DLLPATH + _name + " " + _path ) = -1 ) then
									IupMessage( "ERROR", _name + chr( 10 ) + "decompile ERROR" )
									continue for
								else
									IupMessage( "SUCCESS", _name + chr( 10 ) + "decompile OK" )
								end if
							end if
						end if
					end if

					dim as string _hhFullPath = getHH( DLLPATH + _name )
	
					if( _hhFullPath <> "" ) then

						open _hhFullPath For input As #3
						dim as integer fileLength = lof( 3 )
						if fileLength > 0 then
							hhDocument(i) = string( fileLength, 0 )
							get #3, , hhDocument(i)
						end if
						close
						
						if( bExtractOK = false ) then
						
							' Clean the decompile files, except for hhc or hhk
							dim as string nowdir = curdir()
							IF 0 = CHDIR( DLLPATH + _name ) THEN delete_all()
							chdir( nowdir )
						
							' Write Back the "xxx.hhc" pr "xxx.hhk"
							open _hhFullPath For output As #1
							print #1, hhDocument(i)
							close #i
						end if
						
						hhDocument( i ) = lcase( hhDocument( i ) )
					end if
				end if
				
				deallocate( buf )
				
			end if
			
		next

	end sub
	
	
	private function yesButton_ACTION( ih as Ihandle ptr ) as integer
	
		dim as integer arrayUBound = ubound(tempManuals)
		
		if( arrayUBound >= 0 ) then
			redim manuals(arrayUBound)
			redim hhDocument(arrayUBound)
			
			for i as integer = 0 to arrayUBound
				manuals(i) = tempManuals(i)
			next
			
			deCompile()
		else

			erase manuals
		end if
		
		erase tempManuals
		IupHide( THIS_MAINDIALOG_HANDLE )
	
		return IUP_DEFAULT
		
	end function
	
	
	private function noButton_ACTION( ih as Ihandle ptr ) as integer
	
		IupHide( THIS_MAINDIALOG_HANDLE )
		return IUP_DEFAULT
		
	end function	
		
	
	private sub saveSettings()
	
		dim as string	fullPath = DLLPATH + "CHMhelp.ini"
		dim as string	doc
		
		for i as integer = 0 to ubound(manuals)
			if( i = 0 ) then doc += manuals(i) else doc += ( chr(10) + manuals(i) )
		next

		open fullPath For output As #2
		print #2, doc
		close #2
	
	end sub
	
	
	private sub loadSettings()
	
		dim as string fullPath = DLLPATH + "CHMhelp.ini"
		
		' Check existed
		if( len( dir( fullPath ) ) < 1 ) then

			IupMessageError( THIS_MAINDIALOG_HANDLE, "CHMhelp.ini isn't existed!" )
			return
		end if
		
		open fullPath For input As #1
		dim as integer fileLength = lof( 1 )

		if( fileLength > 0 ) then

			dim as string	s, mode, _left, _right
			dim as boolean	bPreLoad
			
			do until( eof(1) )

				Line Input #1, s
				s = trim(s)
				if( len(s) > 0 ) then
				
					redim preserve manuals(ubound(manuals)+1)
					manuals(ubound(manuals)) = s
				end if
			loop
		end if
	
	end sub
	

	private function init7z() as boolean
	
		sevenZipHandle = DyLibLoad( DLLPATH + "7-zip32.dll" )
		if( sevenZipHandle = 0 ) then
		
			IupMessage( "Error", "DyLibLoad 7-zip32.dll error!" )
			return false
		end if
		
		SevenZip = DyLibSymbol( sevenZipHandle, "SevenZip" )
		if( SevenZip = 0 ) then
			
			IupMessage( "Error", "DyLibSymbol SevenZip error!" )
			dylibfree( sevenZipHandle )
			sevenZipHandle = 0
			return false			
		end if
		
		return true
	
	end function
	
	
	private function initHtmlHelp() as boolean
	
		ocxHandle = DyLibLoad( "hhctrl.ocx" )
		if( ocxHandle = 0 ) then
		
			IupMessage( "Error", "DyLibLoad hhctrl.ocx error!" )
			return false
		end if
		

		HtmlHelpW = DyLibSymbol( ocxHandle, "HtmlHelpW" )
		if( HtmlHelpW = 0 ) then
		
			IupMessage( "Error", "DyLibSymbol HtmlHelpW error!" )
			dylibfree( ocxHandle )
			ocxHandle = 0
			return false
		end if
		
		return true
	
	end function
	
	
	private function getWordDoubleSide() as string
	
		dim as string word = ""
		
		dim as Ihandle ptr sciPoseidon = IupGetFocus()  'cast( Ihandle ptr, IupGetAttribute( _MaintabsHandle, "VALUE_HANDLE" ) ) 'Get Active Doc Tab(iupscintilla handle)
		if( sciPoseidon <> 0 ) then

			' Get sciPoseidon current pos
			dim as string s
			dim as integer _pos = IupScintillaSendMessage( sciPoseidon, 2008, 0, 0 ) ' SCI_GETCURRENTPOS = 2008
			
			while( _pos < IupGetInt( sciPoseidon, "COUNT" ) )
				
				s = IupGetAttributeId( sciPoseidon, "CHAR", _pos )[0]
				
				'if( asc(s) = 9 ) then IupMessage( "",str(_pos))
				
				if( s = ">" OR s = "<" OR s = " " OR s = "=" OR s = "(" OR s = ")" OR s = "[" OR s ="]" OR asc(s) = 9 OR asc(s) = 10 OR asc(s) = 13 ) then
					exit while
				elseif( s = "." OR s = "+" OR s = "-" OR s = "*" OR s = "/" OR s = "%" ) then
					exit while
				end if
				
				_pos += 1
			wend
			
			_pos -= 1
			while( _pos >= 0 )
				
				s = IupGetAttributeId( sciPoseidon, "CHAR", _pos )[0]
				
				'if( asc(s) = 9 ) then IupMessage( "",str(_pos))
				
				if( s = ">" OR s = "<" OR s = " " OR s = "=" OR s = "(" OR s = ")" OR s = "[" OR s ="]" OR asc(s) = 9 OR asc(s) = 10 OR asc(s) = 13 ) then
					exit while
				elseif( s = "." OR s = "+" OR s = "-" OR s = "*" OR s = "/" OR s = "%" ) then
					exit while
				else
					word = s + word 
				end if
			
				_pos -= 1
			wend
			
		end if
		
		return trim( word )
	
	end function
	
	
	private sub refreshList()

		IupSetAttribute( textManualDir, "VALUE", "" )
		IupSetAttribute( listManuals, "REMOVEITEM", "ALL" )
		
		for i as integer = 0 to ubound(manuals)
			
			dim as integer _pos = instr( manuals(i), "," )
			if( _pos > 0 ) then
		
				dim as string _name = left( manuals(i), _pos - 1 )
				IupSetAttribute( listManuals, "APPENDITEM", _name )
			end if
		next
		
	end sub
	
	
	
	private sub callHtmlHelp( word as string )

		if( ocxHandle <> 0 ) then
			
			dim as wstring * 128 keyword
			keyword	= wStr( word )
		
			Dim hh As HH_AKLINK
			
			for i as integer = 0 to ubound(manuals)

				dim as string _target = lcase( "value=""" + word + """" )
				
				if( instr( hhDocument(i), _target ) > 0 ) then

					dim as integer _pos = instr( manuals(i), "," )
					if( _pos > 0 ) then
					
						hh.cbStruct     = SizeOf(HH_AKLINK)
						hh.fReserved    = 0
						hh.pszKeywords  = @keyword

						dim as string path = right( manuals(i), len( manuals(i) ) - _pos )
						dim as wstring * 128 wPath = wstr( path )
					
						' HH_KEYWORD_LOOKUP = 13
						if( HtmlHelpW( 0, wPath, 13, Cast(DWORD_PTR, @hh ) ) <> 0 ) Then exit for
					end if
				end if
			next
		end if
		
	end sub
	
	
	
	
	
	

	' **************************************** The Main *************************************************
	sub poseidon_Dll_Go alias "poseidon_Dll_Go"( _dllFullPath as zstring ptr ) export

		' The main IupOpen() is already ran by poseidonFB, no need anymore
		POSEIDON_HANDLE = IupGetHandle( "POSEIDON_MAIN_DIALOG" ) ' Get poseidonFB main dialog handle(IUP)
		
		if( POSEIDON_HANDLE <> 0 ) then
		
			#ifndef __FB_WIN32__

				IupMessageError( POSEIDON_MAIN_DIALOG, "This Plugin is Win Only!" )
				exit sub

			#endif		
		
		
			' Check the THIS_MAINDIALOG_HANDLE is already created......
			if( THIS_MAINDIALOG_HANDLE = 0 ) then
				
				' Get DLL path
				DLLFULLPATH = _dllFullPath[0]
				DLLPATH = _dllFullPath[0]
				dim as integer slashPos = instrrev( DLLPATH, "/" )
				DLLPATH = left( DLLPATH, slashPos ) ' include last /
				
				' Check which language using, check main dialog title
				dim as zstring ptr poseidonTitlePtr = IupGetAttribute( POSEIDON_HANDLE, "TITLE" )
				dim as string poseidonTitle = trim( poseidonTitlePtr[0] )
				if( instr( poseidonTitle, "poseidonFB" ) = 0 ) then LANGUAGE = "d" else LANGUAGE = "freebasic"
			
				loadSettings()
				
				listManuals = IupList( 0 )
				IupSetAttributes( listManuals, "EXPAND=HORIZONTAL,SIZE=240x" )
				IupSetCallback( listManuals, "ACTION", cast( Icallback, @listManuals_ACTION ) )
				
				' Button:
				dim as Ihandle ptr btnToolsAdd = IupButton( 0, 0 )
				IupSetAttributes( btnToolsAdd, "IMAGE=icon_debug_add,FLAT=YES" )
				IupSetCallback( btnToolsAdd, "ACTION", cast( Icallback, @btnToolsAdd_ACTION ) )

				dim as Ihandle ptr btnToolsErase = IupButton( 0, 0 )
				IupSetAttributes( btnToolsErase, "IMAGE=icon_delete,FLAT=YES" )
				'IupSetAttribute( btnToolsErase, "TIP", GLOBAL.languageItems["remove"].toCString );
				IupSetCallback( btnToolsErase, "ACTION", cast( Icallback, @btnToolsErase_ACTION ) )
				
				dim as Ihandle ptr btnToolsUp = IupButton( 0, 0 )
				IupSetAttributes( btnToolsUp, "IMAGE=icon_uparrow,FLAT=YES" )
				IupSetCallback( btnToolsUp, "ACTION", cast (Icallback, @btnToolsUp_ACTION ) )
				
				dim as Ihandle ptr btnToolsDown = IupButton( 0, 0 )
				IupSetAttributes( btnToolsDown, "IMAGE=icon_downarrow,FLAT=YES" )
				IupSetCallback( btnToolsDown, "ACTION", cast(Icallback,  @btnToolsDown_ACTION ) )
		
				dim as Ihandle ptr vBoxButtonTools = IupVbox( btnToolsAdd, btnToolsErase, btnToolsUp, btnToolsDown, 0 )
		
				dim as Ihandle ptr listHbox = IupHbox( listManuals, vBoxButtonTools, 0 )
				IupSetAttributes( listHbox, "NORMALIZESIZE=VERTICAL" )
				
				dim as Ihandle ptr frameList = IupFrame( listHbox )
				IupSetAttributes( frameList, "ALIGNMENT=ACENTER,MARGIN=2x2" )
				
				dim as Ihandle ptr OPACITYVal = IupVal( 0 )
				IupSetAttributes( OPACITYVal, "MIN=100,MAX=255,VALUE=180,RASTERSIZE=70x20")
				IupSetCallback( OPACITYVal, "VALUECHANGED_CB", cast( Icallback, @OPACITYVal_VALUECHANGED_CB ) )
				
				
				
				dim as Ihandle ptr labelManualDir = IupLabel( " Manual Dir :" )
				IupSetAttributes( labelManualDir, "ALIGNMENT=ARIGHT" )

				textManualDir = IupText( 0 )
				IupSetAttribute( textManualDir, "EXPAND", "HORIZONTAL" )
				IupSetCallback( textManualDir, "ACTION", cast( Icallback, @textManualDir_ACTION ) )
				
				dim as Ihandle ptr btnManualDir = IupButton( 0, 0 )
				IupSetAttributes( btnManualDir, "IMAGE=icon_openfile,FLAT=YES" )
				IupSetCallback( btnManualDir, "ACTION", cast( Icallback, @btnManualDir_ACTION ) )

				dim as Ihandle ptr hBox00 = IupHbox( labelManualDir, textManualDir, btnManualDir, 0 )
				IupSetAttribute( hBox00, "ALIGNMENT", "ACENTER" )
				
				dim as Ihandle ptr labelSEPARATOR = IupLabel( 0 )
				IupSetAttribute( labelSEPARATOR, "SEPARATOR", "HORIZONTAL")


				dim as Ihandle ptr yesButton = IupButton( "   OK   ", 0)
				IupSetAttribute( yesButton, "SIZE", "40x12" )
				IupSetCallback( yesButton, "ACTION", cast( Icallback, @yesButton_ACTION ) )
				
				dim as Ihandle ptr noButton = IupButton(  " Cancel ", 0)
				IupSetAttribute( noButton, "SIZE", "40x12" )
				IupSetHandle( "noButton", noButton )
				IupSetCallback( noButton, "ACTION", cast( Icallback, @noButton_ACTION ) )
				
				dim as Ihandle ptr hBox01 = IupHbox( OPACITYVal, IupFill, yesButton, noButton, 0 )
				
				' Create main vBox
				dim as Ihandle ptr vBoxLayout = IupVbox( frameList, hBox00, labelSEPARATOR, hBox01, 0 )

				THIS_MAINDIALOG_HANDLE = IupDialog( vBoxLayout )
				IupSetAttributes( THIS_MAINDIALOG_HANDLE, "MAXBOX=NO,MINBOX=NO,RESIZE=NO,PARENTDIALOG=POSEIDON_MAIN_DIALOG,ICON=icon_fbmanual,OPACITY=240" )
				IupSetAttribute( THIS_MAINDIALOG_HANDLE, "TITLE", "CHMhelp" )
				IupSetAttribute( THIS_MAINDIALOG_HANDLE, "DEFAULTESC", "noButton" )

				' Init
				redim tempManuals(ubound(manuals))
				redim hhDocument(ubound(manuals))
				
				init7z()
				deCompile()
				
				for i as integer = 0 to ubound(manuals)
					tempManuals(i) = manuals(i)
				next
				
				' If getWordDoubleSide() after IupShowXY(), we'll lost the keyboard focus, so get the word first
				dim as string word = getWordDoubleSide() 
				
				IupShowXY( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTER )
				
				if( initHtmlHelp() ) then

					refreshList()
					if( word <> "" ) then
					
						IupHide( THIS_MAINDIALOG_HANDLE)
						callHtmlHelp( word )
					else
						IupPopup( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTER )
					end if
				else

					IupHide( THIS_MAINDIALOG_HANDLE)
				end if
			else
			
				if( ocxHandle <> 0 ) then
				
					dim as string word = getWordDoubleSide()
					if( word = "" ) then

						IupShowXY( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTER )
						refreshList()
						
						redim tempManuals(ubound(manuals))
						for i as integer = 0 to ubound(manuals)
							tempManuals(i) = manuals(i)
						next
						IupPopup( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTER )
					else
					
						callHtmlHelp( word )
					end if
				end if
				
			end if
		end if
	end sub
	
	
	'When poseidonFB quit, trigger the poseidonFB_Dll_Release()
	sub poseidon_Dll_Release alias "poseidon_Dll_Release"() export
		
		#ifdef __FB_WIN32__
			saveSettings()
		#endif
		
		if( sevenZipHandle <> 0 ) then dylibfree( sevenZipHandle )
		if( ocxHandle <> 0 ) then dylibfree( ocxHandle )
		if( THIS_MAINDIALOG_HANDLE <> 0 ) then IupDestroy( THIS_MAINDIALOG_HANDLE )

	end sub
	
end extern