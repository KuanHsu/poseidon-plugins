#include once "IUP/iup.bi"

type CImage

	private :
		dim i as integer
		declare function load_image_dtor() as Ihandle ptr
		declare function load_image_openfile() as Ihandle ptr
		declare function load_image_fun_protected() as Ihandle ptr
		declare function load_image_struct_obj() as Ihandle ptr
		declare function load_image_variable_private_obj() as Ihandle ptr
		declare function load_image_sub_private() as Ihandle ptr
		declare function load_image_variable_obj() as Ihandle ptr
		declare function load_image_class_obj() as Ihandle ptr
		declare function load_image_union_obj() as Ihandle ptr
		declare function load_image_variable_array_private_obj() as Ihandle ptr
		declare function load_image_alias() as Ihandle ptr
		declare function load_image_sub_public() as Ihandle ptr
		declare function load_image_variable_protected_obj() as Ihandle ptr
		declare function load_image_operator() as Ihandle ptr
		declare function load_image_variable_array_obj() as Ihandle ptr
		declare function load_image_define_var() as Ihandle ptr
		declare function load_image_macro() as Ihandle ptr
		declare function load_image_fun_private() as Ihandle ptr
		declare function load_image_with() as Ihandle ptr
		declare function load_image_property_var() as Ihandle ptr
		declare function load_image_variable_array_protected_obj() as Ihandle ptr
		declare function load_image_define_fun() as Ihandle ptr
		declare function load_image_fun_public() as Ihandle ptr
		declare function load_image_enum_obj() as Ihandle ptr
		declare function load_image_ctor() as Ihandle ptr
		declare function load_image_namespace_obj() as Ihandle ptr
		declare function load_image_sub_protected() as Ihandle ptr
		declare function load_image_property_obj() as Ihandle ptr
		declare function load_image_enum_member_obj() as Ihandle ptr
		declare function load_image_parameter_obj() as Ihandle ptr
		declare function load_image_scope() as Ihandle ptr
		declare function load_image_outline() as ihandle ptr
		declare function load_image_show_pr()  as ihandle ptr
		declare function load_image_show_p() as ihandle ptr
		declare function load_image_show_r() as ihandle ptr
		declare function load_image_show_nopr() as ihandle ptr
		declare function load_image_collapse() as ihandle ptr

	public :
		declare constructor()

end type