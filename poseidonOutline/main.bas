#include once "parser/scanner.bi"
#include once "parser/parser.bi"
#include once "IUP/iup.bi"
#include once "outline.bi"
#include once "image.bi"


dim shared as Ihandle ptr					THIS_MAINDIALOG_HANDLE

dim shared as COutline 						outlineObject
dim shared as CImage 						imageObject
dim shared as Scanner 						tokenScanner
dim shared as CParser 	 					parser

dim shared as CASTnode ptr					root

dim shared as string						tabTitle
'dim shared as boolean						bFocusRun

dim shared as Ihandle ptr					OutlineTIMER
common shared as T_GlobalVariables Ptr		TGlobalVariablesDll

dim shared as integer						KeyPressNum



'=============================
'=============================
'Function Tool
private function gotoLine( treeID as integer ) as integer
				
	dim as CASTnode ptr _node = cast( CASTnode ptr, IupGetAttributeId( outlineObject.getTreeHandle, "USERDATA", treeID ) )
	if( _node <> null ) then
	
		dim as Ihandle ptr iupSci	= getActiveDoucumentPtr()
		dim as string documentTitle	= getActiveDoucumentTiTle()				
		dim as string rootTitle =IupGetAttributeId( outlineObject.getTreeHandle, "TITLE", 0 )[0]
		if( rootTitle = documentTitle ) then

			dim as integer lineNumber = _node->lineNumber_ - 1
			IupSetAttributeId( iupSci, "ENSUREVISIBLE", lineNumber, "ENFORCEPOLICY" )
			IupSetInt( iupSci, "CARET", lineNumber )
			IupSetFocus( iupSci )
		end if
		return IUP_IGNORE
	end if	
	
	return IUP_DEFAULT
end function



'=============================
'=============================
'Callback Function
#ifdef __fb_win32__
	private function GlobalButton_CB cdecl ( button as integer, pressed as integer,  x as integer, y as integer, status as zstring ptr ) as integer
		
		if( pressed = 0 ) then 
			if( button = IUP_BUTTON1 ) then	KeyPressNum = -1
		end if
		
		return IUP_DEFAULT
	end function

	private function GlobalKeyPress_CB cdecl ( c as integer, press as integer ) as integer
		
		KeyPressNum = c
		return IUP_DEFAULT
	end function
#endif


private function OutlineTIMER_ACTION_CB cdecl ( ih as Ihandle ptr ) as integer
	
	if( IupGetInt( THIS_MAINDIALOG_HANDLE, "VISIBLE" ) = 0 ) then return IUP_DEFAULT
	
	'if( IupGetChildCount(TGlobalVariablesDll->tabs) > 1 ) then
		dim as Ihandle ptr iupSci	= getActiveDoucumentPtr()
		dim as string documentTitle	= getActiveDoucumentTiTle()
		
		if( iupSci <> null AND documentTitle <> "" ) then
		
			if( documentTitle <> tabTitle ) then

				tabTitle = documentTitle
				dim as zstring ptr document = IupGetAttribute( iupSci, "VALUE" )
				
				dim as TokenUnit ptr pTokens = tokenScanner.scan( document[0] )
				if( pTokens <> null ) then

					parser.setTokenData( pTokens, tokenScanner.getTokenCount )
					root = parser.parse( tabTitle )
					outlineObject.createTree( root )
				else
					outlineObject.cleanTree()
				end if
			end if
		else
			if( IupGetAttributeId( outlineObject.getTreeHandle, "TITLE", 0 )[0] <> " " ) then outlineObject.cleanTree
			tabTitle = " "
		end if	
	'end if
	
	return IUP_DEFAULT
	
end function


private function valOPACITY_VALUECHANGED_CB cdecl ( ih as Ihandle ptr ) as integer

	IupSetInt( THIS_MAINDIALOG_HANDLE, "OPACITY", IupGetInt( ih, "VALUE" ) )
	return IUP_DEFAULT
end function


private function collapseButton_ACTION cdecl ( ih as Ihandle ptr ) as integer

	dim as Ihandle ptr tree = outlineObject.getTreeHandle()
	if( tree <> null ) then
	
		dim as integer id = IupGetInt( tree, "VALUE" )
		
		if( id <= 0 ) then
		
			if( IupGetAttributeId( tree, "STATE", 0 )[0] = "EXPANDED" ) then
				IupSetAttribute( tree, "EXPANDALL", "NO" )
			else
				IupSetAttribute( tree, "EXPANDALL", "YES" )
				IupSetAttribute( tree, "TOPITEM", "YES" ) ' Set position to top
			end if
		else
			dim as integer 		nowDepth = IupGetIntId( tree, "DEPTH", id )
			dim as zstring ptr	nowState = IupGetAttributeId( tree, "STATE", id )
			
			if( nowState <> null ) then
				for i as integer = IupGetInt( tree, "COUNT" ) - 1 to 1 step -1
					if( IupGetIntId( tree, "DEPTH", i ) = nowDepth ) then
						if( IupGetIntId( tree, "CHILDCOUNT", i ) > 0 ) then
							if( IupGetAttributeId( tree, "KIND", i )[0] = "BRANCH" ) then
								if( nowState[0] = "EXPANDED" ) then 
									IupSetAttributeId( tree, "STATE", i, "COLLAPSED" )
								else
									IupSetAttributeId( tree, "STATE", i, "EXPANDED" )
								end if
							end if
						end if
					end if
				next
			end if
		end if
	end if
	
	return IUP_DEFAULT
end function


private function prButton_ACTION cdecl ( ih as Ihandle ptr ) as integer

	outlineObject.prIndex += 1
	if( outlineObject.prIndex > 3 ) then outlineObject.prIndex = 0
	
	select case outlineObject.prIndex
		case 0
			IupSetAttribute( ih, "IMAGE", "pFB_Outline_pr" )
		case 1
			IupSetAttribute( ih, "IMAGE", "pFB_Outline_p" )
		case 2
			IupSetAttribute( ih, "IMAGE", "pFB_Outline_r" )
		case else 
			IupSetAttribute( ih, "IMAGE", "pFB_Outline_nopr" )
	end select
	
	outlineObject.createTree( root )

	return IUP_DEFAULT
end function


private function mainDialog_DESTROY_CB cdecl ( ih as Ihandle ptr ) as integer
	
	dim as string screenPosition	= IupGetAttribute( ih, "SCREENPOSITION" )[0]
	dim as string dialogRASTERSIZE	= IupGetAttribute( ih, "RASTERSIZE" )[0]
	
	open "plugins/poseidonOutline.cfg" For output As #1
	print #1, screenPosition
	print #1, dialogRASTERSIZE
	print #1, outlineObject.foreColor
	print #1, outlineObject.backColor
	close #1
	

	return IUP_DEFAULT
end function


private function COutline_List_ACTION cdecl ( ih as Ihandle ptr, text as zstring ptr, item as integer, state as integer ) as integer
	
	if( state = 1 ) then
		if( IupGetInt( ih, "COUNT" ) > 0 ) then
			if( item > 0 ) then
				#ifdef __fb_win32__
					IupSetAttributeId( outlineObject.getTreeHandle, "MARKED", outlineObject.listIDs(item), "YES" )
					
					if( KeyPressNum = -1 ) then
						KeyPressNum = 0
						return gotoLine( outlineObject.listIDs(item) )
					end if
				#else
					IupSetInt( outlineObject.getTreeHandle, "VALUE", outlineObject.listIDs(item) )
					return gotoLine( outlineObject.listIDs(item) )
				#endif
			end if
		end if
	end if
	
	return IUP_DEFAULT
end function	


private function COutline_List_K_ANY cdecl ( ih as Ihandle ptr, c as integer ) as integer
	
	if( c = K_CR ) then
	
		dim as integer treeNodeID = IupGetInt( outlineObject.getTreeHandle, "VALUE" ) 
		dim as string treeNodeFullTitle = IupGetAttributeId( outlineObject.getTreeHandle, "TITLE", treeNodeID )[0]
		dim as integer openParen = instr( treeNodeFullTitle, "(" )
		if( openParen < 1 ) then openParen = instr( treeNodeFullTitle, " " )
		if( openParen > 1 ) then
			
			dim as string treeNodeTitle = left( treeNodeFullTitle, openParen - 1 )
			if( treeNodeTitle = IupGetAttribute( ih, "VALUE" )[0] ) then

				return gotoLine( outlineObject.listIDs(treeNodeID) )
			else

				IupSetAttribute( ih, "REMOVEITEM", "ALL" )
				#ifdef __FB_LINUX__
					IupSetAttribute( ih, "1", "" ) 'Keep the arrow ACTIVE
				#endif
			end if
		end if
	elseif( KeyPressNum = K_ESC ) then

		IupSetAttribute( ih, "REMOVEITEM", "ALL" )
		#ifdef __FB_LINUX__
			IupSetAttribute( ih, "1", "" ) 'Keep the arrow ACTIVE
		#endif
	end if	

	return IUP_DEFAULT
end function


private function COutline_List_DROPDOWN_CB cdecl ( ih as Ihandle ptr, state as integer ) as integer

	if( outlineObject.getTreeHandle = null ) then return IUP_DEFAULT
	if( IupGetInt( outlineObject.getTreeHandle, "COUNT" ) = 0 ) then return IUP_DEFAULT
	
	' ListBox open
	if( state = 1 ) then
	
		dim as string	editText = lcase( trim( IupGetAttribute( ih, "VALUE" )[0] ) )
		dim as string	imageName
		dim as integer	count
	
		IupSetAttribute( ih, "REMOVEITEM", "ALL" )
		
		for i as integer = 1 to IupGetInt( outlineObject.getTreeHandle, "COUNT" ) - 1

			dim as CASTnode ptr _node = cast( CASTnode ptr, IupGetAttributeId( outlineObject.getTreeHandle, "USERDATA", i ) )
			if( _node <> null ) then
			
				if( instr( lcase( _node->name_ ), editText ) > 0 ) then

					IupSetAttribute( ih, "APPENDITEM", _node->name_ )
					count += 1
					outlineObject.listIDs(count) = i
					imageName = outlineObject.getImageName( _node )
					if( len( imageName ) > 0 ) then IupSetAttributeId( ih, "IMAGE", IupGetInt( ih, "COUNT" ), imageName )
				end if
			end if
		next
		
		if( IupGetInt( ih, "COUNT" ) = 0 ) then
			#ifdef __FB_LINUX__
				IupSetAttribute( ih, "1", "" ) 'Keep the arrow ACTIVE
			#endif
			return IUP_DEFAULT
		end if
		
		#ifdef __fb_win32__
			IupSetAttribute( ih, "VALUE", IupGetAttribute( ih, "1" ) )
			IupSetAttributeId( outlineObject.getTreeHandle, "MARKED", outlineObject.listIDs(1), "YES" )
		#else
			IupSetAttribute( ih, "VALUE", IupGetAttribute( ih, "1" ) )
			IupSetInt( outlineObject.getTreeHandle, "VALUE", outlineObject.listIDs(1) )
		#endif
	else
	
		if( KeyPressNum = K_CR ) then
			
			#ifdef __fb_win32__
				dim as integer treeNodeID = IupGetInt( outlineObject.getTreeHandle, "VALUE" ) 
				return gotoLine( treeNodeID )
			#endif
		elseif( KeyPressNum = K_ESC ) then

			IupSetAttribute( ih, "REMOVEITEM", "ALL" )
			#ifdef __FB_LINUX__
				IupSetAttribute( ih, "1", "" ) 'Keep the arrow ACTIVE
			#endif
		end if
	end if
	
	return IUP_DEFAULT
end function


private function item_testproc_callback Cdecl (ih As Ihandle Ptr) As Integer
	
	if( THIS_MAINDIALOG_HANDLE = 0 ) then
	
		if( IupGetChildCount(TGlobalVariablesDll->tabs) > 1 ) then
		
			'Open cfg
			dim as string screenPos = "600,100", rasterSize = "400x600", fore = "0 0 0", back = "255 255 255"
			
			open "plugins/poseidonOutline.cfg" For input As #1
			dim as integer fileLength = lof( 1 )
			if fileLength > 0 then
				Line Input #1, screenPos
				Line Input #1, rasterSize
				Line Input #1, fore
				Line Input #1, back
			end if
			close #1
			outlineObject.setPublicParams( screenPos, rasterSize, fore, back )		
		
			'Create Layout
			dim as Ihandle ptr collapseButton = IupButton( "", "" )
			IupSetAttributes( collapseButton, "CANFOCUS=NO,FLAT=YES,IMAGE=pFB_Outline_collapse" )
			IupSetCallback( collapseButton, "ACTION", cast( Icallback, @collapseButton_ACTION ) )
			
			dim as Ihandle ptr prButton = IupButton( "", "" )
			IupSetAttributes( prButton, "CANFOCUS=NO,FLAT=YES,IMAGE=pFB_Outline_pr" )
			IupSetCallback( prButton, "ACTION", cast( Icallback, @prButton_ACTION ) )
		
			dim as Ihandle ptr valOPACITY = IupVal("")
			IupSetAttributes( valOPACITY, "MIN=100,MAX=255,VALUE=200,CANFOCUS=NO,SIZE=48x12" )
			IupSetCallback( valOPACITY, "VALUECHANGED_CB", cast( Icallback, @valOPACITY_VALUECHANGED_CB ) )
			
			dim as Ihandle ptr outlineTreeNodeList = IupList( null )
			IupSetAttributes( outlineTreeNodeList, "CANFOCUS =NO,DROPDOWN=YES,SHOWIMAGE=YES,EDITBOX=YES,EXPAND=HORIZONTAL,DROPEXPAND=NO,VISIBLEITEMS=8" )
			#ifdef __fb_win32__
				IupSetAttribute( outlineTreeNodeList, "FONT", "Consolas, 9" )
			#else
				IupSetAttribute( outlineTreeNodeList, "FONT", "Monospace, 9" )
			#endif			
			IupSetCallback( outlineTreeNodeList, "DROPDOWN_CB",cast(Icallback, @COutline_List_DROPDOWN_CB ) )
			IupSetCallback( outlineTreeNodeList, "ACTION",cast(Icallback, @COutline_List_ACTION ) )
			IupSetCallback( outlineTreeNodeList, "K_ANY",cast(Icallback, @COutline_List_K_ANY ) )
			#ifdef __FB_LINUX__
				IupSetAttribute( outlineTreeNodeList, "1", "" ) 'Keep the arrow ACTIVE
			#else
				IupSetGlobal( "INPUTCALLBACKS", "YES" )
				IupSetFunction( "GLOBALBUTTON_CB", cast(Icallback, @GlobalButton_CB ) )
				IupSetFunction( "GLOBALKEYPRESS_CB", cast(Icallback, @GlobalKeyPress_CB ) )
			#endif
			
			dim as Ihandle ptr hBox = IupHbox( collapseButton, prButton, valOPACITY, outlineTreeNodeList, null )
			IupSetAttribute( hBox, "ALIGNMENT", "ACENTER" )

			dim as Ihandle ptr vBox = IupVbox( hBox, outlineObject.getTreeHandle, null )
		
			THIS_MAINDIALOG_HANDLE = IupDialog( vBox )
			IupSetAttributes( THIS_MAINDIALOG_HANDLE, "TITLE=poseidonOutline,MAXBOX=NO,MINBOX=NO,OPACITY=200,ZORDER=TOP,ICON=pFB_Outline,SHRINK=YES" )
			IupSetAttribute( THIS_MAINDIALOG_HANDLE, "RASTERSIZE", outlineObject.rasterSize )
			IupSetAttributeHandle( THIS_MAINDIALOG_HANDLE, "PARENTDIALOG", TGlobalVariablesDLL->dlg )
			IupSetCallback( THIS_MAINDIALOG_HANDLE, "DESTROY_CB", cast( Icallback, @mainDialog_DESTROY_CB ) )
			

			
			IupShowXY( THIS_MAINDIALOG_HANDLE, outlineObject.dialogX, outlineObject.dialogY )

			dim as Ihandle ptr iupSci	= getActiveDoucumentPtr()
			dim as string documentTitle	= getActiveDoucumentTiTle()
			
			if( iupSci <> null AND documentTitle <> "" ) then
			
				if( documentTitle <> tabTitle ) then

					tabTitle = documentTitle
					dim as zstring ptr document = IupGetAttribute( iupSci, "VALUE" )

					dim as TokenUnit ptr pTokens = tokenScanner.scan( document[0] )
					if( pTokens <> null ) then

						parser.setTokenData( pTokens, tokenScanner.getTokenCount )
						root = parser.parse( tabTitle )
						outlineObject.createTree( root )
					else
						outlineObject.cleanTree()
					end if
					
					OutlineTIMER = IupTimer()
					IupSetAttributes( OutlineTIMER, "TIME=500,RUN=YES" )
					IupSetCallback( OutlineTIMER, "ACTION_CB", cast( Icallback, @OutlineTIMER_ACTION_CB ) )
					
				end if
			else
				
				outlineObject.cleanTree
			end if

		end if
	else
	
		if( TGlobalVariablesDll->tabs <> null ) then
			if( IupGetChildCount(TGlobalVariablesDll->tabs) > 1 ) then

				IupShow( THIS_MAINDIALOG_HANDLE )
				
				dim as integer VALUEPOS = IupGetInt( TGlobalVariablesDll->tabs, "VALUEPOS" )
				if( VALUEPOS > 0 ) then
				
					dim as Ihandle ptr iupSci = TGlobalVariablesDll->tTabsOptions(VALUEPOS).multitext 'Get Active Doc Tab(iupscintilla handle)
					if( iupSci <> null ) then
						
						IupSetAttribute( OutlineTIMER, "RUN", "NO" )
						
						tabTitle = IupGetAttributeId( TGlobalVariablesDll->tabs, "TABTITLE", VALUEPOS )[0]
						dim as zstring ptr document = IupGetAttribute( iupSci, "VALUE" )
						
						dim as TokenUnit ptr pTokens = tokenScanner.scan( document[0] )
						if( pTokens <> null ) then

							parser.setTokenData( pTokens, tokenScanner.getTokenCount )
							root = parser.parse( tabTitle )
							outlineObject.createTree( root )
						else
							outlineObject.cleanTree()
						end if
						
						IupSetAttribute( OutlineTIMER, "RUN", "YES" )
					end if
				end if	
			else
				Iuphide( THIS_MAINDIALOG_HANDLE )
			end if
		end if
	end if
	
	return IUP_DEFAULT
End Function





'=============================
'=============================
' DLL main entry point
function InitPluginProc(TGlobalVariables as T_GlobalVariables Ptr , pszDimErrors() as zstring ptr , bEncodingBool() as byte , szEncodingString as any ptr) as Integer export
	
	if TGlobalVariables = 0 then return 0
	TGlobalVariablesDll = TGlobalVariables
	
	dim as iHandle ptr hToolbar = TGlobalVariablesDLL->toolBar
	if hToolbar = 0 then return 0
	
	dim as IHandle ptr btn = IupButton(NULL, NULL)
	
	IupSetAttribute(btn, "IMAGE", "pFB_Outline")
	IupSetAttribute(btn, "FLAT", "Yes")
	IupSetCallback(btn, "ACTION", cast(any ptr , @item_testproc_callback()))
	IupSetAttribute(btn, "TIP", "poseidon Outline")
	IupSetAttribute(btn, "CANFOCUS", "NO")
	
	'____________________________________________________________________________________________________
	'                                                                                                    |
	IupAppend(hToolbar , btn) ' added button in toolbar                                                  |
	'____________________________________ or ____________________________________________________________|
	'                                                                                                    |
	'  IupInsert(hToolbar , IupGetChild(hToolbar,5), btn) ' insert button in toolbar in the 5+1 position |
	'____________________________________________________________________________________________________|
	
	IupMap(btn)
	IupRefreshChildren(hToolbar)

	return 1	
	
End Function