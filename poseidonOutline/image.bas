#include once "image.bi"

constructor CImage()

	IupSetHandle( "pFB_Outline_function", load_image_fun_public )
	IupSetHandle( "pFB_Outline_sub", load_image_sub_public )
	IupSetHandle( "pFB_Outline_function_protected", load_image_fun_protected )
	IupSetHandle( "pFB_Outline_function_private", load_image_fun_private )
	IupSetHandle( "pFB_Outline_sub_protected", load_image_sub_protected )
	IupSetHandle( "pFB_Outline_sub_private", load_image_sub_private )
	IupSetHandle( "pFB_Outline_variable_array", load_image_variable_array_obj )
	IupSetHandle( "pFB_Outline_variable", load_image_variable_obj )

	IupSetHandle( "pFB_Outline_ctor", load_image_ctor )
	IupSetHandle( "pFB_Outline_dtor", load_image_dtor )
	IupSetHandle( "pFB_Outline_class", load_image_class_obj )
	IupSetHandle( "pFB_Outline_struct", load_image_struct_obj )
	IupSetHandle( "pFB_Outline_property",load_image_property_obj )
	IupSetHandle( "pFB_Outline_property_var", load_image_property_var )
	IupSetHandle( "pFB_Outline_operator", load_image_operator )
	IupSetHandle( "pFB_Outline_variable_protected", load_image_variable_protected_obj )
	IupSetHandle( "pFB_Outline_variable_array_protected", load_image_variable_array_protected_obj )
	IupSetHandle( "pFB_Outline_variable_array_private", load_image_variable_array_private_obj )
	IupSetHandle( "pFB_Outline_variable_private", load_image_variable_private_obj )
	IupSetHandle( "pFB_Outline_enummember", load_image_enum_member_obj )
	IupSetHandle( "pFB_Outline_enum", load_image_enum_obj )
	IupSetHandle( "pFB_Outline_alias", load_image_alias )
	IupSetHandle( "pFB_Outline_union", load_image_union_obj )
	IupSetHandle( "pFB_Outline_namespace", load_image_namespace_obj )
	IupSetHandle( "pFB_Outline_macro", load_image_macro )
	IupSetHandle( "pFB_Outline_scope", load_image_scope )
	IupSetHandle( "pFB_Outline_define_fun", load_image_define_fun )
	IupSetHandle( "pFB_Outline_define_var", load_image_define_var )
	IupSetHandle( "pFB_Outline_with", load_image_with )

	IupSetHandle( "pFB_Outline_openfile", load_image_openfile )
	
	IupSetHandle( "pFB_Outline", load_image_outline )
	IupSetHandle( "pFB_Outline_p", load_image_show_p )
	IupSetHandle( "pFB_Outline_r", load_image_show_r )
	IupSetHandle( "pFB_Outline_pr", load_image_show_pr )
	IupSetHandle( "pFB_Outline_nopr", load_image_show_nopr )
	IupSetHandle( "pFB_Outline_collapse", load_image_collapse )
end constructor

function CImage.load_image_dtor() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "118 19 12")
  IupSetAttribute(image, "2", "159 20 15")
  IupSetAttribute(image, "3", "196 33 21")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "244 169 168")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_openfile() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,_
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,_
    7, 7, 7, 7, 7, 7, 7, 7, 1, 6, 6, 6, 1, 7, 7, 7,_
    7, 7, 7, 7, 7, 7, 7, 7, 0, 1, 7, 7, 6, 1, 0, 7,_
    7, 7, 4, 4, 4, 4, 7, 7, 7, 7, 7, 7, 1, 0, 0, 7,_
    7, 2, 7, 7, 7, 7, 2, 7, 7, 7, 7, 7, 0, 0, 0, 7,_
    4, 3, 3, 3, 5, 5, 5, 4, 4, 4, 4, 4, 7, 7, 7, 7,_
    4, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 4, 7, 7, 7, 7,_
    4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 7, 7, 7, 7,_
    4, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 7,_
    4, 3, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 3, 5, 4, 7,_
    2, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 3, 5, 4, 7, 7,_
    2, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 7, 7, 7,_
    2, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 7, 7, 7, 7,_
    7, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 7, 7, 7, 7, 7,_
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "84 109 146")
  IupSetAttribute(image, "1", "193 201 210")
  IupSetAttribute(image, "2", "158 101 35")
  IupSetAttribute(image, "3", "252 226 158")
  IupSetAttribute(image, "4", "181 122 47")
  IupSetAttribute(image, "5", "246 208 130")
  IupSetAttribute(image, "6", "130 145 166")
  IupSetAttribute(image, "7", "BGCOLOR")

  return image
end function

function CImage.load_image_fun_protected() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 6, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 7, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 7, 7, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "232 116 1")
  IupSetAttribute(image, "2", "238 136 5")
  IupSetAttribute(image, "3", "242 153 12")
  IupSetAttribute(image, "4", "250 250 250")
  IupSetAttribute(image, "5", "252 228 190")
  IupSetAttribute(image, "6", "242 154 15")
  IupSetAttribute(image, "7", "255 255 255")

  return image
end function

function CImage.load_image_struct_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,_
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,_
    6, 6, 6, 6, 6, 5, 2, 4, 4, 4, 2, 3, 6, 6, 6, 6,_
    6, 6, 6, 6, 4, 2, 2, 1, 1, 7, 2, 2, 4, 6, 6, 6,_
    6, 6, 6, 4, 2, 1, 1, 1, 2, 1, 7, 1, 2, 4, 6, 6,_
    6, 6, 3, 2, 7, 2, 0, 0, 0, 0, 0, 1, 1, 2, 3, 6,_
    6, 6, 2, 2, 1, 1, 1, 1, 0, 7, 1, 2, 1, 2, 2, 6,_
    6, 6, 4, 7, 2, 1, 2, 1, 0, 1, 1, 7, 1, 1, 4, 6,_
    6, 6, 4, 1, 1, 7, 1, 1, 0, 1, 7, 1, 2, 7, 4, 6,_
    6, 6, 4, 7, 2, 1, 1, 7, 0, 1, 1, 2, 1, 1, 4, 6,_
    6, 6, 2, 2, 1, 7, 1, 1, 0, 1, 7, 2, 1, 2, 2, 6,_
    6, 6, 3, 2, 1, 2, 7, 1, 0, 1, 1, 1, 7, 4, 3, 6,_
    6, 6, 6, 4, 2, 1, 1, 1, 2, 1, 7, 1, 2, 4, 6, 6,_
    6, 6, 6, 6, 4, 2, 4, 1, 1, 2, 2, 4, 4, 6, 6, 6,_
    6, 6, 6, 6, 6, 5, 2, 4, 4, 4, 2, 3, 6, 6, 6, 6,_
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "255 255 255")
  IupSetAttribute(image, "1", "52 170 194")
  IupSetAttribute(image, "2", "64 130 190")
  IupSetAttribute(image, "3", "156 170 204")
  IupSetAttribute(image, "4", "59 86 158")
  IupSetAttribute(image, "5", "156 214 204")
  IupSetAttribute(image, "6", "BGCOLOR")
  IupSetAttribute(image, "7", "100 170 201")

  return image
end function

function CImage.load_image_variable_private_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 3, 4, 4, 4, 4, 4, 3, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 3, 4, 6, 6, 6, 4, 3, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 7, 6, 5, 5, 5, 6, 7, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 7, 4, 2, 2, 2, 4, 7, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 4, 4, 4, 4, 4, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "204 27 44")
  IupSetAttribute(image, "2", "228 98 100")
  IupSetAttribute(image, "3", "204 68 60")
  IupSetAttribute(image, "4", "238 141 136")
  IupSetAttribute(image, "5", "244 78 92")
  IupSetAttribute(image, "6", "244 118 132")
  IupSetAttribute(image, "7", "204 46 52")
  
  return image
end function

function CImage.load_image_sub_private() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "182 11 1")
  IupSetAttribute(image, "2", "239 13 1")
  IupSetAttribute(image, "3", "254 84 75")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "255 203 200")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_variable_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 2, 1, 5, 2, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 4, 2, 7, 3, 2, 4, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 2, 4, 7, 7, 7, 7, 4, 2, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 6, 6, 6, 6, 6, 6, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 5, 2, 2, 2, 2, 2, 2, 5, 0, 0, 0, 0,_
    0, 0, 0, 0, 2, 4, 6, 6, 6, 6, 4, 2, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 4, 4, 3, 6, 4, 4, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 2, 1, 5, 2, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "4 130 76")
  IupSetAttribute(image, "2", "110 177 127")
  IupSetAttribute(image, "3", "152 194 144")
  IupSetAttribute(image, "4", "74 160 108")
  IupSetAttribute(image, "5", "36 146 92")
  IupSetAttribute(image, "6", "132 188 132")
  IupSetAttribute(image, "7", "172 210 156")

  return image
end function

function CImage.load_image_class_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 6, 7, 7, 7, 7, 7, 6, 0, 0, 0, 0,_
    0, 0, 0, 0, 7, 7, 1, 1, 1, 1, 1, 7, 7, 0, 0, 0,_
    0, 0, 0, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7, 0, 0,_
    0, 0, 2, 7, 1, 1, 3, 4, 4, 4, 3, 1, 1, 7, 2, 0,_
    0, 0, 1, 1, 1, 3, 4, 6, 1, 3, 4, 1, 1, 1, 7, 0,_
    0, 0, 7, 1, 1, 6, 4, 1, 1, 1, 1, 1, 1, 1, 5, 0,_
    0, 0, 7, 1, 1, 6, 4, 1, 1, 1, 1, 1, 1, 1, 5, 0,_
    0, 0, 7, 1, 1, 6, 4, 1, 1, 1, 1, 1, 1, 1, 5, 0,_
    0, 0, 1, 1, 1, 3, 4, 6, 1, 3, 4, 1, 1, 7, 7, 0,_
    0, 0, 2, 7, 1, 1, 3, 4, 4, 4, 3, 1, 1, 5, 2, 0,_
    0, 0, 0, 7, 1, 1, 1, 1, 1, 1, 1, 1, 7, 7, 0, 0,_
    0, 0, 0, 0, 7, 7, 1, 1, 1, 1, 7, 5, 7, 0, 0, 0,_
    0, 0, 0, 0, 0, 2, 7, 5, 5, 5, 7, 2, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "60 158 63")
  IupSetAttribute(image, "2", "188 190 156")
  IupSetAttribute(image, "3", "124 190 108")
  IupSetAttribute(image, "4", "252 254 252")
  IupSetAttribute(image, "5", "60 94 60")
  IupSetAttribute(image, "6", "197 222 188")
  IupSetAttribute(image, "7", "60 126 92")

  return image
end function

function CImage.load_image_union_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 4, 7, 7, 7, 7, 7, 4, 0, 0, 0, 0,_
    0, 0, 0, 0, 7, 7, 6, 5, 5, 5, 6, 7, 7, 0, 0, 0,_
    0, 0, 0, 7, 6, 4, 4, 4, 4, 4, 4, 4, 6, 7, 0, 0,_
    0, 0, 4, 7, 4, 4, 2, 2, 4, 2, 2, 4, 4, 7, 4, 0,_
    0, 0, 7, 6, 4, 4, 0, 3, 4, 3, 0, 4, 4, 6, 7, 0,_
    0, 0, 1, 5, 4, 4, 0, 3, 4, 3, 0, 4, 4, 5, 7, 0,_
    0, 0, 1, 5, 4, 4, 0, 3, 4, 3, 0, 4, 4, 5, 7, 0,_
    0, 0, 1, 5, 4, 4, 0, 3, 4, 3, 0, 4, 4, 5, 7, 0,_
    0, 0, 7, 6, 4, 4, 0, 3, 2, 3, 0, 4, 4, 6, 7, 0,_
    0, 0, 4, 1, 4, 4, 2, 0, 0, 0, 2, 4, 4, 7, 4, 0,_
    0, 0, 0, 7, 6, 4, 4, 4, 4, 4, 4, 4, 6, 7, 0, 0,_
    0, 0, 0, 0, 7, 1, 6, 5, 5, 5, 6, 7, 7, 0, 0, 0,_
    0, 0, 0, 0, 0, 4, 7, 1, 1, 1, 7, 4, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "124 126 44")
  IupSetAttribute(image, "2", "188 190 92")
  IupSetAttribute(image, "3", "156 158 92")
  IupSetAttribute(image, "4", "212 214 172")
  IupSetAttribute(image, "5", "212 214 124")
  IupSetAttribute(image, "6", "172 170 124")
  IupSetAttribute(image, "7", "172 170 84")

  return image
end function

function CImage.load_image_variable_array_private_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0,_
    0, 1, 0, 0, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 1, 0,_
    1, 1, 0, 0, 3, 4, 4, 4, 4, 4, 3, 0, 0, 0, 1, 1,_
    1, 1, 0, 0, 3, 4, 6, 6, 6, 4, 3, 0, 0, 0, 1, 1,_
    1, 1, 0, 0, 7, 6, 5, 5, 5, 6, 7, 0, 0, 0, 1, 1,_
    1, 1, 0, 0, 7, 4, 2, 2, 2, 4, 7, 0, 0, 0, 1, 1,_
    1, 1, 0, 0, 1, 4, 4, 4, 4, 4, 1, 0, 0, 0, 1, 1,_
    0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0,_
    0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "204 26 44")
  IupSetAttribute(image, "2", "228 98 100")
  IupSetAttribute(image, "3", "204 68 60")
  IupSetAttribute(image, "4", "238 141 136")
  IupSetAttribute(image, "5", "244 78 92")
  IupSetAttribute(image, "6", "244 118 132")
  IupSetAttribute(image, "7", "204 46 52")

  return image
end function

function CImage.load_image_alias() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "0 128 192")
  IupSetAttribute(image, "2", "192 220 192")
  IupSetAttribute(image, "3", "128 128 0")
  IupSetAttribute(image, "4", "0 0 128")
  IupSetAttribute(image, "5", "128 0 128")
  IupSetAttribute(image, "6", "0 128 128")
  IupSetAttribute(image, "7", "128 0 0")

  return image
end function

function CImage.load_image_sub_public() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 6, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "38 98 38")
  IupSetAttribute(image, "2", "51 130 51")
  IupSetAttribute(image, "3", "63 159 63")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "191 230 191")
  IupSetAttribute(image, "6", "64 160 64")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_variable_protected_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 1, 2, 2, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 2, 3, 3, 3, 3, 2, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 2, 3, 3, 3, 3, 2, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 1, 2, 2, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "203 115 0")
  IupSetAttribute(image, "2", "231 151 1")
  IupSetAttribute(image, "3", "241 188 12")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_operator() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 4, 4, 4, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 6, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 4, 4, 4, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "231 166 1")
  IupSetAttribute(image, "2", "237 187 5")
  IupSetAttribute(image, "3", "241 203 12")
  IupSetAttribute(image, "4", "249 249 249")
  IupSetAttribute(image, "5", "251 241 189")
  IupSetAttribute(image, "6", "241 204 14")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_variable_array_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,_
    0, 1, 1, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 1, 1, 0,_
    0, 1, 0, 0, 0, 2, 3, 3, 3, 3, 4, 0, 0, 0, 1, 0,_
    2, 1, 0, 0, 2, 3, 3, 3, 3, 3, 1, 4, 0, 0, 1, 4,_
    2, 1, 0, 0, 2, 3, 3, 3, 3, 3, 1, 4, 0, 0, 1, 4,_
    2, 1, 0, 0, 2, 3, 3, 3, 3, 3, 1, 4, 0, 0, 1, 4,_
    2, 1, 0, 0, 2, 3, 3, 3, 3, 1, 1, 4, 0, 0, 1, 4,_
    2, 1, 0, 0, 0, 4, 1, 1, 1, 1, 4, 0, 0, 0, 1, 4,_
    0, 1, 0, 0, 0, 0, 4, 4, 4, 4, 0, 0, 0, 0, 1, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0,_
    0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "127 159 95")
  IupSetAttribute(image, "2", "63 127 63")
  IupSetAttribute(image, "3", "159 191 95")
  IupSetAttribute(image, "4", "63 95 63")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_define_var() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 3, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 4, 3, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 4, 3, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 4, 3, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 2, 2, 2, 2, 4, 4, 3, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 3, 4, 4, 4, 4, 3, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "123 123 123")
  IupSetAttribute(image, "2", "193 193 193")
  IupSetAttribute(image, "3", "90 90 90")
  IupSetAttribute(image, "4", "170 170 170")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_macro() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 1, 0, 0,_
    0, 0, 1, 1, 2, 3, 4, 2, 2, 2, 4, 3, 2, 1, 1, 0,_
    0, 0, 1, 1, 2, 4, 4, 4, 2, 4, 4, 4, 2, 1, 1, 0,_
    0, 0, 1, 2, 2, 4, 3, 4, 3, 4, 3, 4, 2, 2, 1, 0,_
    0, 0, 1, 2, 2, 4, 3, 2, 4, 2, 3, 4, 2, 2, 1, 0,_
    0, 0, 1, 2, 2, 4, 3, 2, 4, 2, 3, 4, 2, 2, 1, 0,_
    0, 0, 1, 1, 2, 4, 3, 2, 2, 2, 3, 4, 2, 1, 1, 0,_
    0, 0, 1, 1, 2, 4, 3, 2, 2, 2, 3, 4, 2, 1, 1, 0,_
    0, 0, 0, 1, 1, 4, 3, 2, 2, 2, 3, 4, 1, 1, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "88 88 88")
  IupSetAttribute(image, "2", "121 121 121")
  IupSetAttribute(image, "3", "215 215 215")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_fun_private() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 4, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "182 11 1")
  IupSetAttribute(image, "2", "239 13 1")
  IupSetAttribute(image, "3", "254 84 75")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "255 203 200")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_with() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 3, 1, 4, 4, 4, 1, 3, 0, 0, 0, 0,_
    0, 0, 0, 0, 4, 1, 1, 7, 7, 7, 1, 1, 4, 0, 0, 0,_
    0, 0, 0, 4, 1, 7, 7, 7, 7, 7, 7, 7, 1, 4, 0, 0,_
    0, 0, 3, 1, 6, 6, 7, 1, 6, 7, 7, 6, 7, 1, 3, 0,_
    0, 0, 1, 1, 6, 5, 7, 2, 5, 6, 6, 5, 7, 1, 1, 0,_
    0, 0, 4, 7, 7, 5, 7, 5, 5, 3, 3, 2, 7, 7, 4, 0,_
    0, 0, 4, 7, 7, 5, 3, 5, 3, 2, 2, 3, 7, 7, 4, 0,_
    0, 0, 4, 7, 7, 2, 5, 2, 1, 5, 5, 6, 7, 7, 4, 0,_
    0, 0, 1, 1, 7, 3, 5, 3, 7, 5, 5, 7, 7, 1, 1, 0,_
    0, 0, 3, 1, 7, 1, 3, 1, 7, 6, 3, 7, 7, 1, 3, 0,_
    0, 0, 0, 4, 1, 7, 7, 7, 7, 7, 7, 7, 1, 4, 0, 0,_
    0, 0, 0, 0, 4, 1, 1, 7, 7, 7, 1, 1, 4, 0, 0, 0,_
    0, 0, 0, 0, 0, 3, 1, 4, 4, 4, 1, 3, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "108 130 244")
  IupSetAttribute(image, "2", "209 207 252")
  IupSetAttribute(image, "3", "178 178 252")
  IupSetAttribute(image, "4", "92 110 244")
  IupSetAttribute(image, "5", "245 241 252")
  IupSetAttribute(image, "6", "148 145 249")
  IupSetAttribute(image, "7", "118 112 244")

  return image
end function

function CImage.load_image_property_var() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 1,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 0, 1,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 4, 1, 0, 0, 0,_
    0, 0, 0, 0, 0, 5, 6, 6, 6, 6, 1, 0, 1, 0, 0, 0,_
    0, 0, 0, 0, 5, 6, 6, 6, 6, 6, 7, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 5, 6, 6, 6, 6, 6, 7, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 5, 6, 6, 6, 6, 6, 7, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 5, 6, 6, 6, 6, 7, 7, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 7, 7, 7, 7, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "94 63 70")
  IupSetAttribute(image, "2", "206 206 206")
  IupSetAttribute(image, "3", "201 173 180")
  IupSetAttribute(image, "4", "224 207 211")
  IupSetAttribute(image, "5", "126 63 77")
  IupSetAttribute(image, "6", "190 94 181")
  IupSetAttribute(image, "7", "158 95 141")

  return image
end function

function CImage.load_image_variable_array_protected_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,_
    0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0,_
    0, 0, 1, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 0,_
    0, 1, 1, 0, 0, 0, 2, 1, 1, 2, 0, 0, 0, 2, 2, 0,_
    0, 1, 1, 0, 0, 2, 1, 1, 1, 1, 2, 0, 0, 2, 2, 0,_
    0, 1, 1, 0, 2, 1, 3, 3, 3, 3, 1, 2, 0, 2, 2, 0,_
    0, 1, 1, 0, 2, 1, 3, 3, 3, 3, 1, 2, 0, 2, 2, 0,_
    0, 1, 1, 0, 0, 2, 1, 1, 1, 1, 2, 0, 0, 2, 2, 0,_
    0, 0, 1, 0, 0, 0, 2, 1, 1, 2, 0, 0, 0, 2, 0, 0,_
    0, 0, 1, 1, 0, 0, 0, 2, 2, 0, 0, 0, 2, 2, 0, 0,_
    0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "231 151 1")
  IupSetAttribute(image, "2", "203 115 0")
  IupSetAttribute(image, "3", "241 188 12")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_define_fun() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "61 61 61")
  IupSetAttribute(image, "2", "121 121 121")
  IupSetAttribute(image, "3", "180 180 180")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "215 215 215")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_fun_public() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 6, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 7, 6, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 4, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "38 98 38")
  IupSetAttribute(image, "2", "51 130 51")
  IupSetAttribute(image, "3", "63 159 63")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "191 230 191")
  IupSetAttribute(image, "6", "64 160 64")
  IupSetAttribute(image, "7", "64 160 128")

  return image
end function

function CImage.load_image_enum_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 2, 6, 6, 4, 6, 6, 2, 0, 0, 0, 0, 0,_
    0, 0, 0, 7, 4, 1, 6, 6, 6, 1, 4, 7, 0, 0, 0, 0,_
    0, 0, 7, 4, 7, 3, 3, 3, 3, 3, 1, 4, 7, 0, 0, 0,_
    0, 2, 4, 7, 7, 5, 5, 5, 5, 5, 3, 6, 4, 2, 0, 0,_
    0, 6, 6, 6, 6, 5, 5, 2, 3, 3, 1, 6, 6, 1, 0, 0,_
    0, 6, 6, 6, 6, 5, 5, 7, 6, 6, 6, 1, 6, 6, 0, 0,_
    0, 4, 4, 4, 4, 5, 5, 5, 5, 5, 7, 4, 4, 4, 0, 0,_
    0, 6, 4, 4, 4, 5, 5, 3, 7, 7, 6, 4, 4, 6, 0, 0,_
    0, 1, 4, 6, 6, 5, 5, 3, 6, 6, 6, 6, 6, 1, 0, 0,_
    0, 2, 4, 6, 6, 5, 5, 5, 5, 5, 3, 6, 4, 2, 0, 0,_
    0, 0, 7, 4, 6, 3, 3, 3, 3, 3, 1, 4, 7, 0, 0, 0,_
    0, 0, 0, 7, 4, 6, 6, 6, 6, 6, 4, 7, 0, 0, 0, 0,_
    0, 0, 0, 0, 2, 6, 4, 4, 4, 6, 2, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "174 136 94")
  IupSetAttribute(image, "2", "227 209 194")
  IupSetAttribute(image, "3", "200 177 153")
  IupSetAttribute(image, "4", "148 97 47")
  IupSetAttribute(image, "5", "252 254 252")
  IupSetAttribute(image, "6", "161 119 76")
  IupSetAttribute(image, "7", "185 155 119")

  return image
end function

function CImage.load_image_ctor() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 4, 3, 6, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 4, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 4, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 5, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "38 65 96")
  IupSetAttribute(image, "2", "50 86 128")
  IupSetAttribute(image, "3", "63 105 157")
  IupSetAttribute(image, "4", "189 208 228")
  IupSetAttribute(image, "5", "255 255 255")
  IupSetAttribute(image, "6", "63 106 158")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_namespace_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 7, 1, 1, 1, 1, 1, 2, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 1, 0, 0,_
    0, 0, 2, 1, 2, 5, 6, 2, 4, 2, 5, 3, 2, 1, 2, 0,_
    0, 0, 1, 1, 1, 6, 3, 5, 2, 2, 6, 3, 2, 4, 1, 0,_
    0, 0, 1, 2, 2, 5, 3, 6, 4, 1, 3, 3, 2, 2, 1, 0,_
    0, 0, 1, 1, 2, 6, 3, 2, 5, 2, 3, 3, 2, 1, 1, 0,_
    0, 0, 1, 2, 2, 5, 3, 2, 5, 4, 3, 3, 2, 2, 1, 0,_
    0, 0, 4, 1, 1, 6, 3, 1, 2, 6, 3, 3, 1, 1, 1, 0,_
    0, 0, 7, 1, 2, 5, 3, 2, 2, 4, 3, 3, 2, 1, 2, 0,_
    0, 0, 0, 4, 1, 2, 2, 1, 2, 1, 2, 2, 1, 4, 0, 0,_
    0, 0, 0, 0, 1, 1, 4, 2, 2, 1, 1, 1, 1, 0, 0, 0,_
    0, 0, 0, 0, 0, 7, 1, 1, 1, 1, 4, 2, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "252 170 104")
  IupSetAttribute(image, "2", "246 214 111")
  IupSetAttribute(image, "3", "4 2 4")
  IupSetAttribute(image, "4", "204 170 106")
  IupSetAttribute(image, "5", "52 42 58")
  IupSetAttribute(image, "6", "52 86 73")
  IupSetAttribute(image, "7", "252 214 204")

  return image
end function

function CImage.load_image_sub_protected() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 0,_
    0, 0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 0,_
    0, 0, 5, 1, 6, 3, 3, 3, 3, 3, 3, 3, 6, 1, 5, 0,_
    0, 0, 5, 1, 3, 3, 4, 4, 4, 4, 4, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 7, 3, 4, 2, 2, 2, 2, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 3, 3, 4, 2, 3, 3, 3, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 3, 3, 4, 4, 4, 4, 4, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 3, 3, 3, 3, 3, 2, 4, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 3, 3, 2, 2, 2, 2, 4, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 3, 3, 4, 4, 4, 4, 4, 3, 3, 1, 5, 0,_
    0, 0, 5, 1, 6, 3, 3, 3, 3, 3, 3, 3, 6, 1, 5, 0,_
    0, 0, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 0,_
    0, 0, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "236 138 4")
  IupSetAttribute(image, "2", "252 230 188")
  IupSetAttribute(image, "3", "244 154 12")
  IupSetAttribute(image, "4", "252 251 252")
  IupSetAttribute(image, "5", "236 118 4")
  IupSetAttribute(image, "6", "244 146 12")
  IupSetAttribute(image, "7", "244 158 28")

  return image
end function

function CImage.load_image_property_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 6, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 4, 4, 4, 4, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 5, 5, 5, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 4, 5, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 1, 0,_
    0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "84 38 95")
  IupSetAttribute(image, "2", "112 49 127")
  IupSetAttribute(image, "3", "139 63 156")
  IupSetAttribute(image, "4", "255 255 255")
  IupSetAttribute(image, "5", "218 188 227")
  IupSetAttribute(image, "6", "138 63 157")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_enum_member_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 0, 1, 1, 1, 1, 2, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 3, 0, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 3, 0, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 3, 0, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 3, 3, 0, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 0, 3, 3, 3, 3, 0, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "142 92 36")
  IupSetAttribute(image, "1", "190 161 129")
  IupSetAttribute(image, "2", "147 99 45")
  IupSetAttribute(image, "3", "158 115 67")
  IupSetAttribute(image, "4", "BGCOLOR")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_parameter_obj() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 0, 1, 1, 1, 1, 2, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 3, 2, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 3, 2, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 1, 3, 2, 4, 4, 4, 4,_
    4, 4, 4, 4, 0, 1, 1, 1, 1, 3, 3, 2, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 2, 3, 3, 3, 3, 2, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "132 66 194")
  IupSetAttribute(image, "1", "199 139 255")
  IupSetAttribute(image, "2", "113 46 175")
  IupSetAttribute(image, "3", "173 107 235")
  IupSetAttribute(image, "4", "BGCOLOR")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_scope() as Ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0,_
    0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "255 0 0")
  IupSetAttribute(image, "2", "0 0 0")
  IupSetAttribute(image, "3", "0 0 0")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_outline() as ihandle ptr

  dim as const ubyte imgdata(255) = {_
    4, 4, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 5, 6, 6, 6, 6, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 5, 2, 2, 2, 2, 5, 4, 7, 7, 7, 7, 7, 7, 4,_
    4, 4, 5, 3, 3, 3, 3, 5, 4, 0, 0, 0, 0, 0, 0, 4,_
    4, 4, 5, 1, 1, 1, 1, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 4, 0, 4, 5, 0, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 4, 4, 4, 4,_
    4, 4, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 5, 6, 6, 6, 6, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 5, 2, 2, 2, 2, 5, 4, 7, 7, 7, 7, 7, 7, 4,_
    4, 4, 5, 3, 3, 3, 3, 5, 4, 0, 0, 0, 0, 0, 0, 4,_
    4, 4, 5, 1, 1, 1, 1, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4,_
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4}

  dim as Ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "20 86 140")
  IupSetAttribute(image, "1", "148 210 244")
  IupSetAttribute(image, "2", "204 238 252")
  IupSetAttribute(image, "3", "180 224 252")
  IupSetAttribute(image, "4", "BGCOLOR")
  IupSetAttribute(image, "5", "20 106 172")
  IupSetAttribute(image, "6", "228 246 252")
  IupSetAttribute(image, "7", "196 234 252")

  return image
end function

function CImage.load_image_show_pr() as ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 1, 1, 1, 1, 1, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0,_
    0, 1, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 2, 0, 0,_
    0, 1, 1, 0, 0, 1, 1, 0, 2, 2, 0, 0, 2, 2, 0, 0,_
    0, 1, 1, 0, 0, 1, 1, 0, 2, 2, 0, 0, 2, 2, 0, 0,_
    0, 1, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 2, 0, 0,_
    0, 1, 1, 1, 1, 1, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 2, 2, 2, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 2, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "0 128 192")
  IupSetAttribute(image, "2", "0 136 0")
  IupSetAttribute(image, "3", "0 0 0")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_show_p() as ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
 
  dim as ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "0 128 192")
  IupSetAttribute(image, "2", "0 0 0")
  IupSetAttribute(image, "3", "0 0 0")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_show_r() as ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "0 136 0")
  IupSetAttribute(image, "2", "0 0 0")
  IupSetAttribute(image, "3", "0 0 0")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function

function CImage.load_image_show_nopr() as ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0,_
    0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0,_
    0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0,_
    0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0,_
    0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0,_
    0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0,_
    0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

  dim as ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "184 184 184")
  IupSetAttribute(image, "2", "0 0 0")
  IupSetAttribute(image, "3", "0 0 0")
  IupSetAttribute(image, "4", "0 0 0")
  IupSetAttribute(image, "5", "0 0 0")
  IupSetAttribute(image, "6", "0 0 0")
  IupSetAttribute(image, "7", "0 0 0")

  return image
end function


function CImage.load_image_collapse() as ihandle ptr

  dim as const ubyte imgdata(255) = {_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,_
    0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0, 0, 0,_
    0, 0, 6, 2, 2, 2, 2, 2, 2, 2, 2, 2, 6, 0, 0, 0,_
    0, 0, 6, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 6, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 6, 2, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0,_
    0, 0, 6, 2, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0,_
    0, 0, 6, 2, 1, 2, 7, 7, 7, 7, 7, 7, 7, 7, 1, 0,_
    0, 0, 6, 2, 1, 2, 5, 5, 5, 5, 5, 5, 5, 7, 1, 0,_
    0, 0, 6, 2, 1, 2, 7, 2, 2, 2, 2, 2, 2, 2, 1, 0,_
    0, 0, 6, 2, 1, 2, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0,_
    0, 0, 6, 6, 1, 2, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0,_
    0, 0, 0, 0, 1, 2, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0,_
    0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,_
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

   dim as ihandle ptr image = IupImage(16, 16, @imgdata(0))

  IupSetAttribute(image, "0", "BGCOLOR")
  IupSetAttribute(image, "1", "140 150 172")
  IupSetAttribute(image, "2", "212 242 252")
  IupSetAttribute(image, "3", "236 250 252")
  IupSetAttribute(image, "4", "252 254 252")
  IupSetAttribute(image, "5", "4 70 124")
  IupSetAttribute(image, "6", "164 178 196")
  IupSetAttribute(image, "7", "244 250 252")

  return image
end function