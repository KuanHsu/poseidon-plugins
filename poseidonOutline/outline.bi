#include once "IUP/iup.bi"
#include once "parser/ast.bi"
#include once "parser/tools.bi"

type COutline
	private :
		as Ihandle ptr		treeHandle

		declare sub createLayout()
		declare sub append( rootTree as Ihandle ptr, _node as CASTnode ptr, bracchID as integer, bInsertMode as boolean = false )
		declare sub setImage( rootTree as Ihandle ptr, _node as CASTnode ptr )

	public :
		as integer			prIndex, dialogX, dialogY
		as string			rasterSize, foreColor, backColor
		as integer			listIDs(1 to 30000)
		
		declare constructor()
		declare destructor()
		declare function getTreeHandle() as Ihandle ptr
		declare sub createTree( head as CASTnode ptr )
		declare sub cleanTree()
		declare sub setPublicParams( _pos as string, _size as string, _fore as string, _back as string )
		declare function getImageName( _node as CASTnode ptr ) as string
		
end type

declare function COutline_BUTTON_CB cdecl( ih as Ihandle ptr, button as integer, pressed as integer, x as integer, y as integer, status as zstring ptr ) as integer