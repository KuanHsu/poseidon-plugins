#include once "outline.bi"

constructor COutline()

	treeHandle = IupTree()
	IupSetAttributes( treeHandle, "ADDROOT=YES,EXPAND=YES,SIZE=NULL,TITLE=EMPTY" )
	IupSetCallback( treeHandle, "BUTTON_CB", cast( Icallback, @COutline_BUTTON_CB ) )
	
end constructor

destructor COutline()

	'if( treeHandle <> null ) then IupDestroy( treeHandle )
	treeHandle = null

end destructor

sub COutline.append( rootTree as Ihandle ptr, _node as CASTnode ptr, bracchID as integer, bInsertMode as boolean = false )

	dim as integer		lastAddNode

	if( _node = null ) then return

	dim as string		BRANCH, LEAF
		
	if( bInsertMode = true ) then 
		BRANCH	= "INSERTBRANCH"
		LEAF	= "INSERTLEAF"
	else
		BRANCH	= "ADDBRANCH"
		LEAF	= "ADDLEAF"
	end if
		
	if( _node->getChildrenCount > 0 ) then

		dim as string		_type = _node->type_
		dim as string		_paramString
	
		select case _node->kind_
			case B_FUNCTION, B_PROPERTY, B_OPERATOR
				dim as integer		_pos = instr( _node->type_, "(" )
				
				if( _pos > 0 ) then
					_type = left( _node->type_, _pos - 1 )
					_paramString = right( _node->type_, len( _node->type_ ) - _pos + 1 )
				end if
				
				if( prIndex = 1 ) then
					_type = ""
				elseif( prIndex = 2 ) then
					_paramString =""
				elseif( prIndex = 3 ) then
					_type = ""
					_paramString =""
				end if

				if( len(_type) > 0 ) then 
					IupSetAttributeId( rootTree, BRANCH, bracchID, _node->name_ + _paramString + " : " + _type + "  ...[" + str( _node->lineNumber_ ) + "]" )
				else
					IupSetAttributeId( rootTree, BRANCH, bracchID, _node->name_ + _paramString + "  ...[" + str( _node->lineNumber_ ) + "]" )
				end if

			case B_SUB, B_CTOR, B_DTOR, B_MACRO
				if( prIndex > 1 ) then _type = ""
			
				IupSetAttributeId( rootTree, BRANCH, bracchID, _node->name_ + _type + "  ...[" + str( _node->lineNumber_ ) + "]" )

			case B_SCOPE:
				IupSetAttributeId( rootTree, BRANCH, bracchID, "" + "  ...[" + str( _node->lineNumber_ ) + "]" )

			case B_TYPE, B_CLASS
				if( len(_node->base_) > 0 ) then
					IupSetAttributeId( rootTree, BRANCH, bracchID, _node->name_ + " : " + _node->base_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
				else
					IupSetAttributeId( rootTree, BRANCH, bracchID, _node->name_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
				end if
				
			case else
				IupSetAttributeId( rootTree, BRANCH, bracchID, _node->name_ + "  ...[" + str( _node->lineNumber_ ) + "]")

		end select
		
		lastAddNode = IupGetInt( rootTree, "LASTADDNODE" )
		setImage( rootTree, _node )
		IupSetAttributeId( rootTree, "USERDATA", lastAddNode, cast( zstring ptr, _node ) )
		
		for i as integer = _node->getChildrenCount - 1 to 0 step -1
			append( rootTree, _node->getChild( i ), lastAddNode )
		next

	else

		dim as boolean		bNoImage = false
		dim as string		_type = _node->type_
		dim as string		_paramString
		
		select case ( _node->kind_ AND 4194303 )
			
			case B_FUNCTION, B_PROPERTY, B_OPERATOR
				dim as integer		_pos = instr( _node->type_, "(" )
					
				if( _pos > 0 ) then
					_type = left( _node->type_, _pos - 1 )
					_paramString = right( _node->type_, len( _node->type_ ) - _pos + 1 )
				end if
				
				if( prIndex = 1 ) then
					_type = ""
				elseif( prIndex = 2 ) then
					_paramString =""
				elseif( prIndex = 3 ) then
					_type = ""
					_paramString =""
				end if				

				if( _node->kind_ AND B_DEFINE ) then
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + _paramString + "  ...[" + str( _node->lineNumber_ ) + "]" )
					exit select
				end if
	
				if( len(_type) > 0 ) then 
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + _paramString + " : " + _type + "  ...[" + str( _node->lineNumber_ ) + "]" )
				else
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + _paramString + "  ...[" + str( _node->lineNumber_ ) + "]" )
				end if

			case B_SUB, B_CTOR, B_DTOR
				if( prIndex > 1 ) then _type = ""
				
				IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + _type + "  ...[" + str( _node->lineNumber_ ) + "]" )

			case B_VARIABLE, B_ALIAS
				if( prIndex = 1 ) then
					_type = ""
				elseif( prIndex = 2 ) then
					_paramString =""
				elseif( prIndex = 3 ) then
					_type = ""
					_paramString =""
				end if
				
				if( _node->kind_ AND B_DEFINE ) then 
					if( len( _type ) > 0 ) then 
						IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + " : " + _type + "  ...[" + str( _node->lineNumber_ ) + "]" )
					else
						IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
					end if
					exit select
				end if
				
				if( len( _type ) > 0 ) then 
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + " : " + _type + "  ...[" + str( _node->lineNumber_ ) + "]" )
				else
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
				end if

			case B_ENUMMEMBER, B_WITH
				IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
					
			case B_TYPE, B_CLASS
				if( len( _node->base_ ) > 0 ) then 
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + " : " + _node->base_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
				else
					IupSetAttributeId( rootTree, LEAF, bracchID, _node->name_ + "  ...[" + str( _node->lineNumber_ ) + "]" )
				end if

			case else
				bNoImage = true
		
		end select

		if( bNoImage = false ) then
			lastAddNode = IupGetInt( rootTree, "LASTADDNODE" )
			setImage( rootTree, _node )
			IupSetAttributeId( rootTree, "USERDATA", lastAddNode, cast( zstring ptr, _node ) )
		end if
	end if

end sub

sub COutline.setImage( rootTree as Ihandle ptr, _node as CASTnode ptr )

	dim as integer		lastAddNode = IupGetInt( rootTree, "LASTADDNODE" )
	dim as string		prot
	
	if( len( _node->protection_ ) > 0 ) then
		if( _node->protection_ <> "public" ) AND ( _node->protection_ <> "shared" ) then prot = "_" + _node->protection_
	end if

	select case _node->kind_

		case ( B_DEFINE OR B_VARIABLE )
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_define_var" )

		case ( B_DEFINE OR B_FUNCTION )
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_define_fun" )
		
		case B_VARIABLE
			if( len( _node->name_ ) > 0 ) then

				if( right( _node->name_, 1 ) = ")" ) then
					IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_variable_array" + prot )
				else
					IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_variable" )
				end if

			end if
			
		case B_FUNCTION
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_function" + prot )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_function" + prot )

		case B_SUB
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_sub" + prot )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_sub" + prot )

		case B_OPERATOR
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_operator" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_operator" )

		case B_PROPERTY:
			if( len( _node->type_ ) > 0 ) then

				if( left( _node->type_, 1 ) = "(" ) then
					IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_property" )
					if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_property" )
				else
					IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_property_var" )
					if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_property_var" )
				end if
			end if

		case B_CTOR
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_ctor" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_ctor" )

		case B_DTOR
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_dtor" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_dtor" )

		case B_TYPE
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_struct" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_struct" )

		case B_CLASS
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_class" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_class" )

		case B_ENUM
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_enum" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_enum" )

		case B_ENUMMEMBER
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_enummember" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_enummember" )

		case B_UNION
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_union" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_union" )

		case B_ALIAS
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_alias" )

		case B_NAMESPACE
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_namespace" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_namespace" )

		case B_MACRO
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_macro" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_macro" )

		case B_SCOPE
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_scope" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_scope" )
			
		case B_WITH
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_with" )
			if( _node->getChildrenCount > 0 ) then IupSetAttributeId( rootTree, "IMAGEEXPANDED", lastAddNode, "pFB_Outline_with" )

		case else
			IupSetAttributeId( rootTree, "IMAGE", lastAddNode, "pFB_Outline_variable" )
	
	end select

end sub

function COutline.getTreeHandle() as Ihandle ptr

	return treeHandle

end function


sub COutline.setPublicParams( _pos as string, _size as string, _fore as string, _back as string )
	
	rasterSize = _size
	foreColor = _fore
	backColor = _back
	
	dim as integer index = instr( _pos, "," )
	if( index > 0 ) then
		dialogX = valint( left( _pos, index - 1 ) )
		dialogY = valint( right( _pos, len(_pos) - index ) )
	end if
	
end sub

sub COutline.cleanTree()

	IupSetAttributeId( treeHandle, "DELNODE", 0, "CHILDREN" )
	IupSetAttribute( treeHandle, "TITLE", " " )

	#ifdef __fb_win32__
		IupSetAttribute( treeHandle, "FONT", "Consolas, 9" )
	#else
		IupSetAttribute( treeHandle, "FONT", "Monospace, 9" )
	#endif
	IupMap( treeHandle )
	IupRefresh( treeHandle )

end sub


sub COutline.createTree( head as CASTnode ptr )

	if( head <> null ) then

		if( head->kind_ = B_BAS OR head->kind_ = B_BI ) then

			dim as string		fullPath = head->name_
			
			IupSetAttributeId( treeHandle, "DELNODE", 0, "CHILDREN" )
			IupSetAttribute( treeHandle, "TITLE", fullPath )
			IupSetAttribute( treeHandle, "FGCOLOR", foreColor )
			IupSetAttribute( treeHandle, "BGCOLOR", backColor )

			#ifdef __fb_win32__
				IupSetAttribute( treeHandle, "FONT", "Consolas, 9" )
			#else
				IupSetAttribute( treeHandle, "FONT", "Monospace, 9" )
			#endif
			
			IupMap( treeHandle )
			IupRefresh( treeHandle )
			
			for i as integer = head->getChildrenCount - 1 to 0 step -1
				append( treeHandle, head->getChild( i ), 0 )
			next
			
			IupMap( treeHandle )
		
		end if
	
	end if

end sub


function COutline.getImageName( _node as CASTnode ptr ) as string

	dim as string prot
	if( len( _node->protection_ ) > 0 ) then
		if( _node->protection_ <> "public" AND _node->protection_ <> "shared" ) then prot = "_" + _node->protection_
	end if

	select case _node->kind_
	
		case ( B_DEFINE OR B_VARIABLE )
			return "pFB_Outline_define_var"
		case ( B_DEFINE OR  B_FUNCTION )
			return "pFB_Outline_define_fun"
		case B_VARIABLE
			if( len( _node->name_ ) > 0 ) then
				if( right( _node->name_, 1 ) = ")" ) then
					return ( "pFB_Outline_variable_array" + prot )
				else
					return( "pFB_Outline_variable" + prot )
				end if
			endif
			
		case B_FUNCTION
			return ( "pFB_Outline_function" + prot )
		case B_SUB
			return ( "pFB_Outline_sub" + prot )
		case B_OPERATOR
			return "pFB_Outline_operator"
		case B_PROPERTY	
			if( len( _node->type_ ) > 0 ) then
				if( left( _node->type_, 1 ) = "(" ) then
					return "pFB_Outline_property"
				else
					return "pFB_Outline_property_var"
				end if
			end if

		case B_CTOR
			return "pFB_Outline_ctor"
		case B_DTOR
			return "pFB_Outline_dtor"
		case B_TYPE
			return "pFB_Outline_struct"
		case B_CLASS
			return "pFB_Outline_class"
		case B_ENUM
			return "pFB_Outline_enum"
		case B_ENUMMEMBER
			return "pFB_Outline_enummember"
		case B_UNION
			return "pFB_Outline_union"
		case B_ALIAS
			return "pFB_Outline_alias"
		case B_NAMESPACE
			return "pFB_Outline_namespace"
		case B_MACRO
			return "pFB_Outline_macro"
		case B_SCOPE
			return "pFB_Outline_scope"
		case else
			return "pFB_Outline_variable"
	end select

	return "pFB_Outline_variable"
end function

'
'
'
'Callback Function

private function COutline_BUTTON_CB cdecl( ih as Ihandle ptr, button as integer, pressed as integer, x as integer, y as integer, status as zstring ptr ) as integer

	dim as integer id = IupConvertXYToPos( ih, x, y )
	
	dim as Ihandle ptr iupSci	= getActiveDoucumentPtr()
	dim as string documentTitle	= getActiveDoucumentTiTle()
	
	if( iupSci <> null AND documentTitle <> "" ) then
	
		if( button = IUP_BUTTON1 ) then 'Left Click
			if( iup_isdouble( status ) ) then 'Double Click
				dim as CASTnode ptr _node = cast( CASTnode ptr, IupGetAttributeId( ih, "USERDATA", id ) )
				if( _node <> null ) then
				
					dim as string rootTitle =IupGetAttributeId( ih, "TITLE", 0 )[0]
					if( rootTitle = documentTitle ) then
						dim as integer lineNumber = _node->lineNumber_  - 1
						IupSetAttributeId( iupSci, "ENSUREVISIBLE", lineNumber, "ENFORCEPOLICY" )
						IupSetInt( iupSci, "CARET", lineNumber )
						IupSetFocus( iupSci )
						
					end if
					return IUP_IGNORE
				end if
			end if
		end if
	end if
	
	return IUP_DEFAULT
	
end function
