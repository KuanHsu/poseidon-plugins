#include once "ast.bi"
#include once "token.bi"
#include once "tools.bi"


type CParser
	private :
		as CASTnode ptr			activeASTnode

		as TokenUnit ptr		pTokens
		as integer				tokenSize
		
		as integer				tokenIndex, nearestTailInsertPos
		as TokenUnit			nullTokenUnit
		as string				jsonTXT
		
		declare function token() as TokenUnit
		declare function prev1() as TokenUnit
		declare function next1() as TokenUnit
		declare function next2() as TokenUnit
		declare sub parseToken( t as TOK = TOK.Tidentifier )
		
		declare function parseIdentifier() as string
		declare function parsePreprocessor() as boolean		
		declare function getVariableType() as string
		declare function parseParam( bDeclare as boolean ) as string
		declare function parseArray() as string
		declare function parseVariable() as boolean
		declare function parserVar() as boolean
		declare function parseNamespace() as boolean
		declare function parseUsing() as boolean
		declare function parseOperator( bDeclare as boolean, _protection as string ) as boolean
		declare function parseProcedure( bDeclare as boolean, _protection as string ) as boolean
		declare function parseTypeBody( B_KIND as integer ) as boolean
		declare function parseFunctionPointer( _name as string, _lineNumber  as integer ) as boolean
		declare function parseType( bClass as boolean = false ) as boolean
		declare function parseEnumBody() as boolean
		declare function parseEnum() as boolean
		declare function parseScope() as boolean
		declare function parseWith() as boolean
		declare function parseEnd() as boolean
		
		declare sub ASTtoJSON( _node as CASTnode ptr )
		
		
	public :
		declare Constructor()
		declare Constructor( _p as TokenUnit ptr, _tokenSize as integer )
		declare Destructor()
		
		declare sub init()
		declare sub clear()
		declare sub setTokenData( _p as TokenUnit ptr, _tokenSize as integer )
		declare function parse( fullPath as string, B_KIND as integer = 0 ) as CASTnode ptr
		declare function toJSON() as string

end type