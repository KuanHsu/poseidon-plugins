#include once "ast.bi"
#include once "token.bi"
#include once "IUP/iup.bi"
#include once "fbc-int/array.bi"

' This function by counting_pine
declare function str_replace( byref haystack as string, byref needle1 as string, byref needle2 as string ) as string

' This function by fxm
' https://www.freebasic.net/forum/viewtopic.php?p=284817#p284817
declare Function split(Byref s As Const String, result() As String, Byref delimiter As Const String = ",") As Integer
declare Function join(s() As Const String, Byref delimiter As Const String = ",") As String

declare function getActiveDoucumentPtr() as Ihandle ptr
declare function getActiveDoucumentTiTle() as string

Type T_TabsOptions
	
	As IHandle Ptr multitext
	
End Type

Type T_GlobalVariables
	
	As Ihandle Ptr t_list ''As Tlist Ptr t_list
	
	As IHandle Ptr dlg
	
	As IHandle Ptr find_dlg
	
	As Ihandle Ptr tool_dlg
	
	As IHandle Ptr tabs
	
	As IHandle Ptr toolbar
	
	As IHandle Ptr statusbar
	
	As IHandle Ptr buildbox
	
	As IHandle Ptr split
	
	As IHandle Ptr vertsplit
	
	As Ihandle Ptr hTimer 
	
	As Ihandle Ptr hTimerUpdateStatusBar
	
	As Ihandle Ptr hTimerUpdateBiFiles
	
	As ihandle Ptr hConfig
	
	As ihandle Ptr hConfigL
	
	As ihandle Ptr hConfigplug
	
	As ihandle Ptr hOptions
	
	As ihandle Ptr hItemTools
	
	As ihandle Ptr hItemFB
	
	As ihandle Ptr hItemAbout
	
	As Double dTimeUpdateListSideWin
	
	As Byte bFlagFindInfo
	
	As Byte bUpdateListSideWin
	
	As Byte bUpdateAutoCompletion
	
	As Byte bUpdateTips
	
	As Byte bFlagBiFilesSignalUpdate
	
	As Byte bFlagInitEditor_ForCallbacks
	
	As Zstring*4 szLangid
	
	As Zstring*40 szLangName
	
	As Zstring*8 szColorbtn(1 To 40)
	
	As Zstring*4 szAlphaCaret
	
	As Zstring*100 szFontEditor
	
	As Zstring*4 szFontEditorSize
		
	As Zstring*100 szFontStatus
	
	As Zstring*4 szFontStatusSize
	
	As Zstring*100 szFontWinOut
	
	As Zstring*4 szFontWinOutSize
	
	As Zstring*100 szFontWinSide
	
	As Zstring*4 szFontWinSideSize
	
	As Zstring*100 szFontAnnot
	
	As Zstring*4 szFontAnnotSize		
		
	As Byte blangCurrent
	
	As Byte bLineAutocomplect
	
	As Byte bFlagAutoComplect
	
	As Byte bFlagFolding
	
	As Zstring*21 szDefaultSystemEncoding
	
	As Zstring Ptr pszKeywords0
	
	As Zstring Ptr pszKeywords1
	
	As Zstring Ptr pszKeywords2
	
	As Zstring Ptr pszKeywords3	
	
	As T_TabsOptions tTabsOptions(100)
	
End Type