#define null 0

#define B_VARIABLE 1
#define B_FUNCTION 2
#define B_SUB 4
#define B_PROPERTY 8
#define B_CTOR 16
#define B_DTOR 32
#define B_PARAM 64
#define B_TYPE 128
#define B_ENUM 256
#define B_UNION 512
#define B_CLASS 1024
#define B_INCLUDE 2048
#define B_ENUMMEMBER 4096
#define B_ALIAS 8192
#define B_BAS 16384
#define B_BI 32768
#define B_NAMESPACE 65536
#define B_MACRO 131072
#define B_SCOPE 262144
#define B_DEFINE 524288
#define B_OPERATOR 1048576
#define B_WITH 2097152
#define B_USING 4194304

type CASTnode
	private :
		as integer			childrenSize
		as CASTnode ptr		father
		as CASTnode ptr		children(any)
		
	public :
		as string			name_
		as integer			kind_
		as string			protection_, type_, base_
		as integer			lineNumber_
		as integer			endLineNum_
		
		declare Constructor( __name as string, __kind as integer, __protection as string, __type as string, __base as string, __lineNumber as integer, __endLineNum as integer = -1 )
		declare Operator[] ( index as integer ) as CASTnode ptr
		declare Destructor()
		declare	function addChild( _child as CASTnode ptr ) as integer
		declare	function addChild( __name as string, __kind as uinteger, __protection as string, __type as string, __base as string, __lineNumber as integer, __endLineNum as integer = -1 ) as CASTnode ptr
		'declare	function insertChildByLineNumber( _child as CASTnode, _ln as integer ) as integer
		declare function getChild( index as integer ) as CASTnode ptr
		declare function child( index as integer ) as CASTnode ptr
		declare function getFather( _endLineNum as integer = -1 ) as CASTnode ptr
		
		declare function getChildren() as CASTnode ptr
		declare function getChildrenCount() as integer
		'declare sub zeroChildCount()

end type