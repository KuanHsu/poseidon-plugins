#include once "tools.bi"

common shared as T_GlobalVariables Ptr TGlobalVariablesDll


' This function by counting_pine
function str_replace(byref haystack as string, byref needle1 as string, byref needle2 as string) as string
   
    dim as integer len1 = len(needle1), len2 = len(needle2)
    dim as integer i
   
    dim as string haystack_ = haystack
   
    i = instr(haystack_, needle1)
    while i
       
        haystack_ = left(haystack_, i - 1) _
                  & needle2 _
                  & mid(haystack_, i + len1)
       
        i = instr(i + len2, haystack_, needle1)
       
    wend
   
    function = haystack_
   
end function


' This function by fxm
' https://www.freebasic.net/forum/viewtopic.php?p=284817#p284817
Function split(Byref s As Const String, result() As String, Byref delimiter As Const String = ",") As Integer
   
    Const As Integer resizing1 = 36
    Const As Integer resizing2 = 10 * resizing1
   
    Dim As Integer i
    Redim As Integer index(resizing1)
    index(0) = 1
   
    If delimiter = "" Then Return 1  '' supplied delimiter empty
   
    Do
        Dim As Integer n = Instr(index(i), s, delimiter)
        If n = 0 Then Exit Do
        If i = Ubound(index) Then
            Redim Preserve index(i + resizing1 * (i \ resizing2 + 1))
        End If
        i += 1
        index(i) = n + Len(delimiter)
    Loop
   
    If i = 0 Then Return 2  '' no delimiter found in string
   
    If Ubound(result) - Lbound(result) < i Then
        Dim As Const FBC.FBARRAY Ptr pd = FBC.ArrayConstDescriptorPtr(result())
        If (pd->flags And FBC.FBARRAY_FLAGS_FIXED_LEN) Then Return 3  '' supplied fix-len result array() too small
        Redim Preserve result(Lbound(result) To Lbound(result) + i)
    End If
   
    For j As Integer = 0 To i - 1
        result(Lbound(result) + j) = Mid(s, index(j), index(j + 1) - index(j) - Len(delimiter))
    Next j
    result(Lbound(result) + i) = Mid(s, index(i))
    Return 0  '' OK
   
End Function



Function join(s() As Const String, Byref delimiter As Const String = ",") As String
   
    Dim As String result
   
    If Ubound(s) >= Lbound(s) Then
        For i As Integer = Lbound(s) To Ubound(s) - 1
            result &= s(i) & delimiter
        Next i
        result &= s(Ubound(s))
    End If
    Return result
   
End Function


function getActiveDoucumentPtr() as Ihandle ptr
	
	if( TGlobalVariablesDll->tabs <> null ) then

		dim as integer VALUEPOS = IupGetInt( TGlobalVariablesDll->tabs, "VALUEPOS" )
		if( VALUEPOS > 0 ) then	return TGlobalVariablesDll->tTabsOptions(VALUEPOS).multitext 'Get Active Doc Tab(iupscintilla handle)
	end if
	
	return null
	
end function


function getActiveDoucumentTiTle() as string

	if( TGlobalVariablesDll->tabs <> null ) then

		dim as integer _valuepos = IupGetInt( TGlobalVariablesDll->tabs, "VALUEPOS" )
		if( _valuepos > -1 ) then return IupGetAttributeId( TGlobalVariablesDll->tabs, "TABTITLE", _valuepos )[0]
	end if
	
	return ""
	
end function