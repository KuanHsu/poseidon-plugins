enum TOK
	Tassign				' =
	Tplus				' +
	Tminus				' -	
	Tdiv				' /
	Ttimes				' *	
	Tmod				' mod
	Tintegerdiv			' \
	Tshl				' shl
	Tshr				' dhr
	Tcaret				' ^

	Tgreater			' >
	Tless				' <
	Tis					' is
 
	Tdotdotdot			' ...
	Tdot				' .
	Tunderline			' _
	Texclam				' !
	Tquest				' ?
	Tdollar				' $
	Tquote				' "
	Tcolon				' :
	Tcomma				' ,
	Tpound				' #
	Tandsign			' &
	Tat					' @
	Topenparen			' (
	Tcloseparen			' )
	Topenbracket		' [
	Tclosebracket		' ]
	Topencurly			' {
	Tclosecurly			' }


	Tand
	Tor
	Txor
	Tnot
	Teqv
	Timp

	Tandalso
	Torelse

	Tlet
	Tfor
	Tto
	Tnext
	Tstep
	Texit
	Tnew
	Tdelete
	Tdo
	Tloop
	Twhile
	Twend
	Tif
	Tthen
	Telse
	Tend
	Tdim
	Tas
	Tenum
	Ttype
	Tunion
	Tfield
	Treturn
	Twith
	Tselect
	Tcase
	Tcast	

	Toption
	Texplicit
	

	
	' function
	Tdeclare
	Tsub
	Tfunction
	Tbyval
	Tbyref
	Tstdcall
	Tcdecl
	Tpascal
	Toverload
	Talias
	Texport
	Tnaked

	' data
	Tbyte
	Tubyte
	Tshort
	Tushort
	Tinteger
	Tuinteger
	Tlongint
	Tulongint
	Tsingle
	Tdouble
	Tstring
	Tzstring
	Twstring
	Tany
	
	Tconst
	Tpointer
	Tptr
	Tptraccess
	Tunsigned
	Textern
	Tcommon
	Tshared
	Tstatic
	Tscope

	' Array
	Tredim
	Tvar
	Tpreserve
	Terase
	
	' class
	Tclass
	Tprivate
	Tprotected
	Tpublic
	Textends
	Tobject
	Tvirtual
	Tabstract
	Tconstructor
	Tdestructor
	Tproperty
	Toperator
	Tthis
	Tbase
	Toverride

	'
	Tinclude
	Tlibpath
	Tonce
	Telseif
	Tendif
	Tifdef
	Tifndef
	Tdefine
	Tmacro
	Tendmacro
	Tlib
	
	'Tinclib
	Tnamespace
	Tusing
	'
	Teol ' \n
	Tidentifier
	Tstrings
	Tnumbers
end enum

type TokenUnit
	as TOK			tok
	as string		identifier
	as integer		lineNumber
end type

declare function identToTOK( ident as string ) as TOK