''------------------------------------------------------------------------------
''
'' File    : fbstyle.bas - FreeBASIC Beautifier Program
''
'' FBSTYLE.EXE is a conversion of BCB.EXE to FreeBASIC so that it can be used on
'' the linux platform. Origional program written by Garvan O'Keeffe, Mike Henning
'' and Vic McLung
''
'' BCB.EXE - BC Beautifier, this is a merger of Garvan's ChangeCase.bas and
'' Mike's FreeBASICIndent, with a few changes to handle more toggle directives and updates.
''
'' Purpose : Changes the case of keywords/Indents a FreeBASIC source file.
''
'' Syntax  : BCB filename[.bas] [CCASE] [TABS]
'' CCASE   : 0 = ALL CAPS, 1 = Mixed, 2 = lower (default = 0)
'' TABS    : Number of spaced of indent (default = 1 tab)
''
'' History : Date       Reason
''           29-01-05   Created by Garvan O'Keeffe (GOK)
''           30-01-05   Modified by Vic McClung to handle additional toggle $directives (Vic)
''           31-01-05   Merged with Mike Henning's FreeBASICIndent.bas to create bcb.bas (Vic)
''           05-02-05   Incorporated Mike's changes and removed $PP as toggle (Vic)
''           09-02-05   Fixed TRIM(LCASE) bug. (GOK)
''           10-02-05   Fixed CONST bug (GOK)
''           20-05-05   Modified copy for FreeBASIC source code (GOK)
''           24-06-05   Ported to FreeBASIC for use on linux (GOK)
''           14-07-05   removed all toggle $directives (GOK)
''                      added asm block handling (GOK)
''                      removed special case CONST handling (GOK)
''           20-08-05   New FreeBASIC module level code handler (GOK)
''           01-12-05   Added casts to make it 0.15b compatible
''
''------------------------------------------------------------------------------
'' NOTES ON INTENDED BEHAVIOUR/COMPROMISES
''
'' REM is always justified full left if it is the first keyword on a line
'' Single quote is indented inline with code unless it is the first character of a line 
''     in which case it is justified left (i.e. left unchanged)
'' Multiline MACROS are left left unformated
''------------------------------------------------------------------------------
' Modified by Kuan Hsu using at poseidonFB IDE plugin, 2021.08.14


#define MAX(A,B) iif(A>B,A,B)

#include once "crt.bi"

declare function CC(byref s as string) as string
declare function IsKeyword(byval anyKeywords as zstring ptr, byval cKeywords as integer, _
		byref pszText as string, byval cchText as integer) as integer
declare function AdjustIndent() as integer
declare function EndinLine() as integer
declare sub FastParse(byref Arg as string)
declare function MCASE(byref s as string) as string

							
declare function STRNICMP (byref str1 as string, byref str2 as string, byval count as integer) as integer

const tok_end = 0  '' numbering follows order of tokens in BumpDowns
dim shared BumpDowns(7) as zstring * 15 => { "end","endif","wend","loop","next","#endif", "glend", "glpopmatrix" }

dim shared BumpOuts(4) as zstring * 15 => { "else","elseif","#elseif","#else","case" }

'' The enum must have same order of BumpUps and must be zero based
enum tok_BumpUps
	tok_if = 0 , tok_union ,tok_type, tok_function, tok_sub, tok_private, tok_public, tok_do, tok_while, tok_for
	tok_begin, tok_select, tok_with, tok_enum, tok__if, tok_glbegin, tok_glpushmatrix
	tok__ifdef, tok___ifndef, tok_asm, tok_scope
end enum

dim shared BumpUps(20) as zstring * 15 => { _
		"if","union","type","function","sub","private","public","do","while","for", _
		"begin","select","with","enum","#if" , "glbegin", "glpushmatrix", _
		"#ifdef", "#ifndef", "asm", "scope" }

dim shared as string Src
dim shared Stk(256) as zstring * 1024

dim shared as integer CCASE = 2
dim shared as integer indents = 1
dim shared as integer curlevel
dim shared as integer Ndx
dim shared as integer FP1, FP2




''-------------------------------------------------------------------------------
''
'' FUNCTION    : CC(s)
''
'' Purpose     : Converts case of argument as specified in GLOBAL INT CCASE
''
'' History     : Date       Reason
''               29-01-05   Created
''
''-------------------------------------------------------------------------------
function CC(byref s as string) as string
	dim ls as string
	select case CCASE
	case 0 : ls = ucase(s)
	case 1 : ls = MCASE(s)
	case 2 : ls = lcase(s)
	case else : ls = ucase(s)
	end select
	return ls
end function


''-------------------------------------------------------------------------------
''
'' FUNCTION    : IsKeyword(s)
''
'' Purpose     : Binary search of array for keyword match
''
'' History     : Date       Reason
''               29-01-05   Modified from PellesC Sample code
''
''-------------------------------------------------------------------------------
function IsKeyword(byval anyKeywords as zstring ptr, byval cKeywords as integer, _
		byref pszText as string, byval cchText as integer) as integer

	dim as integer lo = 0
	dim as integer hi = cKeywords - 1

	'' binary search
	while hi >= lo
		dim tthis as integer
		tthis = (lo + hi) / 2
		dim cond as integer          '' case insensitive

		cond = strnicmp(anyKeywords[tthis*15], pszText, cchText)
		if cond > 0 then
			hi = tthis - 1
		elseif cond < 0 then
			lo = tthis + 1
		elseif anyKeywords[tthis*15+cchText] = 0 then
			return TRUE
		elseif IsKeyword(@anyKeywords[lo*15], tthis - lo, pszText, cchText) then
			return TRUE
		elseif IsKeyword(@anyKeywords[(tthis + 1)*15], hi - tthis, pszText, cchText) then
			return TRUE
		else
			exit while
		end if
	wend

	return FALSE
end function


''-------------------------------------------------------------------------------
''
'' FUNCTION    : AdjustIndent()
''
'' Purpose     : Calculates the correct indent level
''
'' History     : Date       Reason
''               31-01-05   Created
''
''-------------------------------------------------------------------------------
function AdjustIndent() as integer
	dim as integer i
	dim as string t1
	static IntypeFlag as integer

	'' If we get a single line comment the parser will just return
	'' immediatly with a zero Ndx, so will do nothing here.
	if Ndx = 0 then return curlevel

	for i = 0 to ubound(BumpUps)
		if BumpUps(i) = Stk(1) then
			if i=tok_function or i=tok_sub or i=tok_private or i=tok_public then
				if IntypeFlag or Stk(2) = "=" then
					return curlevel
				else
					curlevel = 0
				end if
			end if
			if EndinLine() then return curlevel
			if curlevel = indents and IntypeFlag =0 and (i = tok_union or i = tok_type or i = tok_enum) then curlevel = 0			
			if i = tok_union or i = tok_type or i = tok_asm then IntypeFlag += 1
			curlevel += indents
			return curlevel - indents
		end if
	next

	for i = 0 to ubound(BumpOuts)
		if BumpOuts(i) = Stk(1) then
			return MAX(0,curlevel - indents)
		end if
	next

	for i = 0 to ubound(BumpDowns)
		if BumpDowns(i) = Stk(1) then
			if i = tok_end then
				if Ndx = 1 then return curlevel
				if instr("0123456789", left(Stk(2), 1)) <> 0 then return curlevel
			end if
			curlevel = MAX(0,curlevel - indents)
			if IntypeFlag then IntypeFlag -=1
			return curlevel
		end if
	next

	if EndinLine() then
		curlevel = MAX(0,curlevel - indents)
		if IntypeFlag then IntypeFlag -=1
		return curlevel + indents
	end if

	t1 = " " + Stk(1) + " "
	if (Stk(1) = "const") or _
			(Stk(1) = "dim" and Stk(2) = "shared") then
		curlevel = MAX(0,curlevel - indents)
	elseif instr(" extern declare option const defint defbyte defdbl deflng defshort defsng defstr defubyte defuint defushort ",t1) then
		'' instr is faster (x2) than looping through a list if we don't want to know what token matched
		curlevel = MAX(0,curlevel - indents)
	elseif instr(" #define #dynamic #error #inclib #include #print #static #undef ", t1) then
		curlevel = MAX(0,curlevel - indents)
	else
		curlevel = MAX(indents,curlevel)
	end if

	return curlevel
end function


''-------------------------------------------------------------------------------
''
'' FUNCTION    : EndinLine()
''
'' Purpose     : Looks for THEN and EXIT
''
'' History     : Date       Reason
''               31-01-05   Created
''
''-------------------------------------------------------------------------------
function EndinLine() as integer
	dim as integer i, ii
	for i = 0 to ubound(BumpDowns)
		for ii = 1 to Ndx
			if ii <> Ndx and Stk(ii) = "then" then
				return TRUE
			end if
			if BumpDowns(i) = Stk(ii) and Stk(ii-1) <> "exit" then
				return TRUE
			end if
		next ii
	next i
	return FALSE
end function


''-------------------------------------------------------------------------------
''
'' FUNCTION    : FastParse(Arg)
''
'' Purpose     : Divides up the file into Tokens
''
'' History     : Date       Reason
''               31-01-05   Created
''
''-------------------------------------------------------------------------------
sub FastParse(Arg as string)
	dim as integer cnt1=0,cnt2=0
	Ndx=1

	while Arg[cnt1] <> 0
		if Arg[cnt1] = 34 then      ''quotes - string literals
			if cnt2 then Stk(Ndx)[cnt2]=0 : Ndx +=1 : cnt2=0
			Stk(Ndx)[0] = 34
			cnt1 +=1
			while Arg[cnt1] <> 34
				cnt2 +=1
				Stk(Ndx)[cnt2] = Arg[cnt1]
				if Arg[cnt1] = 0 then exit sub
				cnt1 +=1
			wend
			cnt2+=1
			Stk(Ndx)[cnt2] = 34
			cnt2+=1
			Stk(Ndx)[cnt2]=0
			Ndx+=1
			cnt2=0
		elseif Arg[cnt1] = 32 or Arg[cnt1] = 9 then  '' spaces, tab
			if cnt2 then Stk(Ndx)[cnt2]=0 : cnt2 = 0 : Ndx+=1
		elseif Arg[cnt1] = 58 or Arg[cnt1] = 61 or Arg[cnt1] = 40 then  '' :  = (
			if cnt2 then Stk(Ndx)[cnt2]=0 : Ndx+=1
			Stk(Ndx)[0] = Arg[cnt1]
			Stk(Ndx)[1]=0 : cnt2 = 0 : Ndx+=1
		else
			if Arg[cnt1] = 39 then exit while
			Stk(Ndx)[cnt2]=Arg[cnt1] : cnt2+=1
		end if
		cnt1+=1
	wend
	Stk(Ndx)[cnt2]=0
	if cnt2 = 0 then Ndx-=1
end sub


''-------------------------------------------------------------------------------
''
'' FUNCTION    : MCASE(Arg)
''
'' Purpose     : Converts string to mixed case
''               does not need to be too fancy because it is only used with single words
''
'' History     : Date       Reason
''               24-06-05   Created
''
''-------------------------------------------------------------------------------
function MCASE(s as string) as string
	static b as string
	b = lcase(s)
	if b[0] > 96 and b[0] < 123 then b[0] = b[0] - 32
	return b
end function


''-------------------------------------------------------------------------------
''
'' FUNCTION    : strnicmp(Arg)
''
'' Purpose     : Replacement for C runtime function not implemented in Linux (?)
''
'' History     : Date       Reason
''               10-07-05   Created
''
''-------------------------------------------------------------------------------
function strnicmp(byref str1 as string, byref str2 as string, byval count as integer) as integer
	dim as integer i
	dim as integer i1, i2

	for i = 0 to count -1
		i1 = str1[i]
		i2 = str2[i]
		if i1 > 64 and i1 < 91 then i1 = i1 + 32
		if i2 > 64 and i2 < 91 then i2 = i2 + 32
		if i1 <> i2 then return i1 - i2
	next
	return 0
end function



' **************************************** poseidonFB Plugin Code *************************************************
#include once "IUP/iup.bi"
#include once "IUP/iup_scintilla.bi"

'compile with -dll option
dim shared as Ihandle ptr	THIS_MAINDIALOG_HANDLE
dim shared as Ihandle ptr	POSEIDON_HANDLE

' Modified code of GOK
private sub go( _tabs as integer )

	dim GetKeyWord(304) as zstring *15 => { _
		"ABS", _
		"ACCESS", _
		"ACOS", _
		"ALIAS", _
		"ALLOCATE", _
		"AND", _
		"ANY", _
		"APPEND", _
		"AS", _
		"ASC", _
		"ASIN", _
		"ASM", _
		"ATAN2", _
		"ATN", _
		"BASE", _
		"BEEP", _
		"BIN", _
		"BINARY", _
		"BIT", _
		"BITRESET", _
		"BITSET", _
		"BLOAD", _
		"BSAVE", _
		"BYREF", _
		"BYTE", _
		"BYVAL", _
		"CALL", _
		"CALLOCATE", _
		"CALLS", _
		"CASE", _
		"CBYTE", _
		"CDBL", _
		"CDECL", _
		"CHAIN", _
		"CHDIR", _
		"CHR", _
		"CINT", _
		"CIRCLE", _
		"CLEAR", _
		"CLNG", _
		"CLNGINT", _
		"CLOSE", _
		"CLS", _
		"COLOR", _
		"COMMAND", _
		"COMMON", _
		"CONST", _
		"CONTAINS", _
		"CONTINUE", _
		"COS", _
		"CSHORT", _
		"CSIGN", _
		"CSNG", _
		"CSRLIN", _
		"CUBYTE", _
		"CUINT", _
		"CULNGINT", _
		"CUNSG", _
		"CURDIR", _
		"CUSHORT", _
		"CVD", _
		"CVI", _
		"CVL", _
		"CVLONGINT", _
		"CVS", _
		"CVSHORT", _
		"DATA", _
		"DATE", _
		"DEALLOCATE", _
		"DECLARE", _
		"DEFBYTE", _
		"DEFDBL", _
		"DEFINE", _
		"DEFINED", _
		"DEFINT", _
		"DEFLNG", _
		"DEFSHORT", _
		"DEFSNG", _
		"DEFSTR", _
		"DEFUBYTE", _
		"DEFUINT", _
		"DEFUSHORT", _
		"DIM", _
		"DIR", _
		"DO", _
		"DOUBLE", _
		"DRAW", _
		"DYNAMIC", _
		"ELSE", _
		"ELSEIF", _
		"END", _
		"ENDIF", _
		"ENUM", _
		"ENVIRON", _
		"EOF", _
		"EQV", _
		"ERASE", _
		"ERR", _
		"ERROR", _
		"EXEC", _
		"EXEPATH", _
		"EXIT", _
		"EXP", _
		"EXPLICIT", _
		"EXTERN", _
		"EXTRACT", _
		"FIX", _
		"FLIP", _
		"FOR", _
		"FRE", _
		"FREEFILE", _
		"FUNCTION", _
		"GET", _
		"GETKEY", _
		"GETMOUSE", _
		"GOSUB", _
		"GOTO", _
		"HEX", _
		"HIBYTE", _
		"HIWORD", _
		"IF", _
		"IFDEF", _
		"IFNDEF", _
		"IIF", _
		"IMP", _
		"INCLIB", _
		"INCLUDE", _
		"INKEY", _
		"INP", _
		"INPUT", _
		"INSTR", _
		"INSTRREV", _
		"INT", _
		"INTEGER", _
		"IREPLACE", _
		"IS", _
		"KILL", _
		"LBOUND", _
		"LCASE", _
		"LEFT", _
		"LEN", _
		"LET", _
		"LIB", _
		"LINE", _
		"LOBYTE", _
		"LOC", _
		"LOCAL", _
		"LOCATE", _
		"LOCK", _
		"LOF", _
		"LOG", _
		"LONG", _
		"LONGINT", _
		"LOOP", _
		"LSET", _
		"LTRIM", _
		"MID", _
		"MKD", _
		"MKDIR", _
		"MKI", _
		"MKL", _
		"MKS", _
		"MOD", _
		"MULTIKEY", _
		"NAME", _
		"NEXT", _
		"NOT", _
		"OCT", _
		"ON", _
		"ONCE", _
		"OPEN", _
		"OPTION", _
		"OR", _
		"OUT", _
		"OUTPUT", _
		"PAINT", _
		"PALETTE", _
		"PASCAL", _
		"PAUSE", _
		"PCOPY", _
		"PEEK", _
		"PEEKI", _
		"PEEKS", _
		"PMAP", _
		"POINT", _
		"POINTER", _
		"POKE", _
		"POKEI", _
		"POKES", _
		"POS", _
		"PRESERVE", _
		"PRESET", _
		"PRINT", _
		"PRIVATE", _
		"PROCPTR", _
		"PSET", _
		"PTR", _
		"PUBLIC", _
		"PUT", _
		"RANDOM", _
		"RANDOMIZE", _
		"READ", _
		"REALLOCATE", _
		"REDIM", _
		"REM", _
		"REMAIN", _
		"REPLACE", _
		"REPLACETOKIDX", _
		"RESET", _
		"RESTORE", _
		"RESUME", _
		"RETURN", _
		"RGB", _
		"RIGHT", _
		"RMDIR", _
		"RND", _
		"RSET", _
		"RTRIM", _
		"RUN", _
		"SADD", _
		"SCREEN", _
		"SCREENCOPY", _
		"SCREENINFO", _
		"SCREENLOCK", _
		"SCREENPTR", _
		"SCREENSET", _
		"SCREENUNLOCK", _
		"SEEK", _
		"SELECT", _
		"SGN", _
		"SHARED", _
		"SHELL", _
		"SHL", _
		"SHORT", _
		"SHR", _
		"SIN", _
		"SINGLE", _
		"SIZEOF", _
		"SLEEP", _
		"SPACE", _
		"SPC", _
		"SQR", _
		"STATIC", _
		"STATIC", _
		"STDCALL", _
		"STEP", _
		"STOP", _
		"STR", _
		"STRCAT", _
		"STRCHR", _
		"STRCMP", _
		"STRCPY", _
		"STRING", _
		"STRING", _
		"STRLEN", _
		"STRNCAT", _
		"STRNCMP", _
		"STRNCPY", _
		"STRPTR", _
		"STRRCHR", _
		"STRSTR", _
		"SUB", _
		"SWAP", _
		"SYSTEM", _
		"TAB", _
		"TALLY", _
		"TAN", _
		"THEN", _
		"TIME", _
		"TIMER", _
		"TO", _
		"TOKENIDX", _
		"TRIM", _
		"TRIMEX", _
		"TYPE", _
		"UBOUND", _
		"UBYTE", _
		"UCASE", _
		"UINTEGER", _
		"ULONGINT", _
		"UNDEF", _
		"UNION", _
		"UNLOCK", _
		"UNSIGNED", _
		"UNTIL", _
		"USHORT", _
		"USING", _
		"VA_ARG", _
		"VA_FIRST", _
		"VA_NEXT", _
		"VAL", _
		"VAL64", _
		"VALINT", _
		"VARPTR", _
		"VIEW", _
		"WAIT", _
		"WEND", _
		"WHILE", _
		"WIDTH", _
		"WINDOW", _
		"WINDOWTITLE", _
		"WITH", _
		"WRITE", _
		"XOR", _
		"ZSTRING" _
		}

	dim as Ihandle ptr _MaintabsHandle = IupGetDialogChild( POSEIDON_HANDLE, "POSEIDON_MAIN_TABS" )
	dim as Ihandle ptr _SubtabsHandle = IupGetDialogChild( POSEIDON_HANDLE, "POSEIDON_SUB_TABS" )
	dim as Ihandle ptr sciPoseidon
	
	
	if( IupGetInt( _MaintabsHandle, "SHOWLINES" ) ) then
		sciPoseidon = cast( Ihandle ptr, IupGetAttribute( _MaintabsHandle, "VALUE_HANDLE" ) ) 'Get Active Doc Tab(iupscintilla handle)
	elseif( IupGetInt( _SubtabsHandle, "SHOWLINES" ) ) then
		sciPoseidon = cast( Ihandle ptr, IupGetAttribute( _SubtabsHandle, "VALUE_HANDLE" ) ) 'Get Active Doc Tab(iupscintilla handle)
	else
		exit sub
	end if
	
	
	if( sciPoseidon <> null ) then
	
		dim as integer caretPos = IupGetInt( sciPoseidon, "CARETPOS" )
		dim as integer firstline = IupGetInt( sciPoseidon, "FIRSTVISIBLELINE" )
	
		dim as string document = IupGetAttribute( sciPoseidon, "VALUE" )[0]
		dim as integer documentLength = len( document )
		
		if( documentLength > 0 ) then
		
		
			dim as string filein, fileout
			dim as string BSrc
			dim as string TempString
			dim as string id
			dim level(100) as integer
			dim as integer levelindex
			dim as byte ptr p, p1

			dim as integer DQ_ON, SQ_ON, LC_ON, ASM_ON, PP_ON, MACRO_ON, NELSE_ON, NELSE_STATE
			dim as integer SP_TAB = _tabs   '' Default to TAB
			dim as integer i						
		
			dim as string OutPutDocument
			dim as string lineData
			for i as integer = 0 to documentLength -1 
				
				if( document[i] <> 10 ) then
					
					lineData = lineData + chr( document[i] )
				else
					Src = lineData
					lineData = ""
					print Src
					'' Strip tabs from left - careful of blank lines where p is null
					p = 0
					p = strptr(Src)
					if p <> 0 then
						while *p
							if *p > 32 then exit while
							if *p = 9 then *p = 32
							p += 1
						wend
					end if

					'' blank
					TempString = trim(Src)

					if TempString = "" then
						'print #FP2, Src
						OutPutDocument = OutPutDocument + Src + chr(10)
						continue for
					end if

					DQ_ON = FALSE
					SQ_ON = FALSE
					BSrc = Src

					if lcase(left(trim(Src),4) ) = "rem " then
						i = instr(lcase(Src),"rem ")
						if i then mid(Src,i) = CC("rem ")
						'print #FP2, ltrim(Src)
						OutPutDocument = OutPutDocument + ltrim(Src) + chr(10)
						continue for
					end if

					if lcase(trim(Src)) = "rem" then
						'print #FP2, CC("rem")
						OutPutDocument = OutPutDocument + CC("rem") + chr(10)
						continue for
					end if

					if lcase(left(Src,1) ) = "'" then
						'print #FP2, Src
						OutPutDocument = OutPutDocument + Src + chr(10)
						continue for
					end if
			  
					FastParse(lcase(TempString))

					if Stk(1) = "#define" and Stk(Ndx) = "_" then
						MACRO_ON = TRUE
					end if

					if MACRO_ON then
						if Stk(Ndx) <> "_" then MACRO_ON = FALSE
						'print #FP2, Src
						OutPutDocument = OutPutDocument + Src + chr(10)
						continue for
					end if


					id = string(AdjustIndent(),SP_TAB)

					if LC_ON then id = id + string(indents, SP_TAB)
					if Stk(Ndx) = "_" then
						LC_ON = TRUE
					else
						LC_ON = FALSE
					end if

					'' ASM blocks
					if Stk(1) = "asm" and Ndx = 1 then ASM_ON = TRUE
					if Stk(1) = "end" and Stk(2) = "asm" then  ASM_ON = FALSE
					'if ASM_ON then print #FP2, id; trim(Src) : continue while
					if( ASM_ON ) then
						OutPutDocument = OutPutDocument + id + trim(Src) + chr(10)
						continue for
					end if

					'' Strip comments and strings from copy of source line
					p = strptr(BSrc)

					while *p
						if *p = 34 then DQ_ON = not DQ_ON : p = p+1 : continue while
						if DQ_ON then *p = 32
						if *p = 39 then SQ_ON = TRUE
						if SQ_ON then *p = 32
						p= p +1
					wend
					if instr(lcase(BSrc), " rem ") then BSrc = left(BSrc,instr(lcase(BSrc), " rem "))

					p = strptr(BSrc)
			 
					'' Extract tokens that look like keywords
					while *p
						'' Is not alpha_
						while not ((*p > 64 and *p < 91) or (*p > 96 and *p < 123) or *p = 95)
							p = p + 1
							if *p = 0 then exit while
						wend
						'' is alpha_9
						p1 = p
						do
							while (*p > 64 and *p < 91) or (*p > 96 and *p < 123) or *p = 95
								p = p + 1
								if *p = 0 then exit while
							wend
							p = p + 1
						loop while p1 <> (p-1) and (*(p-1) > 47 and *(p-1) < 58)
						p-=1
						'' When a token is found check to see if it a keyword
						if p1 <> p then
							if IsKeyword(@GetKeyWord(0), ubound(GetKeyWord)+1, mid(BSrc,p1-cptr(byte ptr,strptr(BSrc))+1), p-p1) then
								mid(Src,p1-cptr(byte ptr,strptr(BSrc))+1) = CC(mid(BSrc, p1-cptr(byte ptr,strptr(BSrc))+1,p-p1))
							end if
						end if
					wend
					'print #FP2, id; trim(Src)
					OutPutDocument = OutPutDocument + id + trim(Src) + chr(10)
				end if
			next
			
			IupSetAttribute( sciPoseidon, "VALUE", OutPutDocument )
			IupSetInt( sciPoseidon, "FIRSTVISIBLELINE", firstline )
			IupSetInt( sciPoseidon, "CARETPOS", caretPos )
		end if
	end if

end sub


'****************************IUP Callback Fucctions********************************
private function goButton_ACTION cdecl( ih as Ihandle ptr ) as integer

	dim as Ihandle ptr radio = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "r0" )
	if( radio <> null ) then
		if( IupGetInt( radio, "VALUE" ) = 1 ) then CCASE = 0
	end if
		
	radio = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "r1" )
	if( radio <> null ) then
		if( IupGetInt( radio, "VALUE" ) = 1 ) then CCASE = 1
	end if

	radio = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "r2" )
	if( radio <> null ) then
		if( IupGetInt( radio, "VALUE" ) = 1 ) then CCASE = 2
	end if
	
	radio = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "identText" )
	if( radio <> null ) then
		
		dim as string value = trim( IupGetAttribute( radio, "VALUE" )[0] )
		if( value <> "" ) then
			indents = val( value )
			go( 32 )
		else
			indents = 1
			go( 9 )
		end if
	end if
	
	IupHide( THIS_MAINDIALOG_HANDLE )
	
	return IUP_DEFAULT

end function


private function closeButton_ACTION cdecl( ih as Ihandle ptr ) as integer

	IupHide( THIS_MAINDIALOG_HANDLE )
	return IUP_DEFAULT
end function


' DLL
extern "C"
	declare sub poseidon_Dll_Go alias "poseidon_Dll_Go" ( _dllFullPath as zstring ptr )
	declare sub poseidon_Dll_Release alias "poseidon_Dll_Release" ()
	
	
	' **************************************** The Main *************************************************
	sub poseidon_Dll_Go alias "poseidon_Dll_Go"( _dllFullPath as zstring ptr ) export

		' The main IupOpen() is already ran by poseidonFB, no need anymore
		POSEIDON_HANDLE = IupGetHandle( "POSEIDON_MAIN_DIALOG" ) ' Get poseidonFB main dialog handle(IUP)
		
		if( POSEIDON_HANDLE <> 0 ) then

			' Since poseidonFB rev.437, check "NAME" to get poseidonFB / poseidonD
			if( IupGetAttribute( POSEIDON_HANDLE, "NAME" )[0] = "poseidonFB" ) then
			
				if( THIS_MAINDIALOG_HANDLE = 0 ) then
				
					dim as Ihandle ptr radioKeywordCase0 = IupToggle( "UPEERCASE", 0 )
					dim as Ihandle ptr radioKeywordCase1 = IupToggle( "Mixercase", 0 )
					dim as Ihandle ptr radioKeywordCase2 = IupToggle( "lowercase", 0 )
					IupSetAttributes( radioKeywordCase0, "NAME=r0" )
					IupSetAttributes( radioKeywordCase1, "NAME=r1" )
					IupSetAttributes( radioKeywordCase2, "NAME=r2" )
					
					dim as Ihandle ptr hBoxKeywordCase = IupHbox( radioKeywordCase0, radioKeywordCase1, radioKeywordCase2, null )
					IupSetAttributes( hBoxKeywordCase, "GAP=30,MARGIN=30x,ALIGNMENT=ACENTER" )

					dim as Ihandle ptr frameKeywordCase = IupFrame( IupRadio( hBoxKeywordCase ) )
					IupSetAttributes( frameKeywordCase, "SIZE=160x,GAP=1" )
					IupSetAttribute( frameKeywordCase, "TITLE", "Convert KeyWord Case" )
					
					dim as Ihandle ptr identLabel = IupLabel( "Number of spaces to Indent( null = TAB ): " )
					dim as Ihandle ptr identText = IupText( null )
					IupSetAttributes( identText, "NAME=identText" )
					dim as Ihandle ptr hBoxIdent = IupHbox( identLabel, identText, null )

					dim as Ihandle ptr goButton = IupButton( "  Go  ", 0 )
					dim as Ihandle ptr closeButton = IupButton( " Close ", 0 )
					IupSetCallback( goButton, "ACTION", cast(Icallback, @goButton_ACTION ) )
					IupSetCallback( closeButton, "ACTION", cast(Icallback, @closeButton_ACTION ) )
					
					dim as Ihandle ptr vBoxLayout = IupVbox( frameKeywordCase, hBoxIdent, IupHbox( IupFill, goButton, closeButton, 0 ), 0 )
					IupSetAttributes( vBoxLayout, "ALIGNMENT=ARIGHT,MARGIN=2x1" )
					
					THIS_MAINDIALOG_HANDLE = IupDialog( vBoxLayout )
					IupSetAttributes( THIS_MAINDIALOG_HANDLE, "TITLE=FBStyle,RESIZE=NO,PARENTDIALOG=POSEIDON_MAIN_DIALOG,ZORDER=TOP,OPACITY=210,SHRINK=YES" )
					IupSetHandle( "THIS_MAINDIALOG_HANDLE", THIS_MAINDIALOG_HANDLE )


					IupShowXY( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTERPARENT )
				else
					
					IupShow( THIS_MAINDIALOG_HANDLE )
				end if
			else
			
				IupMessageError( POSEIDON_HANDLE, "Sorry, this plugin is for freeBASIC use only!" )
			end if
		end if
	end sub
	
	
	'When poseidonFB quit, trigger the poseidonFB_Dll_Release()
	sub poseidon_Dll_Release alias "poseidon_Dll_Release"() export
		
		if( THIS_MAINDIALOG_HANDLE <> 0 ) then IupDestroy( THIS_MAINDIALOG_HANDLE )

	end sub
	
end extern