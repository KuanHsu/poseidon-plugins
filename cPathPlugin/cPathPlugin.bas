﻿#include once "IUP/iup.bi"


type UNIT
	as string _name, _x32, _x64
end type


'compile with -dll option
extern "C"
	declare sub poseidon_Dll_Go alias "poseidon_Dll_Go" ( _dllFullPath as zstring ptr )
	declare sub poseidon_Dll_Release alias "poseidon_Dll_Release" ()
	
	
	' shared variables
	dim shared as Ihandle ptr THIS_MAINDIALOG_HANDLE
	dim shared as Ihandle ptr POSEIDON_HANDLE
	dim shared as UNIT FBCunit()
	dim shared as Integer LISTINDEX
	dim shared as string  DLLPATH

	function normalize( _s as string ) as string
		
		dim result as string
		
		for i as integer = 0 to len( _s ) - 1
			if( _s[i] = 92 ) then result = result + "/" else result = result + chr( _s[i] )
		next
		
		return result

	end function


	function button32_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		dim as Ihandle ptr fileDlg = IupFileDlg()

		IupSetAttribute( fileDlg, "DIALOGTYPE", "OPEN" )
		IupSetAttribute( fileDlg, "TITLE", "FBC Compiler Path" )
		IupSetAttribute( fileDlg, "EXTFILTER", "Execute Files|*.exe|All Files|*.*" )
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			dim as zstring ptr fileStringz = IupGetAttribute( fileDlg, "VALUE" )
			dim as string fileString, _fileString = trim( fileStringz[0] )
			
			fileString = normalize( _fileString )
			if( len( dir( fileString ) ) < 1 ) then

				IupMessage( "Error", "File not existed!" )
			else
				
				dim as Ihandle ptr fbc32Handle = IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBC32" )
				if( fbc32Handle <> 0 ) then IupSetAttribute( fbc32Handle, "VALUE", fileString )
			end if
		end if
		
		return IUP_DEFAULT
		
	end function

	function button64_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		dim as Ihandle ptr fileDlg = IupFileDlg()

		IupSetAttribute( fileDlg, "DIALOGTYPE", "OPEN" )
		IupSetAttribute( fileDlg, "TITLE", "FBC Compiler Path" )
		IupSetAttribute( fileDlg, "EXTFILTER", "Execute Files|*.exe|All Files|*.*" )
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			dim as zstring ptr fileStringz = IupGetAttribute( fileDlg, "VALUE" )
			dim as string fileString, _fileString = trim( fileStringz[0] )
			
			for i as integer = 0 to len( _fileString ) - 1
				if( _fileString[i] = 92 ) then fileString = fileString + "/" else fileString = fileString + chr( _fileString[i] )
			next
			
			if( len( dir( fileString ) ) < 1 ) then

				IupMessage( "Error", "File not existed!" )
			else
				
				dim as Ihandle ptr fbc64Handle = IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBC64" )
				if( fbc64Handle <> 0 ) then IupSetAttribute( fbc64Handle, "VALUE", fileString )
			end if
		end if
		
		return IUP_DEFAULT
		
	end function

	function paramOK_NEW_ACTION cdecl( ih as Ihandle ptr ) as integer

		dim as string unitName = trim( IupGetAttribute( IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBCUNIT_NAME" ), "VALUE" )[0] )
		if( len( unitName ) < 1 ) then
			
			IupMessage( "Error", "No Name!" )
			return IUP_DEFAULT
		end if

		
		dim as integer arraySize = ubound( FBCunit )
		
		arraySize += 1
		redim preserve FBCunit(arraySize)
		FBCunit(arraySize)._name = unitName
		FBCunit(arraySize)._x32 = trim( IupGetAttribute( IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBC32" ), "VALUE" )[0] )
		FBCunit(arraySize)._x64 = trim( IupGetAttribute( IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBC64" ), "VALUE" )[0] )

		
		dim as Ihandle ptr listHandle = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "LIST" )
		if( listHandle <> 0 ) then
			IupSetAttribute( listHandle, "APPENDITEM",  FBCunit(arraySize)._name )
			IupSetInt( listHandle, "VALUE", arraySize + 1 )
			LISTINDEX = arraySize + 1
		else
			IupMessage( "Error", "" )
		end if
		
		return IUP_CLOSE
		
	end function

	function paramOK_EDIT_ACTION cdecl( ih as Ihandle ptr ) as integer

		dim as string unitName = trim( IupGetAttribute( IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBCUNIT_NAME" ), "VALUE" )[0] )
		if( len( unitName ) < 1 ) then
			
			IupMessage( "Error", "No Name!" )
			return IUP_DEFAULT
		end if
		
		dim as Ihandle ptr listHandle = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "LIST" )
		if( listHandle <> 0 ) then
			dim as integer index = IupGetInt( listHandle, "VALUE" ) 'Get current item #no
		
			if( index > 0 ) then
				if( index <= ubound( FBCunit ) + 1 ) then 
					index -= 1
					FBCunit(index)._name = unitName
					FBCunit(index)._x32 = trim( IupGetAttribute( IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBC32" ), "VALUE" )[0] )
					FBCunit(index)._x64 = trim( IupGetAttribute( IupGetDialogChild( IupGetHandle( "PARAMDLG_HANDLE" ), "FBC64" ), "VALUE" )[0] )
				
				end if
			end if
		end if

		return IUP_CLOSE
		
	end function


	function paramCancel_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		return IUP_CLOSE
		
	end function



	sub paramDialog( bEdit as boolean = false )

		if( THIS_MAINDIALOG_HANDLE <> 0 ) then
			
			dim as Ihandle ptr gBox, hBox, vBox
			dim as Ihandle ptr labelName, Label32, Label64, labelDummy, labelSEPARATOR
			dim as Ihandle ptr textName, text32, text64
			dim as Ihandle ptr button32, button64, buttonOK, buttonCancel
			

			labelName = IupLabel( "Name:" )
			Label32 = IupLabel( "x32 Path:" )
			Label64 = IupLabel( "x64 Path:" )
			labelDummy = IupLabel( "" )
			labelSEPARATOR = IupLabel( "" )
			
			IupSetAttributes( labelName, "SIZE=48x12" )
			IupSetAttributes( Label32, "SIZE=48x12" )
			IupSetAttributes( Label64, "SIZE=48x12" )
			IupSetAttributes( labelDummy, "SIZE=16x12" )
			IupSetAttributes( labelSEPARATOR, "SEPARATOR=HORIZONTAL" )
			
			textName = IupText( 0 )
			text32 = IupText( 0 )
			text64 = IupText( 0 )
			IupSetAttributes( textName, "SIZE=200x12,NAME=FBCUNIT_NAME" )
			IupSetAttributes( text32, "SIZE=200x12,NAME=FBC32" )
			IupSetAttributes( text64, "SIZE=200x12,NAME=FBC64" )
			
			button32 = IupButton( "...", "" )
			button64 = IupButton( "...", "" )
			IupSetAttributes( button32, "SIZE=16x12" )
			IupSetAttributes( button64, "SIZE=16x12" )
			IupSetCallback( button32, "ACTION", cast( Icallback, @button32_ACTION ) )
			IupSetCallback( button64, "ACTION", cast( Icallback, @button64_ACTION ) )
			
			gBox = IupGridBox(	labelName, textName, labelDummy, label32, text32, button32, label64, text64, button64, 0 )
			IupSetAttributes( gbox, "ORIENTATION=HORIZONTAL,NUMDIV=3,ALIGNMENTLIN=ALEFT,GAPLIN=5,EXPANDCHILDREN=HORIZONTAL,MARGIN=5x5" )
			
			buttonOK = IupButton( "OK", "" )
			buttonCancel = IupButton( "Cancel", "" )
			IupSetAttributes( buttonOK, "SIZE=40x16" )
			IupSetAttributes( buttonCancel, "SIZE=40x16" )
			
			' Different about New and Edit
			if( bEdit = true ) then
			
				IupSetCallback( buttonOK, "ACTION", cast( Icallback, @paramOK_EDIT_ACTION ) )
				IupSetAttribute( textName, "ACTIVE", "NO" )
				if( LISTINDEX > 0 ) then
					if( ubound( FBCunit ) >= LISTINDEX - 1 ) then
						IupSetAttributes( textName, "SIZE=200x12,NAME=FBCUNIT_NAME" )
						IupSetAttributes( text32, "SIZE=200x12,NAME=FBC32" )
						IupSetAttributes( text64, "SIZE=200x12,NAME=FBC64" )
						IupSetAttribute( textName, "VALUE", FBCunit(LISTINDEX-1)._name )
						IupSetAttribute( text32, "VALUE", FBCunit(LISTINDEX-1)._x32 )
						IupSetAttribute( text64, "VALUE", FBCunit(LISTINDEX-1)._x64 )
					end if
				end if
			else

				IupSetCallback( buttonOK, "ACTION", cast( Icallback, @paramOK_NEW_ACTION ) )
			end if
				
			IupSetCallback( buttonCancel, "ACTION", cast( Icallback, @paramCancel_ACTION ) )


			hBox = IupHbox( IupFill(), buttonOK, buttonCancel, 0 )
			IupSetAttributes( hBox, "ALIGNMENT=ABOTTOM,GAP=2,EXPANDCHILDREN=YES,MARGIN=2x2" )
			
			vBox = IupVbox( gBox, labelSEPARATOR, hBox, 0 )
			IupSetAttributes( vBox, "ALIGNMENT=ACENTER,MARGIN=5x5,GAP=0,EXPANDCHILDREN=YES" )
		
			dim as Ihandle ptr paramDlg = IupDialog( vBox )
			IupSetAttributes( paramDlg, "TITLE=FBC_PATH,MAXBOX=NO,MINBOX=NO,RESIZE=NO,TOPMOST=YES,OPACITY=200" )
			IupSetHandle( "PARAMDLG_HANDLE", paramDlg )
			
			IupPopup( paramDlg, IUP_MOUSEPOS, IUP_MOUSEPOS )
		else
		
			IupMessage( "NULL", "" )
		end if


	end sub




	' Main Dialog
	function addButton_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		paramDialog( false )
		
		return IUP_DEFAULT
		
	end function

	function delButton_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		dim as Ihandle ptr listHandle = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "LIST" )
		if( listHandle <> 0 ) then
			dim as integer index = IupGetInt( listHandle, "VALUE" ) 'Get current item #no
			IupSetAttribute( listHandle, "REMOVEITEM", str( index ) )
			
			
			if( ubound(FBCunit) = 0 ) then
			
				erase FBCunit
				LISTINDEX = 0
			elseif( ubound(FBCunit) > 0 ) then
				
				dim as integer arrayIndex
				dim as UNIT tempFBCUNIT()
				redim tempFBCUNIT(ubound(FBCunit)-1)
				
				
				for i as integer = 0 to ubound(FBCunit)
					if( i <> index - 1 ) then 
						tempFBCUNIT(arrayIndex) = FBCunit(i)
						arrayIndex += 1
					end if
				next
				
				redim FBCunit(ubound(tempFBCUNIT))
				for i as integer = 0 to ubound(FBCunit)
					FBCUNIT(i) = tempFBCUNIT(i)
				next
				
				if( index = 1 ) then
					IupSetInt( listHandle, "VALUE", 1 )
					LISTINDEX = 1
				elseif( index = IupGetInt( listHandle, "COUNT" ) + 1 ) then
					IupSetInt( listHandle, "VALUE", index - 1 )
					LISTINDEX = index - 1
				else
					IupSetInt( listHandle, "VALUE", index )
					LISTINDEX = index
				end if
			end if

		end if
		
		return IUP_DEFAULT
		
	end function

	function editButton_ACTION cdecl( ih as Ihandle ptr ) as integer

		dim as Ihandle ptr listHandle = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "LIST" )
		if( listHandle <> 0 ) then
			dim as integer index = IupGetInt( listHandle, "VALUE" ) 'Get current item #no
			if( index > 0 ) then paramDialog( true )
		end if
		
		return IUP_DEFAULT
		
	end function

	function applyButton_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		if( LISTINDEX > 0 ) then
			if( ubound( FBCunit ) >= LISTINDEX - 1 ) then

				' We can send command message by using POSEIDON_COMMANDLINE
				dim as Ihandle ptr _command = IupGetDialogChild( POSEIDON_HANDLE, "POSEIDON_COMMANDLINE" )
				if( _command <> 0 ) then 
					IupSetAttribute( _command, "VALUE", "FBCx32," + FBCunit(LISTINDEX-1)._x32 ) ' Set poseidonFB x32 compiler path
					IupSetAttribute( _command, "VALUE", "FBCx64," + FBCunit(LISTINDEX-1)._x64 )	' Set poseidonFB x64 compiler path
				end if

			end if
		end if
		
		return IUP_DEFAULT
		
	end function

	function list_ACTION cdecl( ih as Ihandle ptr, text as zstring ptr, item as integer, state as integer ) as integer ' Ihandle *ih, char *text, int item, int state
		
		if( state = 1 ) then LISTINDEX = item
		
		return IUP_DEFAULT
		
	end function
	
	/'
	function THIS_MAINDIALOG_HANDLE_CLOSE cdecl( ih as Ihandle ptr ) as integer
		
		open "settings/cPath.ini" For output As #1
		
		for i as integer = 0 to ubound(FBCunit)
			dim as string s = FBCunit(i)._name + "," + FBCunit(i)._x32 + "," + FBCunit(i)._x64
			print #1, s
		next

		return IUP_DEFAULT
		
	end function
	'/

	sub poseidon_Dll_Go alias "poseidon_Dll_Go"( _dllFullPath as zstring ptr ) export

		' The main IupOpen() is already ran by poseidonFB, no need anymore
		POSEIDON_HANDLE = IupGetHandle( "POSEIDON_MAIN_DIALOG" ) ' Get poseidonFB main dialog handle(IUP)
		
		if( POSEIDON_HANDLE <> 0 ) then
		
			' Check the THIS_MAINDIALOG_HANDLE is already created......
			if( THIS_MAINDIALOG_HANDLE = 0 ) then
		
				#if defined(__fb_win32__)
				
					' Get DLL path
					DLLPATH = _dllFullPath[0]
					dim as integer slashPos = instrrev( dllPath, "/" )
					DLLPATH = left( dllPath, slashPos ) ' include last /				
				
				
					dim as Ihandle ptr list = iuplist( 0 )
					dim as Ihandle ptr addButton, delButton, editButton, applyButton

					IupSetAttributes( list, "EXPAND=YES,VISIBLELINES=10,NAME=LIST" )
					IupSetCallback( list, "ACTION", cast( Icallback, @list_ACTION ) )

					addButton = IupButton( "Add", "" )
					IupSetCallback( addButton, "ACTION", cast( Icallback, @addButton_ACTION ) )
					delButton = IupButton( "Del", "" )
					IupSetCallback( delButton, "ACTION", cast( Icallback, @delButton_ACTION ) )
					editButton = IupButton( "Edit", "" )
					IupSetCallback( editButton, "ACTION", cast( Icallback, @editButton_ACTION ) )
					applyButton = IupButton( "Apply", "" )
					IupSetCallback( applyButton, "ACTION", cast( Icallback, @applyButton_ACTION ) )

					IupSetAttributes( addButton, "FLAT=YES,SIZE=36x16" )
					IupSetAttributes( delButton, "FLAT=YES,SIZE=36x16" )
					IupSetAttributes( editButton, "FLAT=YES,SIZE=36x16" )
					IupSetAttributes( applyButton, "FLAT=YES,SIZE=108x16" )

					dim as Ihandle ptr buttonHbox = IupHbox( addButton, delButton, editButton, 0 )
					dim as Ihandle ptr vBox = IupVbox( list, buttonHbox, applyButton, 0 )

					THIS_MAINDIALOG_HANDLE = IupDialog(vbox)
					IupSetAttributes( THIS_MAINDIALOG_HANDLE, "MAXBOX=NO,MINBOX=NO,RESIZE=NO,PARENTDIALOG=POSEIDON_MAIN_DIALOG,OPACITY=200" )
					IupSetAttribute( THIS_MAINDIALOG_HANDLE, "TITLE", "Set Compiler Paths" )
					'IupSetCallback( THIS_MAINDIALOG_HANDLE, "CLOSE_CB", cast( Icallback, @THIS_MAINDIALOG_HANDLE_CLOSE ) )
					IupShowXY( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTER )

					' open ini
					open DLLPATH + "settings/cPath.ini" For input As #1
					dim as integer fileLength = lof( 1 )

					if fileLength > 0 then
						do until( eof(1) )
							
							dim as string params(2), tempStr, s
							dim as integer arrayIndex
							
							Line Input #1, s
							if( instr( s, "," ) > 0 ) then
								for i as integer = 0 to len( s ) - 1
									
									if( chr(s[i]) = "," ) then
										if( arrayIndex < 3 ) then
											params(arrayIndex) = tempStr
											arrayIndex += 1
											tempStr = ""
										end if
									else
										if( chr(s[i]) <> """" ) then tempStr = tempStr + chr(s[i])
									end if
								next

								if( arrayIndex = 2 ) then
									
									params(2) = tempStr
									
									dim as integer arraySize = ubound( FBCunit )
									arraySize += 1
									redim preserve FBCunit(arraySize)
									FBCunit(arraySize)._name = trim( normalize( params(0) ) )
									FBCunit(arraySize)._x32 = trim( normalize( params(1) ) )
									FBCunit(arraySize)._x64 = trim( normalize( params(2) ) )
								end if
							end if
						loop

						for i as integer = 0 to ubound(FBCunit)
							IupSetAttribute( list, "APPENDITEM", FBCunit(i)._name )
						next
					end if

					close #1
					/'
					open "settings/editorSettings.ini" For input Encoding "utf8" As #1
					dim as integer fileLength = lof( 1 )
					dim as boolean bInBlock


					if fileLength > 0 then
						do until( eof(1) )
							
							dim as string params(2), tempStr, s
							dim as integer arrayIndex
							
							Line Input #1, s
							if( s = "[buildtools]" ) then
								bInBlock = true
								continue do
							elseif( s = "[parser]" ) then
								exit do
							else
								if( bInBlock = true ) then
									if( instr( s, "," ) > 0 ) then
										if( chr(s[0]) = "'" ) then
											for i as integer = 1 to len( s ) - 1
												
												if( chr(s[i]) = "," ) then
													if( arrayIndex < 3 ) then
														params(arrayIndex) = tempStr
														arrayIndex += 1
														tempStr = ""
													end if
												else
													if( chr(s[i]) <> """" ) then tempStr = tempStr + chr(s[i])
												end if
											next

											if( arrayIndex = 2 ) then
												
												params(2) = tempStr
												
												dim as integer arraySize = ubound( FBCunit )
												arraySize += 1
												redim preserve FBCunit(arraySize)
												FBCunit(arraySize)._name = trim( normalize( params(0) ) )
												FBCunit(arraySize)._x32 = trim( normalize( params(1) ) )
												FBCunit(arraySize)._x64 = trim( normalize( params(2) ) )
											end if
										end if
									end if
								end if
							end if
						loop

						for i as integer = 0 to ubound(FBCunit)
							IupSetAttribute( list, "APPENDITEM", FBCunit(i)._name )
						next
					end if
					
					close #1
					'/
				#else
					IupMessage( "Waring", "This Plugin is Windows Only" )
				#endif
			else
			
				IupShow( THIS_MAINDIALOG_HANDLE )
			end if
		end if
	end sub
	
	'When poseidonFB quit, trigger the poseidonFB_Dll_Release() that save the ini
	sub poseidon_Dll_Release alias "poseidon_Dll_Release"() export
		
		if( THIS_MAINDIALOG_HANDLE <> 0 ) then IupDestroy( THIS_MAINDIALOG_HANDLE )
		
		open DLLPATH + "settings/cPath.ini" For output As #1
		
		for i as integer = 0 to ubound(FBCunit)
			dim as string s = FBCunit(i)._name + "," + FBCunit(i)._x32 + "," + FBCunit(i)._x64
			print #1, s
		next
		close #1
		/'
		open "settings/editorSettings.ini" For input Encoding "utf8" As #1
		dim as integer fileLength = lof( 1 ), arrayIndex
		dim as string	s(2000)

		if fileLength > 0 then
			do until( eof(1) )
				Line Input #1, s(arrayIndex)
				arrayIndex += 1
			loop
		end if
		close #1
		
		open "settings/editorSettings.ini" For output Encoding "utf8" As #1
		for i as integer = 0 to arrayIndex - 1
			print #1, s(i)
			if( s(i) = "[buildtools]" ) then
				for j as integer = 0 to ubound(FBCunit)
					dim as string _s = "'" + FBCunit(j)._name + "," + FBCunit(j)._x32 + "," + FBCunit(j)._x64
					print #1, _s
				next
			end if
		next
		close #1
		'/
	end sub
	
end extern