﻿#include once "IUP/iup.bi"
#include once "IUP/iup_scintilla.bi"
#include once "file.bi"

'compile with -dll option
'dim shared as Ihandle ptr	THIS_MAINDIALOG_HANDLE
dim shared as Ihandle ptr	POSEIDON_HANDLE


extern "C"
	
	declare sub poseidon_Dll_Go alias "poseidon_Dll_Go" ( _dllFullPath as zstring ptr )
	declare sub poseidon_Dll_Release alias "poseidon_Dll_Release" ()
	
	declare function IupExecute( filename as zstring ptr, parameters as zstring ptr ) as integer 'IUP function

	' **************************************** The Main *************************************************
	sub poseidon_Dll_Go alias "poseidon_Dll_Go"( _dllFullPath as zstring ptr ) export

		' The main IupOpen() is already ran by poseidonFB, no need anymore
		POSEIDON_HANDLE = IupGetHandle( "POSEIDON_MAIN_DIALOG" ) ' Get poseidonFB main dialog handle(IUP)
		
		if( POSEIDON_HANDLE <> 0 ) then
			
			#ifdef __FB_WIN32__
				dim as Ihandle ptr documentPTR = IupGetFocus()
				
				if( documentPTR <> 0 ) then
				
					dim as string fullPath = IupGetAttribute( documentPTR, "NAME" )[0]
					if( FileExists( fullPath ) ) then
						
						dim index as integer = InStrRev( fullPath, "/" )
						if( index > 0 ) then
							
							'fullPath = left( fullPath, index - 1 )
							IupExecute( "cmd.exe", "/k cd /D " + left( fullPath, index - 1 ) ) ' Fire-and-forget
							/'
							dim as string currentDIR = curdir
							if( ChDir( fullPath ) = 0 ) then
							
								IupExecute( "cmd", "" ) 
								ChDir( currentDIR )
							end if
							'/
						end if
					
					end if
				end if
			#else
				
				IupMessage( "Warring", "This Plugin Is Windows Only." )
			#endif
		end if
	end sub
	
	
	'When poseidonFB quit, trigger the poseidonFB_Dll_Release()
	sub poseidon_Dll_Release alias "poseidon_Dll_Release"() export
	end sub
	
end extern