﻿#include once "IUP/iup.bi"
#include once "IUP/iup_scintilla.bi"
#include once "string.bi"

#DEFINE NL !"\n"



'compile with -dll option
extern "C"
	declare sub poseidon_Dll_Go alias "poseidon_Dll_Go" ( _dllFullPath as zstring ptr )
	declare sub poseidon_Dll_Release alias "poseidon_Dll_Release" ()

	type POS_UNIT
		as integer _start, _length
	end type

	type PARAMETER
		as	string			_code
		as	integer			_paramsCount, index
		as	POS_UNIT		_params(any,any)
	end type
	
	type SNIPPET_UNIT
		
		as string			shortCut, title, description, fullPath
		as PARAMETER		_object
	
	end type	
	
	' shared variables
	dim shared as Ihandle ptr	THIS_MAINDIALOG_HANDLE
	dim shared as Ihandle ptr	POSEIDON_HANDLE, WORKING_POSEIDON_DOCUMENT_HANDLE
	
	dim shared as Ihandle ptr	editContainerLabel, runContainerLabel
	dim shared as Ihandle ptr	sciEdit, sciRun
	dim shared as Ihandle ptr	editFullPathText, runFullPathText
	dim shared as Ihandle ptr	tabs
	
	
	dim shared as PARAMETER		param
	dim shared as PARAMETER		restoreParam
	dim shared as PARAMETER		newParam
	
	dim shared as SNIPPET_UNIT	snippetManager(any)
	
	
	dim shared as string 		DLLPATH, editFullPath, runFullPath, display0, display
	dim shared as string 		Language


	private sub setScintillaStyle( sci as Ihandle ptr )

		if( Language = "d" ) then

			IupSetAttribute( sci, "LEXERLANGUAGE", "d" )
			IupSetAttribute( sci, "KEYWORDS0", "abstract alias align asm assert auto body bool break byte case cast catch cdouble cent cfloat char class const continue creal dchar debug default delegate delete deprecated do double" )
			IupSetAttribute( sci, "KEYWORDS1", "else enum export extern false final finally float for foreach foreach_reverse function goto idouble if ifloat immutable import in inout int interface invariant ireal is lazy long macro mixin module" )
			IupSetAttribute( sci, "KEYWORDS4", "new nothrow null out override package pragma private protected public pure real ref return scope shared short static struct super switch synchronized template this throw true try typedef typeid typeof" )
			IupSetAttribute( sci, "KEYWORDS5", "ubyte ucent uint ulong union unittest ushort version void volatile wchar while with __FILE__ __FILE_FULL_PATH__ __MODULE__ __LINE__ __FUNCTION__ __PRETTY_FUNCTION__ __gshared __traits __vector __parameters" )

			IupSetAttribute( sci, "STYLECLEARALL", "YES" )
			
			IupSetAttribute( sci, "STYLEFGCOLOR1", "0 128 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR2", "0 128 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR3", "0 128 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR4", "0 128 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR5", "128 128 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR10", "128 0 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR12", "0 128 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR13", "127 127 127" )
			IupSetAttribute( sci, "STYLEFGCOLOR14", "0 0 0" )
			IupSetAttribute( sci, "STYLEFGCOLOR6", "0 0 255" )
			IupSetAttribute( sci, "STYLEFGCOLOR7", "255 127 39" )
			IupSetAttribute( sci, "STYLEFGCOLOR20", "63 72 204" )
			IupSetAttribute( sci, "STYLEFGCOLOR21", "0 162 232" )
		else

			IupSetAttribute( sci, "LEXERLANGUAGE", "freebasic" )
			IupSetAttribute( sci, "KEYWORDS0", "#assert #define #else #elseif #endif #endmacro #error #if #ifdef #ifndef #inclib #include #lang #libpath #line #macro #pragma #print #undef $dynamic $include $static $lang abs abstract access acos add alias allocate alpha and andalso any append as assert assertwarn asc asin asm atan2 atn base beep bin binary bit bitreset bitset bload boolean bsave byref byte byval call callocate case cast cbyte cdbl cdecl chain chdir" )
			IupSetAttribute( sci, "KEYWORDS1", "chr cint circle class clear clng clngint close cls color command common condbroadcast condcreate conddestroy condsignal condwait const constructor continue cos cptr cshort csign csng csrlin cubyte cuint culng culngint cunsg curdir cushort custom cvd cvi cvl cvlongint cvs cvshort data date dateadd datediff datepart dateserial datevalue day deallocate declare defbyte defdbl defined defint deflng deflongint defshort defsng defstr defubyte defuint defulongint defushort delete destructor dim dir do double draw dylibfree dylibload dylibsymbol else elseif encoding end enum environ eof eqv erase erfn erl ermn err error event exec exepath exit exp export extends extern false field fileattr filecopy filedatetime fileexists filelen fix flip for format frac fre freefile function get getjoystick getkey getmouse gosub goto hex hibyte hiword" )
			IupSetAttribute( sci, "KEYWORDS2", "hour if iif imageconvertrow imagecreate imagedestroy imageinfo imp implements import inkey inp input instr instrrev int integer is isdate isredirected kill lbound lcase left len let lib line lobyte loc local locate lock lof log long longint loop loword lpos lprint lset ltrim mid minute mkd mkdir mki mkl mklongint mks mkshort mod month monthname multikey mutexcreate mutexdestroy mutexlock mutexunlock naked name namespace next new not now object oct offsetof on once open operator option or orelse out output overload override paint palette pascal pcopy peek pmap point pointcoord pointer poke pos preserve preset print private procptr property protected pset ptr public put random randomize read reallocate redim rem reset restore resume return rgb rgba right rmdir" )
			IupSetAttribute( sci, "KEYWORDS3", "rnd rset rtrim run sadd scope screen screencopy screencontrol screenevent screeninfo screenglproc screenlist screenlock screenptr screenres screenset screensync screenunlock second seek select setdate setenviron setmouse settime sgn shared shell shl shr short sin single sizeof sleep space spc sqr static stdcall step stick stop str strig string strptr sub swap system tab tan then this threadcall threadcreate threaddetach threadwait time timeserial timevalue timer to trans trim true type typeof ubound ubyte ucase uinteger ulong ulongint union unlock unsigned until ushort using va_arg va_first va_next val vallng valint valuint valulng var varptr view virtual wait wbin wchr weekday weekdayname wend while whex width window windowtitle winput with woct write wspace wstr wstring xor year zstring" )

			IupSetAttribute( sci, "STYLECLEARALL", "YES")
			
			IupSetAttribute( sci, "STYLEFGCOLOR5", "127 127 127" )	'SCE_B_PREPROCESSOR 5
			IupSetAttribute( sci, "STYLEFGCOLOR3", "0 0 255" )		'SCE_B_KEYWORD 3
			IupSetAttribute( sci, "STYLEFGCOLOR10", "225 127 39" )	'SCE_B_KEYWORD2 10
			IupSetAttribute( sci, "STYLEFGCOLOR11", "63 72 204" )	'SCE_B_KEYWORD3 11
			IupSetAttribute( sci, "STYLEFGCOLOR12", "0 162 232" )	'SCE_B_KEYWORD4 12
			IupSetAttribute( sci, "STYLEFGCOLOR2", "128 128 0" )	'SCE_B_NUMBER 2
			IupSetAttribute( sci, "STYLEFGCOLOR4", "128 0 0" )		'SCE_B_STRING 4
			IupSetAttribute( sci, "STYLEFGCOLOR1", "0 128 0" )		'SCE_B_COMMENT 1
			'IupSetAttribute( sci, "STYLEBGCOLOR1", "255 255 255" )	'SCE_B_COMMENT 1
			IupSetAttribute( sci, "STYLEFGCOLOR19", "0 128 0" )		'SCE_B_COMMENT 1
			'IupSetAttribute( sci, "STYLEBGCOLOR19", "255 255 255" )	'SCE_B_COMMENT 1
		end if
			
		IupSetAttributeId( sci, "INDICATORSTYLE", 0, "FULLBOX" )
		IupSetAttributeId( sci, "INDICATORALPHA", 0, "100" )
		IupSetAttributeId( sci, "INDICATORSTYLE", 1, "FULLBOX" )
		IupSetAttributeId( sci, "INDICATORALPHA", 1, "100" )
			
		'IupScintillaSendMessage( sci, 2563, 1, 0 ) 'SCI_SETMULTIPLESELECTION 2563
		'IupScintillaSendMessage( sci, 2565, 1, 0 ) ' SCI_SETADDITIONALSELECTIONTYPING 2565


	end sub

	
	private function normalize( _s as string ) as string
		
		dim result as string
		
		for i as integer = 0 to len( _s ) - 1
			if( _s[i] = 92 ) then result = result + "/" else result = result + chr( _s[i] )
		next
		
		return result

	end function

	
	private function splitINIdata( byval s as string, byref _left as string, byref _right as string, byval symbol as string = "=" ) as boolean
	
		dim as integer equalPos = instr( s, symbol )
		
		if( equalPos > 0 ) then
		
			_left = left( s, equalPos - 1 )
			_right = right( s, len(s) - equalPos )
		else
			return false
		end if
	
		return true
	
	end function
	
	
	private	function loadSnippet( fullPath as string ) as SNIPPET_UNIT

		dim as SNIPPET_UNIT result

		if( len( dir( fullPath ) ) > 0 ) then
		
			open fullPath For input As #2
			dim as integer fileLength = lof( 2 ), _paramCount
			dim as string mode, _left, _right, _x, _y, _start, _length
			
			dim as PARAMETER tempParam
			
			redim tempParam._params(99,99) as POS_UNIT

			
			if fileLength > 0 then
				do until( eof(2) )
					
					dim as string s
					
					Line Input #2, s
					
					if( s = "[PROPERTIES]" ) then
						mode = "[PROPERTIES]"
						continue do
					elseif( s = "[CODE]" ) then
						mode = "[CODE]"
						continue do
					elseif( s = "[PARAMETER]" ) then
						mode = "[PARAMETER]"
						continue do
					end if
					
					select case mode
						case "[PROPERTIES]"
							if( splitINIdata( s, _left, _right ) ) then
								
								select case _left
									case "TITLE"
										result.title = _right
									case "DESCRIPTION"
										result.description = _right
									case "SHORTCUT"
										result.shortCut = _right
									case else
								end select
							end if
						
						case "[CODE]"
							if( len(tempParam._code) = 0 ) then
								tempParam._code = s
							else
								tempParam._code += ( chr(10) + s )
							end if
						
						case "[PARAMETER]"
							splitINIdata( s, _left, _right, ":" )
							splitINIdata( _left, _x, _y, "," )
							splitINIdata( _right, _start, _length, "," )
							
							if( _left <> "" AND _right <> "" AND _start <> "" AND _length <> "" AND _x <> "" AND _y <> "" ) then 
								
								tempParam._params(val(_x),val(_y))._start = val(_start)
								tempParam._params(val(_x),val(_y))._length = val(_length)
								_paramCount = val(_x) + 1
							end if
						case else
					
					end select

				loop
				
				if( len(tempParam._code) = 0 ) then
					
					IupMessageError( THIS_MAINDIALOG_HANDLE, fullPath + " File format Error!" + chr(10) + "No code data" )
					close #2
					return result
				end if
				
				tempParam._paramsCount = _paramCount
				tempParam.index = 0
				'IupSetAttribute( sciEdit, "VALUE", tempParam._code )
				
				if( _paramCount > 0 ) then redim preserve tempParam._params(_paramCount - 1, 99) as POS_UNIT else erase tempParam._params
				
				result._object = tempParam
				result.fullPath = fullPath
			end if	
			
			close #2
		end if
	
		return result
		
	end function
	
	
	private function loadSettings() as boolean
	
		dim as string fullPath = DLLPATH + "CodeSnippets.ini"
		
		' Check existed
		if( len( dir( fullPath ) ) < 1 ) then

			IupMessageError( THIS_MAINDIALOG_HANDLE, "CodeSnippets.ini isn't existed!" )
			return false
		end if
		
		open fullPath For input As #1
		dim as integer fileLength = lof( 1 )

		if( fileLength > 0 ) then

			dim as string	s, mode, _left, _right
			dim as boolean	bPreLoad
			
			do until( eof(1) )

				Line Input #1, s
				s = trim(s)
				
				if( s = "[OPTIONS]" ) then
					mode = "[OPTIONS]"
					continue do
				elseif( s = "[SNIPPETS]" ) then
					mode = "[SNIPPETS]"
					continue do
				elseif( s = "[SEARCHDIR]" ) then
					mode = "[SEARCHDIR]"
					continue do
				end if
				
				select case mode
					case "[OPTIONS]"
						if( splitINIdata( s, _left, _right ) ) then
							
							if( _left = "ShortCut" ) then
								if( _right = "OFF" ) then IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_shortCutToggle" ),"VALUE", "OFF" )
							elseif( _left = "Load" ) then
								if( _right = "ON" ) then 
									IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadToggle" ),"VALUE", "ON" )
									bPreLoad = true
								end if
							end if
						end if
					
					case "[SEARCHDIR]"
						
						dim as Ihandle ptr dirTextHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_textSearchDir" )
						if( dirTextHandle <> 0 ) then IupSetAttribute( dirTextHandle, "VALUE", s )
						
					case "[SNIPPETS]"

						if( len(s) > 0 ) then 
							dim as SNIPPET_UNIT sn = loadSnippet( s )
							
							if( len( sn.fullPath ) > 0 ) then
							
								redim preserve snippetManager( ubound(snippetManager) + 1 )
								snippetManager( ubound(snippetManager) ) = sn
								
								dim as Ihandle ptr listHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadList" )
								if( listHandle <> 0 ) then
								
									dim as string listItem = sn.shortCut
									
									for i as integer = len( sn.shortCut ) to 19
										listItem += " "
									next
									listItem += ( " : " + sn.title )
									IupSetAttribute( listHandle, "APPENDITEM", listItem )
								end if
							end if
						end if
						
					case else
				
				end select				
			loop
		end if
		
		close #1
		
		return true
		
	end function
	

	private sub saveSettings()
	
		dim as string	fullPath = DLLPATH + "CodeSnippets.ini"
		dim as string	_sc = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_shortCutToggle" ), "VALUE" )[0]
		dim as string	_pl = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadToggle" ), "VALUE" )[0]
		dim as string	_sd = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_textSearchDir" ), "VALUE" )[0]
		dim as string	document = "[OPTIONS]" + chr(10)
		
		document +=	( "ShortCut=" + _sc + chr(10) )
		document +=	( "Load=" + _pl + chr(10) )
		document +=	( "[SEARCHDIR]" + chr(10) )
		document +=	( _sd + chr(10) )
		document +=	( "[SNIPPETS]" + chr(10) )
		
		for i as integer = 0 to ubound(snippetManager)
			document +=	( snippetManager(i).fullPath + chr(10) )
		next

		open fullPath For output As #1
		print #1, document
		close #1
	
	end sub
	
	
	' From FreeBASIC Manual
	private function list_files( _sc as string ) as string
		
		dim as string _dir = normalize( IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_textSearchDir" ), "VALUE" )[0] )
		
		if( len(_dir) > 0 ) then

			if( right(_dir, 1 ) <> "/" ) then _dir += ( "/" )
		
			Dim As String filename = Dir( _dir + "*", &h20 ) ' Start a file search with the specified filespec/attrib *AND* get the first filename.
			Do While Len(filename) > 0 ' If len(filename) is 0, exit the loop: no more filenames are left to be read.

				'Print filename
				if( instr( filename, _sc + "_" ) = 1 ) then
			
					dim as SNIPPET_UNIT su = loadSnippet( _dir + filename )
					if( len( su.fullPath ) > 0 ) then return su._object._code
				end if
				
				filename = Dir() ' Search for (and get) the next item matching the initially specified filespec/attrib.
			Loop
		end if
		
		return ""
		
	End function


	private sub sort( _INDEX as integer, newStringLength as integer )
	
		dim pointArray() as POS_UNIT ptr
		
		redim pointArray(10000)
		
		dim as integer arrayIndex
		
		for x as integer = 0 to param._paramsCount - 1
			for y as integer = 0 to 99
				if( param._params( x, y )._length = 0 ) then
					exit for
				else
					if( x = _INDEX ) then param._params( x, y )._length = -1 * param._params( x, y )._length ' Mark the select
					pointArray(arrayIndex) = @param._params( x, y )
					arrayIndex += 1
				end if
			next
		next
		
		redim preserve pointArray(arrayIndex-1)

		dim temp as POS_UNIT ptr
		' Just bubble
		For i as integer = 0 To arrayIndex - 1
			For j as integer = i To arrayIndex - 1
				If pointArray(i)->_start > pointArray(j)->_start Then
					temp = pointArray(i) 							
					pointArray(i) = pointArray(j) 					
					pointArray(j) = Temp
				End If
			Next j
		Next i		


		dim as integer adjustPos
		
		for i as integer = 0 to arrayIndex -1
		
			' Hit
			if( pointArray(i)->_length < 0 ) then
			
				pointArray(i)->_start = pointArray(i)->_start + adjustPos

				pointArray(i)->_length = abs( pointArray(i)->_length )
				
				adjustPos += ( newStringLength - pointArray(i)->_length )
				
				pointArray(i)->_length = newStringLength
				
			else
				
				pointArray(i)->_start = pointArray(i)->_start + adjustPos
			end if
		next
	
	end sub
	
	
	private sub markSelections( _sci as Ihandle ptr, _param as PARAMETER ptr, INDICATOR as integer )
	
		dim as integer _Index = _param->index - 1
		dim as integer head, tail
		
		IupScintillaSendMessage( _sci, 2505, 0, IupGetInt( _sci, "COUNT" ) )
		
		for y as integer = 0 to 99
			
			if( _param->_params( _Index, y )._length > 0 ) then
				
				IupScintillaSendMessage( _sci, 2500, INDICATOR, 0 ) ' SCI_SETINDICATORCURRENT = 2500
				IupScintillaSendMessage( _sci, 2504, _param->_params( _Index, y )._start, _param->_params( _Index, y )._length ) ' SCI_INDICATORFILLRANGE =  2504
			else

				exit for
			end if
		next
	end sub
	
	private sub markAllParams( _sci as Ihandle ptr, _param as PARAMETER ptr )
	
		IupScintillaSendMessage( _sci, 2505, 0, IupGetInt( _sci, "COUNT" ) )
		
		dim as integer indicator

		for x as integer = 0 to _param->_paramsCount - 1
			for y as integer = 0 to 99
				
				if( _param->_params( x, y )._length > 0 ) then
					
					if( x < 32 ) then indicator = x else indicator = int( 31 * rnd )
					IupScintillaSendMessage( _sci, 2500, indicator, 0 ) ' SCI_SETINDICATORCURRENT = 2500
					IupScintillaSendMessage( _sci, 2504, _param->_params( x, y )._start, _param->_params( x, y )._length ) ' SCI_INDICATORFILLRANGE =  2504
				else

					exit for
				end if
			next
		next
	end sub
	
	
	private function buttonNew_ACTION cdecl( ih as Ihandle ptr ) as integer

		dim as integer result = IupMessageAlarm( THIS_MAINDIALOG_HANDLE, "Alarm", "Are you sure create new?", "YESNO" )
		if( result = 1 ) then 
		
			erase newParam._params
			
			newParam.index = 0
			newParam._paramsCount = 0
			newParam._code = ""
			
			display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
			IupSetAttribute( editContainerLabel, "TITLE", display0 )
			IupSetAttribute( editFullPathText, "VALUE", "" )
			IupSetAttribute( sciEdit, "VALUE", "" )
			IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Title" ), "VALUE", "" )
			IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Description" ), "VALUE", "" )
			IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_ShortCut" ), "VALUE", "" )
		end if
		
		
		return IUP_DEFAULT
	end function	
	
	
	private function buttonEdit_ACTION cdecl( ih as Ihandle ptr ) as integer

		dim as Ihandle ptr fileDlg = IupFileDlg()
		
		IupSetAttribute( fileDlg, "PARENTDIALOG", "THIS_MAINDIALOG_HANDLE" )
		IupSetAttribute( fileDlg, "DIALOGTYPE", "OPEN" )
		IupSetAttribute( fileDlg, "TITLE", "Open Sinppet File" )
		IupSetAttribute( fileDlg, "EXTFILTER", "Snippet Files|*.snippet|All Files|*.*" )
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			dim as string fullPath = IupGetAttribute( fileDlg, "VALUE" )[0]
			dim as SNIPPET_UNIT _sn = loadSnippet( fullPath )
			
			if( len( _sn.fullPath ) > 0 ) then
			
				newParam = _sn._object
				editFullPath = fullPath
				display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
				IupSetAttribute( editContainerLabel, "TITLE", display0 )
				IupSetAttribute( editFullPathText, "VALUE", editFullPath )
				
				IupSetAttribute( sciEdit, "VALUE", newParam._code )
				IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Title" ), "VALUE", _sn.title )
				IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Description" ), "VALUE", _sn.description )
				IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_ShortCut" ), "VALUE", _sn.shortCut )
			
			else
				
				IupMessageError( THIS_MAINDIALOG_HANDLE, "Loading " + editFullPath + chr(10) + "Error!"  )
			end if
		end if
		
		return IUP_DEFAULT
		
	end function
	
	
	private function buttonSave_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		dim as string _code = IupGetAttribute( sciEdit, "VALUE" )[0]
		
		if( len(trim(_code)) < 1 ) then
		
			dim as integer result = IupMessageAlarm( THIS_MAINDIALOG_HANDLE, "Alarm", "Code is null, continue?", "YESNO" )
			if( result = 2 ) then return IUP_DEFAULT
		end if
	
		dim as Ihandle ptr fileDlg = IupFileDlg()

		IupSetAttribute( fileDlg, "PARENTDIALOG", "THIS_MAINDIALOG_HANDLE" )
		IupSetAttribute( fileDlg, "DIALOGTYPE", "SAVE" )
		IupSetAttribute( fileDlg, "TITLE", "Save Snippet" )
		IupSetAttribute( fileDlg, "EXTFILTER", "Snippet Files|*.snippet|All Files|*.*" )
		IupSetAttribute( fileDlg, "FILE", "Snippet Files|*.snippet|All Files|*.*" )
		
		IupSetAttribute( fileDlg, "FILE", "*.snippet" )
		 
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			editFullPath = IupGetAttribute( fileDlg, "VALUE" )[0]

			dim as string _title = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Title" ), "VALUE" )[0]
			dim as string _description = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Description" ), "VALUE" )[0]
			dim as string _sc = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_ShortCut" ), "VALUE" )[0]


			dim as string document = "[PROPERTIES]" + chr(10)
			document +=	( "TITLE=" + _title + chr(10) )
			document +=	( "DESCRIPTION=" + _description + chr(10) )
			document +=	( "SHORTCUT=" + _sc + chr(10) )

			document +=	( "[CODE]" + chr(10) )
			document +=	( _code + chr(10) )
			
			document +=	( "[PARAMETER]" )
			
			for x as integer = 0 to newParam._paramsCount - 1
				for y as integer = 0 to 99
					
					if( newParam._params(x,y)._length > 0 ) then
						document +=	( chr(10) + str(x) + "," + str(y) + ":" + str( newParam._params(x,y)._start ) + "," + str( newParam._params(x,y)._length ) )
					end if
				next
			next
			
			open editFullPath For output As #9
			print #9, document
			close #9
			
			IupSetAttribute( editFullPathText, "VALUE", editFullPath )
		end if

		return IUP_DEFAULT
		
	end function
	
	
	private function buttonAdd_ACTION cdecl( ih as Ihandle ptr ) as integer

		'if( newParam._paramsCount < 1 ) then return IUP_DEFAULT
		' CodeSnippets_labelCreate
		
		if( sciEdit <> 0 ) then
		
			dim as string targetText = IupGetAttribute( sciEdit, "SELECTEDTEXT" )[0]
			dim as integer targetTextLength = len( targetText )
			
			if( targetTextLength = 0 ) then 
	
				IupMessageError( sciEdit, "Length of Parameter is null" )
				return IUP_DEFAULT
			end if
			
			
			newParam._paramsCount += 1
			newParam.index = newParam._paramsCount
			redim preserve newParam._params(0 to ( newParam._paramsCount - 1 ),99)
			
			dim as integer _INDEX = newParam.index - 1
			
			/'
			SCI_SETSEARCHFLAGS = 2198,
			
			SCFIND_WHOLEWORD = 2,
			SCFIND_MATCHCASE = 4,
			SCFIND_WORDSTART = 0x00100000,
			SCFIND_REGEXP = 0x00200000,
			SCFIND_POSIX = 0x00400000,
			'/
			if( Language = "freebasic" ) then IupScintillaSendMessage( sciEdit, 2198, 2, 0 ) else IupScintillaSendMessage( sciEdit, 2198, 6, 0 )
			IupSetInt( sciEdit, "TARGETSTART", 0 )
			IupSetInt( sciEdit, "TARGETEND", -1 )
			
			dim as integer count, findPos = IupScintillaSendMessage( sciEdit, 2197, targetTextLength, cast(integer,strptr(targetText) ) )
			
			while( findPos > -1 )
			
				newParam._params(_INDEX, count )._start = findPos
				newParam._params(_INDEX, count )._length = targetTextLength
				
				count += 1

				IupSetInt( sciEdit, "TARGETSTART", findPos + targetTextLength )
				IupSetInt( sciEdit, "TARGETEND", -1 )
				findPos = IupScintillaSendMessage( sciEdit, 2197, targetTextLength, cast(integer,strptr(targetText) ) )
			wend
			
			markSelections( sciEdit, @newParam, 0 )
			
			IupSetAttribute( sciEdit, "SELECTIONPOS", "NONE" )
			display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
			IupSetAttribute( editContainerLabel, "TITLE", display0 )
			
		end if
	
		return IUP_DEFAULT
		
	end function
	
	
	private function buttonDel_ACTION cdecl( ih as Ihandle ptr ) as integer

		if( newParam._paramsCount < 1 ) then return IUP_DEFAULT

		if( sciEdit <> 0 ) then
		
			if( newParam._paramsCount > 1 ) then

				dim as PARAMETER tempParam
				dim as integer count
				dim as integer _INDEX = newParam.index - 1

				redim tempParam._params(0 to ( newParam._paramsCount - 2 ),99)
				
				for i as integer = 0 to newParam._paramsCount - 1
					if( i <> _INDEX ) then 
						for j as integer = 0 to 99
							tempParam._params(count,j) = newParam._params(i,j)
						next
						count += 1
					end if
				next
				
				erase newParam._params
				tempParam._paramsCount = newParam._paramsCount - 1
				tempParam._code = newParam._code
				if( newParam.index = 1 ) then
					tempParam.index = tempParam._paramsCount
				else
					tempParam.index = newParam.index - 1
				end if
				
				newParam = tempParam
				markSelections( sciEdit, @newParam, 0 )
			else
			
				newParam._paramsCount = 0
				newParam.index = 0
				erase newParam._params
				IupScintillaSendMessage( sciEdit, 2505, 0, IupGetInt( sciEdit, "COUNT" ) )
			end if

			IupSetAttribute( sciEdit, "SELECTIONPOS", "NONE" )
			display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
			IupSetAttribute( editContainerLabel, "TITLE", display0 )
		end if
	
		return IUP_DEFAULT
		
	end function	
	
	
	private function buttonLeft_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		if( newParam._paramsCount = 0 ) then return IUP_DEFAULT
		
		if( sciEdit <> 0 ) then
		
			if( newParam.index > 1 ) then newParam.index -= 1 else newParam.index = newParam._paramsCount

			if( editContainerLabel <> 0 ) then

				display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
				IupSetAttribute( editContainerLabel, "TITLE", display0 )
			end if
			
			IupSetAttribute( sciEdit, "SELECTIONPOS", "NONE" )
			markSelections( sciEdit, @newParam, 0 )
		end if

		return IUP_DEFAULT
		
	end function
	
	
	private function buttonRight_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		if( newParam._paramsCount = 0 ) then return IUP_DEFAULT
		
		if( sciEdit <> 0 ) then
		
			if( newParam.index < newParam._paramsCount  ) then newParam.index += 1 else newParam.index = 1

			if( editContainerLabel <> 0 ) then

				display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
				IupSetAttribute( editContainerLabel, "TITLE", display0 )
			end if
			
			IupSetAttribute( sciEdit, "SELECTIONPOS", "NONE" )
			markSelections( sciEdit, @newParam, 0 )
		end if
		
		return IUP_DEFAULT
		
	end function
	
	
	private function buttonOpen_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		dim as Ihandle ptr fileDlg = IupFileDlg()
		
		IupSetAttribute( fileDlg, "PARENTDIALOG", "THIS_MAINDIALOG_HANDLE" )
		IupSetAttribute( fileDlg, "DIALOGTYPE", "OPEN" )
		IupSetAttribute( fileDlg, "TITLE", "Open Sinppet File" )
		IupSetAttribute( fileDlg, "EXTFILTER", "Snippet Files|*.snippet|All Files|*.*" )
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			dim as string fullPath = IupGetAttribute( fileDlg, "VALUE" )[0]
			dim as SNIPPET_UNIT _sn = loadSnippet( fullPath )
			
			if( len( _sn.fullPath ) > 0 ) then
			
				param = _sn._object
				runFullPath = fullPath
				display = " " + format( param.index, "00" ) + " / " + format( param._paramsCount, "00" ) + " "
				IupSetAttribute( runContainerLabel, "TITLE", display )
				IupSetAttribute( runFullPathText, "VALUE", runFullPath )
				
				IupSetAttribute( sciRun, "READONLY", "NO" )
				IupSetAttribute( sciRun, "VALUE", param._code )
				IupSetAttribute( sciRun, "READONLY", "YES" )
				
				IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Title1" ), "VALUE", _sn.title )
				IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Description1" ), "VALUE", _sn.description )
				IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_ShortCut1" ), "VALUE", _sn.shortCut )
			else
				
				IupMessageError( THIS_MAINDIALOG_HANDLE, "Loading " + editFullPath + chr(10) + "Error!"  )
			end if		
		end if
		
		return IUP_DEFAULT
	
	end function


	private function iuputText_K_ANY cdecl( ih as Ihandle ptr, c as integer ) as integer
	
		dim as Ihandle ptr iuputDlg = IupGetHandle( "CodeSnippets_iuputDlg" )
		
		
		if( iuputDlg <> 0 ) then
			
			if( c = 13 ) then			'ENTER

				if( sciRun <> 0 ) then
					
					dim as string	iuputResult = IupGetAttribute( ih, "VALUE" )[0]
					dim as integer	_Index = param.index - 1, head, tail, y
					

					IupScintillaSendMessage( sciRun, 2505, 0, IupGetInt( sciRun, "COUNT" ) )

					' Get last y index
					for y = 0 to 99
						if( param._params( _Index, y )._length < 1 ) then exit for
					next
					
					dim as integer yTail = y - 1
					
					IupSetAttribute( sciRun, "READONLY", "NO" )
					
					for y = yTail to 0 step -1
						
						if( param._params( _Index, y )._length < 1 ) then exit for
						
						head = param._params( _Index, y )._start
						tail = head + param._params( _Index, y )._length
						
						IupScintillaSendMessage( sciRun, 2572, head, tail ) ' SCI_SETSELECTION 2572
						IupSetAttribute( sciRun, "SELECTEDTEXT", iuputResult )
					next
					
					IupSetAttribute( sciRun, "READONLY", "YES" )
					sort( param.index - 1, len( iuputResult ) )
					
					markSelections( sciRun, @param, 1 )
				end if

				IupDestroy( iuputDlg )
			
			elseif( c = 65307 ) then		' ESC

				IupDestroy( iuputDlg )
			end if
		
		end if
		
		return IUP_DEFAULT
	
	end function
	
	
	private sub createIuputDialog( x as integer, y as integer )
	
		dim as Ihandle ptr iuputText = IupText( 0 )
		IupSetAttributes( iuputText, "SIZE=100x" )
		IupSetCallback( iuputText, "K_ANY", cast( Icallback, @iuputText_K_ANY ) )
		
		dim as Ihandle ptr iuputDlg = IupDialog( iuputText )
		IupSetAttributes( iuputDlg, "BORDER=NO,RESIZE=NO,MAXBOX=NO,MINBOX=NO,MENUBOX=NO,OPACITY=198" )
		IupSetHandle( "CodeSnippets_iuputDlg", iuputDlg )
		
		IupSetFocus( iuputText )
		
		IupPopup( iuputDlg, x, y )
		
	end sub
	

	private function buttonLeft10_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		if( param._paramsCount = 0 ) then return IUP_DEFAULT
		
		if( sciRun <> 0 ) then
		
			if( param.index > 1 ) then param.index -= 1 else param.index = param._paramsCount

			if( runContainerLabel <> 0 ) then

				display = " " + format( param.index, "00" ) + " / " + format( param._paramsCount, "00" ) + " "
				IupSetAttribute( runContainerLabel, "TITLE", display )
			end if
			
			IupSetAttribute( sciRun, "SELECTIONPOS", "NONE" )
			markSelections( sciRun, @param, 0 )
		end if

		return IUP_DEFAULT
		
	end function
	
	private function buttonRight10_ACTION cdecl( ih as Ihandle ptr ) as integer
		
		if( param._paramsCount = 0 ) then return IUP_DEFAULT
		
		if( sciRun <> 0 ) then
		
			if( param.index < param._paramsCount  ) then param.index += 1 else param.index = 1

			if( runContainerLabel <> 0 ) then

				display = " " + format( param.index, "00" ) + " / " + format( param._paramsCount, "00" ) + " "
				IupSetAttribute( runContainerLabel, "TITLE", display )
			end if
			
			IupSetAttribute( sciRun, "SELECTIONPOS", "NONE" )
			markSelections( sciRun, @param, 0 )
		end if
		
		return IUP_DEFAULT
		
	end function
	
	
	private function sciRun_K_ANY cdecl( ih as Ihandle ptr, c as integer ) as integer
	
		if( c = 9 ) then ' TAB
			
			return buttonRight10_ACTION( 0 )
		elseif( c = 268435465 ) then
			
			return buttonLeft10_ACTION( 0 )
		elseif( c = 13 ) then
			
			if( param._params( param.index - 1, 0 )._length > 0 ) then

				dim as integer lineNumber = IupScintillaSendMessage( sciRun, 2166, param._params( param.index - 1, 0 )._start, 0 )
				IupSetInt( ih, "FIRSTVISIBLELINE", lineNumber )
				
				dim as string xy = IupGetAttribute( sciRun, "SCREENPOSITION" )[0], _x, _y 
				
				splitINIdata( xy, _x, _y, "," )
				createIuputDialog( val(_x) + 80, val(_y) - 20 )
			end if
		end if
	
		return IUP_DEFAULT
		
	end function
	
	
	private function sciRun_BUTTON_CB cdecl( ih as Ihandle ptr, button as integer, pressed as integer, x as integer, y as integer, status as zstring ptr ) as integer
		
		IupSetAttribute( ih, "READONLY", "YES" )
		
		dim as boolean bBingo

		if( button = IUP_BUTTON1 ) then 'Left Click
			if( iup_isdouble( status ) ) then 'Double Click
				
				return IUP_IGNORE
			else

				if( pressed = 1 ) then
					
					if( param.index < 1 ) then return IUP_DEFAULT
					
					dim as integer _pos = IupConvertXYToPos( ih, x, y ), Index = param.index - 1, head, tail

					for y as integer = 0 to 99
						
						if( param._params( Index, y )._length < 1 ) then exit for
					
						head = param._params( Index, y )._start
						tail = head + param._params( Index, y )._length
						
						' Click the Params
						if( _pos >= head AND _pos <= tail ) then

							createIuputDialog( IUP_MOUSEPOS, IUP_MOUSEPOS )
							return IUP_IGNORE
						end if
					next
				end if
			end if
		end if

		return IUP_DEFAULT
		
	end function
	
	
	private function quickAddtoPoseidon() as boolean
	
		dim as string word
		
		if( WORKING_POSEIDON_DOCUMENT_HANDLE <> 0 ) then

			dim as Ihandle ptr sciPoseidon = WORKING_POSEIDON_DOCUMENT_HANDLE 'cast( Ihandle ptr, IupGetAttribute( _MaintabsHandle, "VALUE_HANDLE" ) ) 'Get Active Doc Tab(iupscintilla handle)
			if( sciPoseidon <> 0 ) then

				' Get sciPoseidon current pos
				dim as string s
				dim as integer _pos = IupScintillaSendMessage( sciPoseidon, 2008, 0, 0 ) - 1' SCI_GETCURRENTPOS = 2008
				
				while( _pos > -1 )
					
					s = IupGetAttributeId( sciPoseidon, "CHAR", _pos )[0]
					if( s = " " OR s = "=" OR asc(s) = 9 OR asc(s) = 10 ) then
						exit while
					else
						word = s + word
					endif
					
					_pos -= 1
				wend
				
				_pos += 1
				
				if( len(word) > 0 ) then
				
					for i as integer = 0 to ubound(snippetManager)
						
						if( word = snippetManager(i).shortCut ) then

							IupScintillaSendMessage( sciPoseidon, 2645, _pos, len(word) ) ' SCI_SETSELECTION 2572
							IupSetAttribute( sciPoseidon, "ADD", snippetManager(i)._object._code )
							return true
						end if
					next
					
					' Not in snippetManager, try load file
					dim as string toPoseidonWord = list_files( word )
					if( len(toPoseidonWord) > 0 ) then
						
						IupScintillaSendMessage( sciPoseidon, 2645, _pos, len(word) ) ' SCI_SETSELECTION 2572
						IupSetAttribute( sciPoseidon, "ADD", toPoseidonWord )
						return true
					end if
					
				end if					
			end if
		end if		
		
		return false
	
	end function


	private function sendButton_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		if( WORKING_POSEIDON_DOCUMENT_HANDLE <> 0 ) then 

			dim as Ihandle ptr sciPoseidon = WORKING_POSEIDON_DOCUMENT_HANDLE 'cast( Ihandle ptr, IupGetAttribute( _MaintabsHandle, "VALUE_HANDLE" ) ) 'Get Active Doc Tab(iupscintilla handle)
			if( sciPoseidon <> 0 ) then

				' Get sciPoseidon current pos
				'dim as _pos = IupScintillaSendMessage( ih, 2008, 0, 0 ) ' SCI_GETCURRENTPOS = 2008
				
				if( sciRun <> 0 ) then	IupSetAttribute( sciPoseidon, "ADD", IupGetAttribute( sciRun, "VALUE" ) )
				
			end if
		end if		
		
		
		return IUP_DEFAULT
		
	end function
	
	
	private function buttonRestore_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		param = restoreParam
		
		IupSetAttribute( sciRun, "READONLY", "NO" )
		IupSetAttribute( sciRun, "VALUE", param._code )
		IupSetAttribute( sciRun, "READONLY", "YES" )
		
		param.index = 0
		display = " " + format( param.index, "00" ) + " / " + format( param._paramsCount, "00" ) + " "
		IupSetAttribute( runContainerLabel, "TITLE", display )
		
		return IUP_DEFAULT
		
	end function
	
	
	private function sendToEdit_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		dim as Ihandle ptr listHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadList" )
		if( listHandle <> 0 ) then
		
			dim as integer index = IupGetInt( listHandle, "VALUE" )
			if( index > 0 ) then
			
				if( index - 1 <=  ubound(snippetManager) ) then
					
					newParam = snippetManager(index-1)._object
					
					editFullPath = snippetManager(index-1).fullPath
					display0 = " " + format( newParam.index, "00" ) + " / " + format( newParam._paramsCount, "00" ) + " "
					IupSetAttribute( editContainerLabel, "TITLE", display0 )
					IupSetAttribute( editFullPathText, "VALUE", editFullPath )
					
					IupSetAttribute( sciEdit, "VALUE", newParam._code )
					IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Title" ), "VALUE", snippetManager(index-1).title )
					IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Description" ), "VALUE", snippetManager(index-1).description )
					IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_ShortCut" ), "VALUE", snippetManager(index-1).shortCut )
					
					' Change to Tab-Run
					IupSetInt( tabs, "VALUEPOS", 0 )
				end if
			end if
		end if
		
		return IUP_DEFAULT
	
	end function
	
	private function sendToRun_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		dim as Ihandle ptr listHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadList" )
		if( listHandle <> 0 ) then
		
			dim as integer index = IupGetInt( listHandle, "VALUE" )
			if( index > 0 ) then
			
				if( index - 1 <=  ubound(snippetManager) ) then
					
					param = snippetManager(index-1)._object
					
					runFullPath = snippetManager(index-1).fullPath
					display = " " + format( param.index, "00" ) + " / " + format( param._paramsCount, "00" ) + " "
					IupSetAttribute( runContainerLabel, "TITLE", display )
					IupSetAttribute( runFullPathText, "VALUE", runFullPath )
					
					IupSetAttribute( sciRun, "READONLY", "NO" )
					IupSetAttribute( sciRun, "VALUE", param._code )
					IupSetAttribute( sciRun, "READONLY", "YES" )
					
					IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Title1" ), "VALUE", snippetManager(index-1).title )
					IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_Description1" ), "VALUE", snippetManager(index-1).description )
					IupSetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_ShortCut1" ), "VALUE", snippetManager(index-1).shortCut )

					' Change to Tab-Run
					IupSetInt( tabs, "VALUEPOS", 1 )
				end if
			end if
		end if
		
		return IUP_DEFAULT
	
	end function
	
	
	private function buttonSearchDir_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		dim as Ihandle ptr fileDlg = IupFileDlg(), dirTextHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_textSearchDir" )

		IupSetAttribute( fileDlg, "PARENTDIALOG", "THIS_MAINDIALOG_HANDLE" )
		IupSetAttribute( fileDlg, "DIALOGTYPE", "DIR" )
		IupSetAttribute( fileDlg, "TITLE", "Search Snippets Dir" )
		
		dim as string _dir = IupGetAttribute( dirTextHandle, "VALUE" )[0]
		if( len( trim(_dir) ) > 0 ) then IupSetAttribute( fileDlg, "FILE", _dir ) 
		
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			dim as string fullPath = trim( IupGetAttribute( fileDlg, "VALUE" )[0] )
			if( len( dir( fullPath, &h10 ) ) > 0 ) then IupSetAttribute( dirTextHandle, "VALUE", fullPath )
		end if
		
		return IUP_DEFAULT
	
	end function
	
	
	private function preLoadList_BUTTON_CB cdecl( ih as Ihandle ptr, button as integer, pressed as integer, x as integer, y as integer, status as zstring ptr ) as integer

		if( button = IUP_BUTTON3 ) then

			dim as Ihandle ptr sendToEdit = IupItem( "Send To Create Tab", 0 )
			IupSetCallback( sendToEdit, "ACTION", cast( Icallback, @sendToEdit_ACTION ) )
			
			dim as Ihandle ptr sendToRun = IupItem( "Send To Run Tab", 0 )
			IupSetCallback( sendToRun, "ACTION", cast( Icallback, @sendToRun_ACTION ) )
						
			dim as Ihandle ptr popupMenu = IupMenu( sendToEdit, sendToRun, 0 )

			IupPopup( popupMenu, IUP_MOUSEPOS, IUP_MOUSEPOS )
			IupDestroy( popupMenu )
		end if
		
		return IUP_DEFAULT
		
	end function


	private function buttonAddList_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		dim as Ihandle ptr fileDlg = IupFileDlg()
		
		IupSetAttribute( fileDlg, "PARENTDIALOG", "THIS_MAINDIALOG_HANDLE" )
		IupSetAttribute( fileDlg, "DIALOGTYPE", "OPEN" )
		IupSetAttribute( fileDlg, "TITLE", "Open Sinppet File" )
		IupSetAttribute( fileDlg, "EXTFILTER", "Snippet Files|*.snippet|All Files|*.*" )
		IupPopup( fileDlg, IUP_CURRENT, IUP_CURRENT )
		
		if( IupGetInt( fileDlg, "STATUS") <> -1 ) then
		
			dim as string fullPath = IupGetAttribute( fileDlg, "VALUE" )[0]
			dim as SNIPPET_UNIT sn = loadSnippet( fullPath )
			
			if( len( sn.fullPath ) > 0 ) then
			
				redim preserve snippetManager( ubound(snippetManager) + 1 )
				snippetManager( ubound(snippetManager) ) = sn
				
				dim as Ihandle ptr listHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadList" )
				if( listHandle <> 0 ) then
				
					dim as string listItem = sn.shortCut
					
					for i as integer = len( sn.shortCut ) to 19
						listItem += " "
					next
					listItem += ( " : " + sn.title )
					IupSetAttribute( listHandle, "APPENDITEM", listItem )
				end if
			else
				' Fail
			end if
		end if
		
		return IUP_DEFAULT
		
	end function	


	private function buttonDelList_ACTION cdecl( ih as Ihandle ptr ) as integer
	
		dim as Ihandle ptr listHandle = IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_preLoadList" )
		if( listHandle <> 0 ) then
		
			dim as integer itemNumber = IupGetInt( listHandle, "VALUE" ), i
			
			if( itemNumber > 0 ) then
				if( ubound( snippetManager ) = 0 ) then
				
					erase snippetManager(0)._object._params
					erase snippetManager
					
					IupSetAttribute( listHandle, "REMOVEITEM", "ALL" )
					
				elseif( ubound( snippetManager ) > 0 ) then

					dim as SNIPPET_UNIT tempManager( ubound( snippetManager )-1 )
					dim as integer count
					
					for i  = 0 to ubound( snippetManager )
						if( i <> itemNumber - 1 ) then
							tempManager(count) = snippetManager(i)
							count += 1
						endif
					next
				
					IupSetInt( listHandle, "REMOVEITEM", itemNumber )
					
					for i = 0 to ubound( tempManager )
						snippetManager(i) = tempManager(i)
					next
					
					redim preserve snippetManager( ubound(tempManager) )
				end if
			end if
		end if
		
		return IUP_DEFAULT
		
	end function

	/'
	private function buttonOptionSave_ACTION cdecl( ih as Ihandle ptr ) as integer

		saveSettings()
		return IUP_DEFAULT
	end function
	'/
	
	
	

	' **************************************** The Main *************************************************
	sub poseidon_Dll_Go alias "poseidon_Dll_Go"( _dllFullPath as zstring ptr ) export

		' The main IupOpen() is already ran by poseidonFB, no need anymore
		POSEIDON_HANDLE = IupGetHandle( "POSEIDON_MAIN_DIALOG" ) ' Get poseidonFB main dialog handle(IUP)
		
		if( POSEIDON_HANDLE <> 0 ) then
		
			' Check the THIS_MAINDIALOG_HANDLE is already created......
			if( THIS_MAINDIALOG_HANDLE = 0 ) then
				
				' Get DLL path
				DLLPATH = _dllFullPath[0]
				dim as integer slashPos = instrrev( DLLPATH, "/" )
				DLLPATH = left( DLLPATH, slashPos ) ' include last /
				
				' Check which language using
				dim as zstring ptr poseidonTitlePtr = IupGetAttribute( POSEIDON_HANDLE, "TITLE" )
				dim as string poseidonTitle = trim( poseidonTitlePtr[0] )
				if( instr( poseidonTitle, "poseidonFB" ) = 0 ) then Language = "d" else Language = "freebasic"
				
				
				' Create Layout
				' Create Tab--Create
				dim as Ihandle ptr buttonNew = IupButton( " New ", 0 )
				dim as Ihandle ptr buttonEdit = IupButton( " Edit ", 0 )
				dim as Ihandle ptr buttonSave = IupButton( " Save ", 0 )
				dim as Ihandle ptr toolBar01 = IupHbox( buttonNew, buttonEdit, buttonSave, 0 )
				
				IupSetCallback( buttonNew, "ACTION", cast( Icallback, @buttonNew_ACTION ) )
				IupSetCallback( buttonEdit, "ACTION", cast( Icallback, @buttonEdit_ACTION ) )
				IupSetCallback( buttonSave, "ACTION", cast( Icallback, @buttonSave_ACTION ) )
				
				
				dim as Ihandle ptr label00 = IupLabel( "  Full Path: " )
				dim as Ihandle ptr label01 = IupLabel( "      Title: " )
				dim as Ihandle ptr label02 = IupLabel( "Description: " )
				dim as Ihandle ptr label03 = IupLabel( "   ShortCut: " )

				editFullPathText = IupText( 0 )
				IupSetAttributes( editFullPathText, "EXPAND=YES,SIZE=320x,READONLY=YES" )
				dim as Ihandle ptr text01 = IupText( 0 )
				IupSetAttributes( text01, "EXPAND=YES,SIZE=320x,NAME=codeSnippet_Title" )
				dim as Ihandle ptr text02 = IupText( 0 )
				IupSetAttributes( text02, "EXPAND=YES,SIZE=320x,NAME=codeSnippet_Description" )
				dim as Ihandle ptr text03 = IupText( 0 )
				IupSetAttributes( text03, "EXPAND=YES,SIZE=320x,NAME=codeSnippet_ShortCut" )
				
				dim as Ihandle ptr hBox00 = IupHbox( label00, editFullPathText, 0 )
				IupSetAttributes( hBox00, "EXPANDCHILDREN=YES" )
				dim as Ihandle ptr hBox01 = IupHbox( label01, text01, 0 )
				IupSetAttributes( hBox01, "EXPANDCHILDREN=YES" )
				dim as Ihandle ptr hBox02 = IupHbox( label02, text02, 0 )
				IupSetAttributes( hBox02, "EXPANDCHILDREN=YES" )
				dim as Ihandle ptr hBox03 = IupHbox( label03, text03, 0 )
				IupSetAttributes( hBox03, "EXPANDCHILDREN=YES" )
				
				dim as Ihandle ptr vBox01 = IupVbox( hBox00, hBox01, hBox02, hBox03, 0 )
				IupSetAttributes( vBox01, "EXPANDCHILDREN=YES" )
				
				dim as Ihandle ptr frame01 = IupFlatFrame( vBox01 )
				IupSetAttributes( frame01, "TITLE=Properties" )
				
				' Scintilla Edit
				sciEdit = IupScintilla()
				IupSetAttributes( sciEdit, "EXPAND=YES,SCROLLBAR=YES,TABSIZE=4,INDENTATIONGUIDES=LOOKBOTH,UTF8MODE=YES" )
				
				dim as Ihandle ptr frame02 = IupFlatFrame( sciEdit )
				IupSetAttributes( frame02, "TITLE=Code,EXPAND=YES" )
				
				dim as Ihandle ptr buttonLeft =		IupButton( "  <  ", 0 )
				dim as Ihandle ptr buttonRight =	IupButton( "  >  ", 0 )
				dim as Ihandle ptr buttonAdd =		IupButton( " Add ", 0 )
				dim as Ihandle ptr buttonDel =		IupButton( " Del ", 0 )
				
				editContainerLabel = IupLabel( " 00 / 00 " )
				
				dim as Ihandle ptr toolBar02 = IupHbox( buttonLeft, editContainerLabel, buttonRight, buttonAdd, buttonDel, 0 )
				IupSetAttributes( toolBar02, "ALIGNMENT=ACENTER" )
				IupSetCallback( buttonAdd, "ACTION", cast( Icallback, @buttonAdd_ACTION ) )
				IupSetCallback( buttonDel, "ACTION", cast( Icallback, @buttonDel_ACTION ) )
				IupSetCallback( buttonLeft, "ACTION", cast( Icallback, @buttonLeft_ACTION ) )
				IupSetCallback( buttonRight, "ACTION", cast( Icallback, @buttonRight_ACTION ) )
				
				
				dim as Ihandle ptr vBoxTitle = IupVbox( toolBar01, frame01, frame02, toolBar02, 0 )
				IupSetAttributes( vBoxTitle, "EXPANDCHILDREN=YES" )
				


				
				' Run Tab
				dim as Ihandle ptr buttonOpen = IupButton( " Open ", 0 )
				IupSetCallback( buttonOpen, "ACTION", cast( Icallback, @buttonOpen_ACTION ) )
				
				
				dim as Ihandle ptr hBoxOpen = IupHbox( buttonOpen, 0 )
				IupSetAttributes( hBoxOpen, "ALIGNMENT=ACENTER" )
				'IupSetAttributes( hBox10, "EXPANDCHILDREN=YES" )
				
				dim as Ihandle ptr label10 	= IupLabel( "  Full Path: " )
				dim as Ihandle ptr label11 	= IupLabel( "      Title: " )
				dim as Ihandle ptr label12 	= IupLabel( "Description: " )
				dim as Ihandle ptr label13 	= IupLabel( "   ShortCut: " )


				runFullPathText = IupText( 0 )
				IupSetAttributes( runFullPathText, "SIZE=320x,READONLY=YES" )
				dim as Ihandle ptr text11 = IupText( 0 )
				IupSetAttributes( text11, "SIZE=320x,READONLY=YES,NAME=codeSnippet_Title1" )
				dim as Ihandle ptr text12 = IupText( 0 )
				IupSetAttributes( text12, "SIZE=320x,READONLY=YES,NAME=codeSnippet_Description1" )
				dim as Ihandle ptr text13 = IupText( 0 )
				IupSetAttributes( text13, "SIZE=320x,READONLY=YES,NAME=codeSnippet_ShortCut1" )
				

				dim as Ihandle ptr hBox10 = IupHbox( label10, runFullPathText, 0 )
				IupSetAttributes( hBox10, "ALIGNMENT=ACENTER" )
				dim as Ihandle ptr hBox11 = IupHbox( label11, text11, 0 )
				IupSetAttributes( hBox11, "ALIGNMENT=ACENTER" )
				'IupSetAttributes( hBox11, "EXPANDCHILDREN=YES" )
				dim as Ihandle ptr hBox12 = IupHbox( label12, text12, 0 )
				IupSetAttributes( hBox12, "ALIGNMENT=ACENTER" )
				'IupSetAttributes( hBox12, "EXPANDCHILDREN=YES" )
				dim as Ihandle ptr hBox13 = IupHbox( label13, text13, 0 )
				IupSetAttributes( hBox13, "ALIGNMENT=ACENTER" )
				'IupSetAttributes( hBox13, "EXPANDCHILDREN=YES" )
				
				dim as Ihandle ptr vBox11 = IupVbox( hBox10, hBox11, hBox12, hBox13, 0 )
				'IupSetAttributes( vBox11, "EXPANDCHILDREN=YES" )
				
				dim as Ihandle ptr frame11 = IupFlatFrame( vBox11 )
				IupSetAttributes( frame11, "TITLE=Properties" )				
				
				' Scintilla Run
				sciRun = IupScintilla()
				IupSetAttributes( sciRun, "EXPAND=YES,SCROLLBAR=YES,TABSIZE=4,INDENTATIONGUIDES=LOOKBOTH,UTF8MODE=YES" )
				IupSetCallback( sciRun, "BUTTON_CB", cast( Icallback, @sciRun_BUTTON_CB ) )
				IupSetCallback( sciRun, "K_ANY", cast( Icallback, @sciRun_K_ANY ) )
				
				
				dim as Ihandle ptr frame10 = IupFlatFrame( sciRun )
				IupSetAttributes( frame10, "TITLE=Code,EXPAND=YES" )
				
				' Operate
				dim as Ihandle ptr buttonLeft10 =	IupButton( " < ", 0 )
				dim as Ihandle ptr buttonRight10 =	IupButton( " > ", 0 )
				dim as Ihandle ptr buttonSend = 	IupButton( "       Send       ", 0 )
				dim as Ihandle ptr buttonRestore = 	IupButton( " Restore ALL ", 0 )
				
				runContainerLabel = IupLabel( " 00 / 00 " )

				dim as Ihandle ptr toolBar10 = IupHbox( buttonLeft10, runContainerLabel, buttonRight10, IupFill, buttonRestore, buttonSend, 0 )
				IupSetAttributes( toolBar10, "ALIGNMENT=ACENTER" )
				
				IupSetCallback( buttonSend, "ACTION", cast( Icallback, @sendButton_ACTION ) )
				IupSetCallback( buttonRestore, "ACTION", cast( Icallback, @buttonRestore_ACTION ) )
				IupSetCallback( buttonLeft10, "ACTION", cast( Icallback, @buttonLeft10_ACTION ) )
				IupSetCallback( buttonRight10, "ACTION", cast( Icallback, @buttonRight10_ACTION ) )
				

				dim as Ihandle ptr vBoxRun = IupVbox( hBoxOpen, frame11, frame10, toolBar10, 0 )
				'IupSetAttributes( vBoxRun, "EXPANDCHILDREN=YES" )
				


				
				dim as Ihandle ptr shortCutToggle = IupToggle( "Use Document Short Cut.", 0 )
				dim as Ihandle ptr preLoadToggle = IupToggle( "Load Sinpplets To Pre-Load List While DLL Starting", 0 )
				dim as Ihandle ptr labelSearchDir = IupLabel( "Snippets Search Dir : " )
				dim as Ihandle ptr textSearchDir = IupText( 0 )
				dim as Ihandle ptr buttonSearchDir = IupButton( " Open ", 0 )
				dim as Ihandle ptr preLoadList = IupList( 0 )

				IupSetAttributes( shortCutToggle, "SIZE=320x,NAME=codeSnippet_shortCutToggle,VALUE=ON" )
				IupSetAttributes( preLoadToggle, "SIZE=320x,NAME=codeSnippet_preLoadToggle" )
				IupSetAttributes( textSearchDir, "EXPAND=HORIZONTAL,NAME=codeSnippet_textSearchDir" )
				IupSetAttributes( preLoadList, "SIZE=320x,NAME=codeSnippet_preLoadList,EXPAND=YES" )
				
				dim as Ihandle ptr IupHboxSearchDir = IupHbox( labelSearchDir, textSearchDir, buttonSearchDir, 0 )
				IupSetAttributes( IupHboxSearchDir, "ALIGNMENT=ACENTER" )
				
				dim as Ihandle ptr buttonAddList = IupButton( "  Add  ", 0 )
				dim as Ihandle ptr buttonDelList = IupButton( "  Del  ", 0 )
				'dim as Ihandle ptr buttonOptionSave = IupButton( "   Save   ", 0 )
				
				IupSetCallback( buttonSearchDir, "ACTION", cast( Icallback, @buttonSearchDir_ACTION ) )
				IupSetCallback( preLoadList, "BUTTON_CB", cast( Icallback, @preLoadList_BUTTON_CB ) )
				IupSetCallback( buttonAddList, "ACTION", cast( Icallback, @buttonAddList_ACTION ) )
				IupSetCallback( buttonDelList, "ACTION", cast( Icallback, @buttonDelList_ACTION ) )
				'IupSetCallback( buttonOptionSave, "ACTION", cast( Icallback, @buttonOptionSave_ACTION ) )
				
				dim as Ihandle ptr preLoadFrame = IupFrame( preLoadList )
				IupSetAttribute( preLoadFrame, "TITLE", " *Pre-Load List* " )
				
				
				dim as Ihandle ptr hBoxButtonList = IupHbox( buttonAddList, buttonDelList, IupFill, /' buttonOptionSave ,'/ 0 )
				dim as Ihandle ptr vBoxOption = IupVbox( shortCutToggle, preLoadToggle, IupHboxSearchDir, preLoadFrame, hBoxButtonList, 0 )


				tabs = IupTabs( vBoxTitle, vBoxRun, vBoxOption, 0 )
				IupSetAttributes( tabs, "TABTYPE=RIGHT,TABTITLE0=Create,TABTITLE1=Run,TABTITLE2=Option,NAME=codeSnippet_tabs" )
				
				
				THIS_MAINDIALOG_HANDLE = IupDialog( tabs )
				IupSetAttributes( THIS_MAINDIALOG_HANDLE, "MAXBOX=NO,MINBOX=NO,RESIZE=NO,PARENTDIALOG=POSEIDON_MAIN_DIALOG,OPACITY=220" )
				IupSetAttribute( THIS_MAINDIALOG_HANDLE, "TITLE", "Code Snippets" )
				IupSetHandle( "THIS_MAINDIALOG_HANDLE", THIS_MAINDIALOG_HANDLE )
				
				WORKING_POSEIDON_DOCUMENT_HANDLE = IupGetFocus()
				
				IupShowXY( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, IUP_CENTER )

				' Change to Tab-Run
				IupSetInt( tabs, "VALUEPOS", 1 )
				
				dim as zstring ptr p = IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_shortCutToggle" ), "VALUE" )
				
				' Show dialog first, then apply style will work......
				setScintillaStyle( sciEdit )
				setScintillaStyle( sciRun )
				
				loadSettings()
				
				if( IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_shortCutToggle" ), "VALUE" )[0] = "ON" ) then 
					if( quickAddtoPoseidon() = true ) then IupHide( THIS_MAINDIALOG_HANDLE )
				end if
			else
				
				WORKING_POSEIDON_DOCUMENT_HANDLE = IupGetFocus()
				if( IupGetAttribute( IupGetDialogChild( IupGetHandle( "THIS_MAINDIALOG_HANDLE" ), "codeSnippet_shortCutToggle" ), "VALUE" )[0] = "ON" ) then 
					if( quickAddtoPoseidon() = false ) then IupShow( THIS_MAINDIALOG_HANDLE )
				end if
			end if
		end if
	end sub
	
	'When poseidonFB quit, trigger the poseidonFB_Dll_Release()
	sub poseidon_Dll_Release alias "poseidon_Dll_Release"() export
		
		if( THIS_MAINDIALOG_HANDLE <> 0 ) then
		
			saveSettings()
			IupDestroy( THIS_MAINDIALOG_HANDLE )
		end if

	end sub
	
end extern