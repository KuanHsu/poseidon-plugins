﻿#include once "IUP/iup.bi"
#include once "IUP/iup_scintilla.bi"



'compile with -dll option


#ifdef __FB_WIN32__
	type CASTnode
		'private :
			as CASTnode ptr			father						'D language 4 bytes
			as CASTnode ptr			children					'D language 8 bytes
			as long					childDummy					'dummy
			
		public :
			as ulongint				name_						'D language 8 bytes
			as long					dummy
			
			as uinteger				kind_						'D language 8 bytes
			as ulongint				protection_, type_, base_	'D language 8x3 bytes
			as integer				lineNumber_
			as integer				endLineNum_
	end type
#else
	type CASTnode
		'private :
			as CASTnode ptr		father						'D language 4 bytes
			as CASTnode ptr		children					'D language 8 bytes
			as CASTnode ptr		childDummy					'dummy
			
		public :
			as string			name_						'D language 8 bytes
			as long				dummy
			
			as uinteger			kind_						'D language 8 bytes
			/'
			as string			protection_', type_', base_	'D language 8x3 bytes
			'/
			as string			dummy1, dummy2
			as long				lineNumber_
			as long				endLineNum_
	end type
#endif


dim shared as Ihandle ptr	THIS_MAINDIALOG_HANDLE
dim shared as Ihandle ptr	POSEIDON_HANDLE
dim shared as Ihandle ptr	POSEIDON_OUTLINE_ZBOX_HANDLE
dim shared as Ihandle ptr	treeF2

'Icon
private function loadImage_sort() as Ihandle ptr

	dim imgdata(256) as ubyte = {_
									5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,_
									5, 5, 5, 5, 5, 5, 1, 3, 3, 4, 1, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 5, 3, 1, 5, 3, 4, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 5, 5, 5, 5, 3, 4, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 1, 4, 3, 4, 3, 4, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 4, 3, 5, 5, 3, 4, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 4, 3, 5, 1, 3, 4, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 1, 4, 3, 3, 4, 3, 4, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,_
									5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 2, 2, 2, 2, 2, 2,_
									5, 5, 6, 5, 5, 5, 5, 5, 5, 5, 2, 2, 5, 7, 2, 2,_
									5, 5, 6, 5, 5, 5, 5, 5, 5, 5, 7, 5, 7, 2, 2, 5,_
									6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 7, 2, 7, 5, 5,_
									5, 0, 6, 0, 5, 5, 5, 5, 5, 5, 7, 2, 2, 5, 5, 7,_
									5, 5, 0, 5, 5, 5, 5, 5, 5, 5, 2, 2, 5, 5, 7, 7,_
									5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2, 2, 7, 2, 2, 2}
			
	dim as Ihandle ptr image = IupImage( 16, 16, @imgdata(0) )
	IupSetAttribute(image, "0", "76 90 92")
	IupSetAttribute(image, "1", "124 170 164")
	IupSetAttribute(image, "2", "140 70 148")
	IupSetAttribute(image, "3", "28 134 124")
	IupSetAttribute(image, "4", "74 143 155")
	IupSetAttribute(image, "5", "BGCOLOR")
	IupSetAttribute(image, "6", "107 119 134")
	IupSetAttribute(image, "7", "164 122 172")

	return image
	
end function



' This function is by dodicat
' https://www.freebasic.net/forum/viewtopic.php?f=7&t=27758#p262904
private Sub sort( array() As string, begin As Long = 0, Finish As Long = -1 )

	If begin>finish Then begin=Lbound(array):Finish=Ubound(array)
	
	Dim As Long i=begin,j=finish
	Dim As string x=lcase(array(((I+J)\2)))
	While I <= J
		While lcase(array(I)) < X :I+=1:Wend 'CHANGE TO > FOR SORTING DOWN
		While lcase(array(J)) > X :J-=1:Wend 'CHANGE TO < FOR SORTING DOWN
		If I<=J Then Swap array(I),array(J): I+=1:J-=1
	Wend
	If J >begin Then  sort(array(),begin,J)
	If I <Finish Then sort(array(),I,Finish)
	
End Sub		
	

private sub createTree()

	if( IupGetChildCount( POSEIDON_OUTLINE_ZBOX_HANDLE ) > 0 ) then
	
		dim as integer zboxPos = IupGetInt( POSEIDON_OUTLINE_ZBOX_HANDLE, "VALUEPOS" ) ' Get active zbox pos
		if( zboxPos >= 0 ) then

			dim as integer count
			dim as Ihandle ptr poseidonTree = IupGetChild( POSEIDON_OUTLINE_ZBOX_HANDLE, zboxPos )
			dim as Ihandle ptr toggleHandle = IupGetDialogChild( THIS_MAINDIALOG_HANDLE, "TraditionF2_sortToggle" )
			
			' Clear the tree
			if( treeF2 <> 0 ) then IupSetAttributeId( treeF2, "DELNODE", 0, "CHILDREN" )
			
			IupSetAttribute( treeF2, "TITLE", IupGetAttributeId( poseidonTree, "TITLE", 0 ) )
			IupSetAttributeId( treeF2, "ADDBRANCH", 0, "FUNCTIONs" )
			IupSetAttributeId( treeF2, "ADDBRANCH", 0, "SUBs" )
			

			if( IupGetAttribute( toggleHandle, "VALUE" )[0] = "OFF" ) then
			
				for i as integer = 1 to IupGetInt( poseidonTree, "COUNT" ) - 1
				
					dim as CASTnode ptr userdata = cast( CASTnode ptr, IupGetAttributeId( poseidonTree, "USERDATA", i ) )
					
					if( userdata->kind_ AND 6 ) then
					
						dim as string	title
						dim as string	poseidonNodeTitle = IupGetAttributeId( poseidonTree, "TITLE", i )[0]
						
						dim as integer	openPos = instr( poseidonNodeTitle, "(" )
						
						if( openPos > 0 ) then title = left( poseidonNodeTitle, openPos - 1 ) else title = poseidonNodeTitle
						
						if( instrrev( poseidonNodeTitle, "... [" ) = 0 ) then title = title + " ... [" + str( userdata->lineNumber_ ) + "]"
					

						if( userdata->kind_ AND 4 ) then 'B_SUB
							
							dim as integer countSUBchilds = IupGetIntId( treeF2, "CHILDCOUNT", 1 )
							IupSetAttributeId( treeF2, "ADDLEAF", countSUBchilds + 1, title )

							dim as integer lastAddNode = IupGetInt( treeF2, "LASTADDNODE" )
							IupSetAttributeId( treeF2, "IMAGE", lastAddNode, "IUP_sub" )

						elseif( userdata->kind_ AND 2 ) then 'B_FUNCTION
						
							dim as integer functionID = IupGetIntId( treeF2, "LAST", 1 )
							IupSetAttributeId( treeF2, "ADDLEAF", functionID + IupGetIntId( treeF2, "CHILDCOUNT", functionID ), title )
							
							dim as integer lastAddNode = IupGetInt( treeF2, "LASTADDNODE" )
							IupSetAttributeId( treeF2, "IMAGE", lastAddNode, "IUP_function" )
							
						end if
					end if
				next
			
			else
				
				dim as integer		i
				dim as string		_sub(), _function()
				dim as integer		subIndex = -1, functionIndex = -1
				
				redim _sub(3000)
				redim _function(3000)
				
				for i = 1 to IupGetInt( poseidonTree, "COUNT" ) - 1
				
					dim as CASTnode ptr userdata = cast( CASTnode ptr, IupGetAttributeId( poseidonTree, "USERDATA", i ) )

					dim as string	title
					dim as string	poseidonNodeTitle = IupGetAttributeId( poseidonTree, "TITLE", i )[0]
					
					dim as integer	openPos = instr( poseidonNodeTitle, "(" )
					
					if( openPos > 0 ) then title = left( poseidonNodeTitle, openPos - 1 ) else title = poseidonNodeTitle
					
					if( instrrev( poseidonNodeTitle, "... [" ) = 0 ) then title = title + " ... [" + str( userdata->lineNumber_ ) + "]"

					if( userdata->kind_ AND 4 ) then 'B_SUB
						
						subIndex += 1
						_sub(subIndex) = title
						
					elseif( userdata->kind_ AND 2 ) then 'B_FUNCTION

						functionIndex += 1
						_function(functionIndex) = title
						
					end if
				next
				
				'if( subIndex > -1 ) then redim preserve _sub(subIndex)
				'if( functionIndex > -1 ) then redim preserve _function(functionIndex)
					
				if( subIndex > -1 ) then sort( _sub(), Lbound(_sub), subIndex )
				if( functionIndex > -1 ) then sort( _function(), Lbound(_function), functionIndex )
				
				for i = 0 to subIndex
					
					dim as integer countSUBchilds = IupGetIntId( treeF2, "CHILDCOUNT", 1 )
					IupSetAttributeId( treeF2, "ADDLEAF", countSUBchilds + 1, _sub(i) )

					dim as integer lastAddNode = IupGetInt( treeF2, "LASTADDNODE" )
					IupSetAttributeId( treeF2, "IMAGE", lastAddNode, "IUP_sub" )

				next
				
				for i = 0 to functionIndex
					
					dim as integer functionID = IupGetIntId( treeF2, "LAST", 1 )
					IupSetAttributeId( treeF2, "ADDLEAF", functionID + IupGetIntId( treeF2, "CHILDCOUNT", functionID ), _function(i) )
					
					dim as integer lastAddNode = IupGetInt( treeF2, "LASTADDNODE" )
					IupSetAttributeId( treeF2, "IMAGE", lastAddNode, "IUP_function" )

				next					
			
			end if
		end if
		
		for id as integer = 0 to IupGetInt( treeF2, "COUNT" ) - 1
		
			IupSetIntId( treeF2, "TITLEFONTSIZE", id, 8 )
		next
	end if
	
end sub
	

private function setDialogSizeReturnPos() as string

	dim as string rasterSize = IupGetAttribute( IupGetDialogChild( POSEIDON_HANDLE, "POSEIDON_LEFT_TABS" ),"RASTERSIZE" )[0]
	rasterSize = right( rasterSize, len( rasterSize ) - instrrev( rasterSize, "x" ) )
	dim as string screenXY = IupGetAttribute( IupGetDialogChild( POSEIDON_HANDLE, "POSEIDON_LEFT_TABS" ),"SCREENPOSITION" )[0]
	screenXY = right( screenXY, len( screenXY ) - instrrev( screenXY, "," ) )

	rasterSize = "450x" + rasterSize
	IupSetAttribute( THIS_MAINDIALOG_HANDLE, "RASTERSIZE", rasterSize )
	
	return screenXY

end function



extern "C"
	declare sub poseidon_Dll_Go alias "poseidon_Dll_Go" ( _dllFullPath as zstring ptr )
	declare sub poseidon_Dll_Release alias "poseidon_Dll_Release" ()

	
	private function sortToggle_ACTION( ih as Ihandle ptr ) as integer
		
		createTree()
		
		return IUP_DEFAULT
		
	end function

	
	private function val_VALUECHANGED_CB( ih as Ihandle ptr ) as integer
		
		dim as integer value = IupGetInt( ih, "VALUE" )
		IupSetInt( THIS_MAINDIALOG_HANDLE, "OPACITY", value )
		
		return IUP_DEFAULT
		
	end function
	

	private function closeButton_ACTION( ih as Ihandle ptr ) as integer
		
		IupHide( THIS_MAINDIALOG_HANDLE )
		
		return IUP_DEFAULT
		
	end function
	

	private function tree_EXECUTELEAF_CB cdecl( ih as Ihandle ptr, id as integer ) as integer
	
		dim as string title = IupGetAttributeId( ih, "TITLE", id )[0]
		
		if( len( title ) > 0 ) then
		
			dim as integer openPos = instrrev( title, " ... [" )
			if( openPos > 0 ) then
			
				title = right( title, len(title) - ( openPos + 5 ) )
				title = left( title, len(title) - 1 )
				
				if( POSEIDON_HANDLE <> 0 ) then
				
					dim as Ihandle ptr _MaintabsHandle = IupGetDialogChild( POSEIDON_HANDLE, "POSEIDON_MAIN_TABS" )
					if( _MaintabsHandle <> 0 ) then
						
						dim as Ihandle ptr sciPoseidon = cast( Ihandle ptr, IupGetAttribute( _MaintabsHandle, "VALUE_HANDLE" ) ) 'Get Active Doc Tab(iupscintilla handle)
						if( sciPoseidon <> 0 ) then
						
							dim as integer lineNumber = val( title )
							lineNumber -= 1
							
							IupScintillaSendMessage( sciPoseidon, 2234, lineNumber, 0 )	' SCI_ENSUREVISIBLEENFORCEPOLICY 2234
							IupScintillaSendMessage( sciPoseidon, 2024, lineNumber, 0 )	' SCI_GOTOLINE 2024
							IupSetInt( sciPoseidon, "FIRSTVISIBLELINE", lineNumber )
						end if
					end if
				end if					
			end if
		end if
		
		return IUP_DEFAULT
	
	end function


	' **************************************** The Main *************************************************
	sub poseidon_Dll_Go alias "poseidon_Dll_Go"( _dllFullPath as zstring ptr ) export

		' The main IupOpen() is already ran by poseidonFB, no need anymore
		POSEIDON_HANDLE = IupGetHandle( "POSEIDON_MAIN_DIALOG" ) ' Get poseidonFB main dialog handle(IUP)
		
		if( POSEIDON_HANDLE <> 0 ) then

			POSEIDON_OUTLINE_ZBOX_HANDLE = IupGetDialogChild( POSEIDON_HANDLE, "zbox_Outline" )
			
			if( POSEIDON_OUTLINE_ZBOX_HANDLE <> 0 ) then
				
				' Since poseidonFB rev.437, check "NAME" to get poseidonFB / poseidonD
				if( IupGetAttribute( POSEIDON_HANDLE, "NAME" )[0] = "poseidonFB" ) then

					if( THIS_MAINDIALOG_HANDLE = 0 ) then
						
						treeF2 = IupTree()
						IupSetCallback( treeF2, "EXECUTELEAF_CB", cast( Icallback, @tree_EXECUTELEAF_CB ) )
						
						
						IupSetHandle( "TraditionF2_sort", loadImage_sort() )
						dim as Ihandle ptr sortToggle = IupToggle( 0, 0 )
						IupSetAttributes( sortToggle, "IMAGE=TraditionF2_sort,VALUE=OFF,FLAT=YES,NAME=TraditionF2_sortToggle" )
						IupSetCallback( sortToggle, "ACTION", cast(Icallback, @sortToggle_ACTION ) )
						
						dim as Ihandle ptr _val = IupVal( 0 )
						IupSetAttributes( _val, "MAX=240,MIN=100,VALUE=180,RASTERSIZE=80x20" )
						IupSetCallback( _val, "VALUECHANGED_CB", cast(Icallback, @val_VALUECHANGED_CB ) )
						
						dim as Ihandle ptr closeButton = IupButton( "   Close   ", 0 )
						IupSetAttribute( closeButton, "FLAT", "YES" )
						IupSetHandle( "closeButton", closeButton )
						IupSetCallback( closeButton, "ACTION", cast(Icallback, @closeButton_ACTION ) )
						
						dim as Ihandle ptr hBox = IupHbox( sortToggle, _val, IupFill, closeButton, 0 )
						IupSetAttribute( hBox, "ALIGNMENT", "ACENTER" )
						
						dim as Ihandle ptr vBox = IupVbox( treeF2, hBox, 0 )
						
						THIS_MAINDIALOG_HANDLE = IupDialog( vBox )
						IupSetAttributes( THIS_MAINDIALOG_HANDLE, "BOARED=NO,RESIZE=NO,MAXBOX=NO,MINBOX=NO,MENUBOX=NO,PARENTDIALOG=POSEIDON_MAIN_DIALOG,OPACITY=180,SHRINK=YES" )
						IupSetHandle( "THIS_MAINDIALOG_HANDLE", THIS_MAINDIALOG_HANDLE )
						IupSetAttribute( THIS_MAINDIALOG_HANDLE, "DEFAULTESC", "closeButton" )

						
						IupMap( THIS_MAINDIALOG_HANDLE )

						createTree()
						IupPopup( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, val( setDialogSizeReturnPos() ) )
						
					else
					
						createTree()
						IupPopup( THIS_MAINDIALOG_HANDLE, IUP_RIGHT, val( setDialogSizeReturnPos() ) )
						
					end if
				else
				
					IupMessageError( POSEIDON_HANDLE, "Sorry, this plugin is for freeBASIC use only!" )
				end if
			end if
		end if
	end sub
	
	
	'When poseidonFB quit, trigger the poseidonFB_Dll_Release()
	sub poseidon_Dll_Release alias "poseidon_Dll_Release"() export
		
		if( THIS_MAINDIALOG_HANDLE <> 0 ) then IupDestroy( THIS_MAINDIALOG_HANDLE )

	end sub
	
end extern